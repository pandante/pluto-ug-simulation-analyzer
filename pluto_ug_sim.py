import multiprocessing as mp
import ctypes
import numpy as np
from pathlib import Path
import configparser
import tools.grid_dot_out_reader as gdor
from mp_assistant import tonumpyarray


# TODO: This class probably does too much.
class PlutoUGSim(object):

    def __init__(self, runfile, runname, data_dir):

        super().__init__()

        self.data_dir = data_dir

        self._runinfo_from_ini(runfile, runname)

    def _runinfo_from_ini(self, runfile, runname):

        # Parse ini files
        confr = configparser.ConfigParser()
        confr.read(runfile)

        # Run identifier
        self.run_id = confr.get(runname, 'run_id')

        # TODO: Include time_i and time_f into run parameters - currently they are in the calculation or plotting ini files only. The behaviour should be such that if the latter are not provided then by default, all files are processed.
        # TODO: Include x[123]_min and x[123]_max into run parameters - currently they are in the calculation or plotting ini files. (Maybe there was a reason for this, though.)
        # TODO: Parameters should be overrideable by command line arguments.
        # Run parameters

        try:
            grid_file = confr.get(runname, 'grid_file')
            self.grid_file = Path(self.data_dir) / grid_file

        except configparser.NoOptionError:
            self.grid_file = None

        if self.grid_file:
            self.x1n, self.x2n, self.x3n = gdor.read(self.grid_file, 'nodal')
            self.x1, self.x2, self.x3 = gdor.read(self.grid_file, 'zonal')
            self.nx1 = self.x1.size
            self.nx2 = self.x2.size
            self.nx3 = self.x3.size
            self.x1min = np.min(self.x1n)
            self.x1max = np.max(self.x1n)
            self.x2min = np.min(self.x2n)
            self.x2max = np.max(self.x2n)
            self.x3min = np.min(self.x3n)
            self.x3max = np.max(self.x3n)

        else:
            self.nx1 = confr.getint(runname, 'nx1')
            self.nx2 = confr.getint(runname, 'nx2')
            self.nx3 = confr.getint(runname, 'nx3')
            self.x1min = confr.getfloat(runname, 'x1min')
            self.x1max = confr.getfloat(runname, 'x1max')
            self.x2min = confr.getfloat(runname, 'x2min')
            self.x2max = confr.getfloat(runname, 'x2max')
            self.x3min = confr.getfloat(runname, 'x3min')
            self.x3max = confr.getfloat(runname, 'x3max')
            self.x1n = np.linspace(self.x1min, self.x1max, self.nx1 + 1)
            self.x2n = np.linspace(self.x2min, self.x2max, self.nx2 + 1)
            self.x3n = np.linspace(self.x3min, self.x3max, self.nx3 + 1)
            self.x1 = (self.x1n[:-1] + self.x1n[1:]) / 2.
            self.x2 = (self.x2n[:-1] + self.x2n[1:]) / 2.
            self.x3 = (self.x3n[:-1] + self.x3n[1:]) / 2.

        self.x1d = self.x1[None, None, :]
        self.x2d = self.x2[None, :, None]
        self.x3d = self.x3[:, None, None]
        self.dx1 = self.x1n[1:] - self.x1n[:-1]
        self.dx2 = self.x2n[1:] - self.x2n[:-1]
        self.dx3 = self.x3n[1:] - self.x3n[:-1]

        self.shape = [self.nx3, self.nx2, self.nx1]
        self.ncells = self.nx3 * self.nx2 * self.nx1

        self.nvar = confr.getint(runname, 'nvar')
        self.irho = confr.getint(runname, 'irho')
        self.ivx1 = confr.getint(runname, 'ivx1')
        self.ivx2 = confr.getint(runname, 'ivx2')
        self.ivx3 = confr.getint(runname, 'ivx3')
        self.iprs = confr.getint(runname, 'iprs')
        self.itr1 = confr.getint(runname, 'itr1')
        self.itr2 = confr.getint(runname, 'itr2')

        self.dfile_pfx = confr.get(runname, 'dfile_pfx')
        self.dfile_ext = confr.get(runname, 'dfile_ext')
        self.dfile_dtype = np.dtype(confr.get(runname, 'dfile_dtype'))
        self.dvar_blocksize = np.dtype(self.dfile_dtype).alignment * self.ncells

        # NOTE: This is problem specific - not sure it should be here
        self.power = confr.getfloat(runname, 'power')
        self.vdnorm = confr.getfloat(runname, 'vdnorm')
        self.xdnorm = confr.getfloat(runname, 'xdnorm')
        self.tdnorm = confr.getfloat(runname, 'tdnorm')
        self.time_j = confr.getint(runname, 'time_j')

    def update_grid_params(self, x1, x2, x3, centering='zonal'):
        """
        NOTE: to update a non-uniform grid, the centering must be nodal.
              that is, a zonal input always assumes a uniform grid.
        """
        if centering == 'zonal':
            self.x1, self.x2, self.x3 = x1, x2, x3
            self.nx1, self.nx2, self.nx3 = x1.size, x2.size, x3.size
            self.dx1 = np.r_[self.nx1 * [x1[1] - x1[0]]]
            self.dx2 = np.r_[self.nx2 * [x2[1] - x2[0]]]
            self.dx3 = np.r_[self.nx3 * [x3[1] - x3[0]]]
            self.x1n = np.r_[x1[0] - 0.5 * self.dx1[0], x1 + 0.5 * self.dx1]
            self.x2n = np.r_[x2[0] - 0.5 * self.dx2[0], x2 + 0.5 * self.dx2]
            self.x3n = np.r_[x3[0] - 0.5 * self.dx3[0], x3 + 0.5 * self.dx3]
        else:
            self.x1n, self.x2n, self.x3n = x1, x2, x3
            self.nx1, self.nx2, self.nx3 = x1.size - 1, x2.size - 1, x3.size - 1
            self.dx1, self.dx2, self.dx3 = np.diff(self.x1n), np.diff(self.x2n), np.diff(self.x3n)
            self.x1 = self.x1n[1:] - 0.5 * self.dx1
            self.x2 = self.x2n[1:] - 0.5 * self.dx2
            self.x3 = self.x3n[1:] - 0.5 * self.dx3

        self.x1d = self.x1[None, None, :]
        self.x2d = self.x2[None, :, None]
        self.x3d = self.x3[:, None, None]
        self.x1min = np.min(self.x1n)
        self.x1max = np.max(self.x1n)
        self.x2min = np.min(self.x2n)
        self.x2max = np.max(self.x2n)
        self.x3min = np.min(self.x3n)
        self.x3max = np.max(self.x3n)
        self.shape = [self.nx3, self.nx2, self.nx1]
        self.ncells = self.nx3 * self.nx2 * self.nx1
        self.dvar_blocksize = np.dtype(self.dfile_dtype).alignment * self.ncells

    @staticmethod
    def multi_get(conf, sect, key, nmulti):

        val = conf.get(sect, key).split()
        val *= nmulti if len(val) == 1 else 1

        return val

    @staticmethod
    def multi_getfloat(conf, sect, key, nmulti):

        val = [float(i) for i in conf.get(sect, key).split()]
        val *= nmulti if len(val) == 1 else 1

        return val

    @staticmethod
    def multi_getint(conf, sect, key, nmulti):

        val = [int(i) for i in conf.get(sect, key).split()]
        val *= nmulti if len(val) == 1 else 1

        return val

    @staticmethod
    def multi_getboolean(conf, sect, key, nmulti):

        val = [i == 'True' for i in conf.get(sect, key).split()]
        val *= nmulti if len(val) == 1 else 1

        return val

    def x_space(self):
        """
        Create spatial arrays as shared memory arrays
        """

        # x coordinate array (in kpc)
        x1l = np.linspace(self.x1min, self.x1max, self.nx1, endpoint=False) + self.dx1 / 2.
        x2l = np.linspace(self.x2min, self.x2max, self.nx2, endpoint=False) + self.dx2 / 2.
        x3l = np.linspace(self.x3min, self.x3max, self.nx3, endpoint=False) + self.dx3 / 2.

        x1 = x1l[None, None, :]
        x2 = x2l[None, :, None]
        x3 = x3l[:, None, None]

        return x1, x2, x3

    def x_space_slices(self, pl_axis, intcpt=0):
        """
        Create spatial arrays for slice perpendicular to pl_axis, at intercept intcpt
        pl_axis is axis number as defined by PLUTO
        """

        x1 = x2 = x3 = None

        # Intercepts
        if pl_axis == 1:
            x1 = intcpt / self.nx1 * (self.x1max - self.x1min) + self.x1min
            x2 = np.linspace(self.x2min, self.x2max, self.nx2, endpoint=False) + self.dx2
            x3 = np.linspace(self.x3min, self.x3max, self.nx3, endpoint=False) + self.dx3
            return x1, x2[None, :], x3[:, None]

        elif pl_axis == 2:
            x1 = np.linspace(self.x1min, self.x1max, self.nx1, endpoint=False) + self.dx1
            x2 = intcpt / self.nx2 * (self.x2max - self.x2min) + self.x2min
            x3 = np.linspace(self.x3min, self.x3max, self.nx3, endpoint=False) + self.dx3
            return x1[None, :], x2, x3[:, None]

        elif pl_axis == 3:
            x1 = np.linspace(self.x1min, self.x1max, self.nx1, endpoint=False) + self.dx1
            x2 = np.linspace(self.x2min, self.x2max, self.nx2, endpoint=False) + self.dx2
            x3 = intcpt / self.nx3 * (self.x3max - self.x3min) + self.x3min
            return x1[None, :], x2[:, None], x3

    def x_space_1d(self):
        """
        Create spatial arrays as shared memory arrays
        """

        # x coordinate array (in kpc)
        x1 = np.linspace(self.x1min, self.x1max, self.nx1, endpoint=False) + self.dx1
        x2 = np.linspace(self.x2min, self.x2max, self.nx2, endpoint=False) + self.dx2
        x3 = np.linspace(self.x3min, self.x3max, self.nx3, endpoint=False) + self.dx3

        return x1, x2, x3

    def x_2d_labels(self, pl_axis, unit='kpc'):
        """
        Return appropriate labels for PLUTO axes.
        """

        x1 = x2 = x3 = None

        # Intercepts
        if pl_axis == 1:
            x1label = r'$y$ ' + '[ ' + unit + ' ]'
            x2label = r'$z$ ' + '[ ' + unit + ' ]'

        elif pl_axis == 2:
            x1label = r'$x$ ' + '[ ' + unit + ' ]'
            x2label = r'$z$ ' + '[ ' + unit + ' ]'

        elif pl_axis == 3:
            x1label = r'$x$ ' + '[ ' + unit + ' ]'
            x2label = r'$y$ ' + '[ ' + unit + ' ]'

        return x1label, x2label

    def read_data(self, vars, time, rot=None):
        """
        Read PLUTO data
        :return: data as shared arrays, converted to numpy arrays
        """

        # Data file
        fh = open(self.data_name(time, rot))

        out = [None] * 7

        # Create shared arrays.
        if 'rho' in vars:
            fh.seek(self.irho * self.dvar_blocksize)
            rho = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.irho] = rho

        if 'vx1' in vars:
            fh.seek(self.ivx1 * self.dvar_blocksize)
            vx1 = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.ivx1] = vx1

        if 'vx2' in vars:
            fh.seek(self.ivx2 * self.dvar_blocksize)
            vx2 = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.ivx2] = vx2

        if 'vx3' in vars:
            fh.seek(self.ivx3 * self.dvar_blocksize)
            vx3 = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.ivx3] = vx3

        if 'prs' in vars:
            fh.seek(self.iprs * self.dvar_blocksize)
            prs = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.iprs] = prs

        if 'tr1' in vars:
            fh.seek(self.itr1 * self.dvar_blocksize)
            tr1 = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.itr1] = tr1

        if 'tr2' in vars:
            fh.seek(self.itr2 * self.dvar_blocksize)
            tr2 = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
            out[self.itr2] = tr2

        fh.close()

        return out

    def read_data_mp(self, vars, time, rot=None):
        """
        Read PLUTO data
        :return: data as shared arrays, converted to numpy arrays
        """

        # Data file
        with open(self.data_name(time, rot)) as fh:

            out = [None] * 7

            # Create shared arrays.
            if 'rho' in vars:
                rho = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.irho * self.dvar_blocksize)
                rho[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.irho] = rho

            if 'vx1' in vars:
                vx1 = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.ivx1 * self.dvar_blocksize)
                vx1[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.ivx1] = vx1

            if 'vx2' in vars:
                vx2 = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.ivx2 * self.dvar_blocksize)
                vx2[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.ivx2] = vx2

            if 'vx3' in vars:
                vx3 = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.ivx3 * self.dvar_blocksize)
                vx3[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.ivx3] = vx3

            if 'prs' in vars:
                prs = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.iprs * self.dvar_blocksize)
                prs[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.iprs] = prs

            if 'tr1' in vars:
                tr1 = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.itr1 * self.dvar_blocksize)
                tr1[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.itr1] = tr1

            if 'tr2' in vars:
                tr2 = tonumpyarray(mp.RawArray(ctypes.c_float, self.ncells), self.shape, self.dfile_dtype)
                fh.seek(self.itr2 * self.dvar_blocksize)
                tr2[:, :, :] = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)
                out[self.itr2] = tr2

        return out

    def read_var(self, var, time, rot=None):

        # Data file
        fh = open(self.data_name(time, rot))

        ivar = getattr(self, 'i'+var)
        fh.seek(ivar * self.dvar_blocksize)
        data = np.fromfile(fh, self.dfile_dtype, self.ncells).reshape(self.shape)

        fh.close()

        return data

    def data_name(self, time, rot=None, dir=None, pfx=None, ext=None):

        if not dir:
            dir = self.data_dir

        if rot:
            pm1 = 'm' if rot[0] < 0 else 'p'
            pm2 = 'm' if rot[1] < 0 else 'p'
            pm3 = 'm' if rot[2] < 0 else 'p'
            rot_part = f'.{pm1}{abs(int(rot[0])):>03d}.{pm2}{abs(int(rot[1])):>03d}.{pm3}{abs(int(rot[2])):>03d}'
        else:
            rot_part = ''

        if pfx == None:
            pfx = self.dfile_pfx

        if ext == None:
            ext = self.dfile_ext

        return f'{dir}/{pfx}{rot_part}.{time:>04d}.{ext}'
