import numpy as np
import tools.physconst as pc
import tools.radiative_cooling as rc
import scipy.interpolate as spi
import tools.norm as norm

# Every variable has a "result", a "normalization", a "label", a "var_type", a "reduction_type"

mu = 0.60364
kelvin = pc.c * pc.c * pc.amu / pc.kboltz
nm = norm.PhysNorm(x=pc.kpc, v=pc.c, dens=mu*pc.amu, curr=1., temp=kelvin)


def need_vars(varlist):
    """
    Set the list of variables that need to be read from PLUTO data

    :varlist:          list ov variable names whose independent variables you want
    :point_type_only:  return only needed variables for point type

    :return:  list of variable names
    """

    var_dict = {
        'all'              : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr1', 'tr2'],
        'x1'               : [],
        'x2'               : [],
        'x3'               : [],
        'rho'              : ['rho'],
        'vx1'              : ['vx1'],
        'vx2'              : ['vx2'],
        'vx3'              : ['vx3'],
        'prs'              : ['prs'],
        'prs_pr'           : ['prs'],
        'tr1'              : ['tr1'],
        'tr2'              : ['tr2'],
        'te'               : ['rho', 'prs'],
        'te_pr'            : ['rho', 'prs', 'tr2'],
        'rhow'             : ['rho', 'tr2'],
        'vmag'             : ['vx1', 'vx2', 'vx3'],
        'vmagw'            : ['vx1', 'vx2', 'vx3', 'tr2'],
        'vradw'            : ['vx1', 'vx2', 'vx3', 'tr2'],
        'pmagw'            : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'pradw'            : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'ekin'             : ['rho', 'vx1', 'vx2', 'vx3'],
        'ekinw'            : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'ekinwr'           : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'eth'              : ['prs'],
        'ethw'             : ['prs', 'tr2'],
        'ekin/eth'         : ['rho', 'vx1', 'vx2', 'vx3', 'prs'],
        'ekinw/ethw'       : ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr2'],
        'ekinwr/ethw'      : ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr2'],
        'sfr'              : ['rho', 'tr2'],
        'sfrd'             : ['rho', 'tr2'],
        'sfrsd'            : ['rho', 'tr2'],
        'radio'            : ['prs', 'tr1'],
        'radiop'           : ['prs', 'tr1'],
        'Nh'               : ['rho'],
        'CO_rho'           : ['rho', 'prs'],
        'CO_rho_x1_slit'   : ['rho', 'prs'],
        'CO_rho_jet'       : ['rho', 'prs', 'tr1'],
        'CO_rhow'          : ['rho', 'prs', 'tr2'],
        'CO_rhow_jet'      : ['rho', 'prs', 'tr1', 'tr2'],
        'vxw1'             : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'vxw2'             : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'vxw3'             : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'redshw_x1'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'redshw_x2'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'redshw_x3'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmaw_x1'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmaw_x2'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmaw_x3'        : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'vradw_av'         : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmaw'           : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmawr'          : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'sigmawt'          : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'theta'            : [],
        'f_ekinwr'         : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'f_pradw'          : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'mdot_out'         : ['rho', 'vx1', 'vx2', 'vx3', 'tr2'],
        'lmd'              : ['rho', 'prs', 'tr2'],
        'lmdw'             : ['rho', 'prs', 'tr2'],
        'lo3'              : ['rho', 'prs', 'tr2'],
        'lha'              : ['rho', 'prs', 'tr2'],
        'lx1'              : ['rho', 'prs', 'tr1'],
        'lx2'              : ['rho', 'prs', 'tr1'],
        'lx3'              : ['rho', 'prs', 'tr1'],
        'lx4'              : ['rho', 'prs', 'tr1'],
        'lxs'              : ['rho', 'prs', 'tr1'],
        'fh1'              : ['rho', 'prs', 'tr2'],
        'fh2'              : ['rho', 'prs', 'tr2'],
        'tcl'              : ['rho', 'prs', 'tr2'],
        'tclw'             : ['rho', 'prs', 'tr2'],
        'tcl/tdyn'         : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'tclw/tdyn'        : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'CO_tcl'           : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'CO_tclw'          : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'CO_tcl_rho'       : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'CO_tclw_rhow'     : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr2'],
        'CO_tcl_jet'       : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr1', 'tr2'],
        'CO_tclw_jet'      : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr1', 'tr2'],
        'CO_tcl_rho_jet'   : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr1', 'tr2'],
        'CO_tclw_rhow_jet' : ['rho', 'prs', 'vx1', 'vx2', 'vx3', 'tr1', 'tr2'],
    }

    nv = []
    for v in varlist:
        nv += var_dict[v]

    return set(nv)


# TODO: Create new framework for creating cuts (thresholds) for arbitrary variables
# TODO: Use masking instead of 0

# TODO: primitives should be in array with indices used from pluto_parameters.PlutoUGSimParameters￼

def expression(var, x1=None, x2=None, x3=None, rho=None, vx1=None, vx2=None, vx3=None, prs=None, tr1=None, tr2=None,
               rho_min=0., rho_max=1.e30, te_min=0., te_max=1.e30, tr1_min=0., tr2_min=0., radius_min=None,
               radius_max=0., rho_crit=0., time=0., power=0., momentum=0., dx1=None, dx2=None, dx3=None, lscale=10.,
               nm=nm, x1_min=-1.e30, x1_max=1.e30, x2_min=1.e-30, x2_max=1.e30, x3_min=1.e-30, x3_max=1.e30):
    """
    Expressions for variables

    Everything here is in "data" (code) units. Normalizations are done elsewhere.

    :param rho_max:
    :param te_max:
    :param dx1:
    :param dx2:
    :param dx3:
    :param time:
    :param power:
    :param momentum:
    :param var:  The variable to be plotted
    :param data:  List of nvar PLUTO data to construct expression
    :param tr1_min:
    :param tr2_min:
    :param rho_min:
    :param rho_max:
    :param te_min:
    :param te_max:
    :param nm:    Normalization for turning cooling table into "code" units
    :param lscale:  Characteristic lengthscale (used in cooling/dynamic time, and mass outflow rate)
    :return:
    """

    rc_vars = ['lmd', 'lmdw', 'lo3', 'lha', 'lx1', 'lx2', 'lx3', 'lx4', 'lxs', 'fh1', 'fh2',
               'tcl', 'tclw', 'tcl/tdyn', 'tclw/tdyn', 'CO_tcl', 'CO_tclw', 'CO_tcl_rho', 'CO_tclw_rhow',
               'CO_tcl_jet', 'CO_tclw_jet', 'CO_tcl_rho_jet', 'CO_tclw_rhow_jet']

    if var in rc_vars:

        rct = rc.RCTable('neq2015.dat',
                         np.dtype([('T', '<f8'), ('ne', '<f8'), ('nH', '<f8'), ('nion', '<f8'), ('rho', '<f8'),
                                   ('fHI', '<f8'), ('fHII', '<f8'), ('mu', '<f8'),
                                   ('L', '<f8'), ('Ln', '<f8'), ('LOIII', '<f8'), ('LHa', '<f8'),
                                   ('LX1', '<f8'), ('LX2', '<f8'), ('LX3', '<f8'), ('LX4', '<f8')]),
                         skiprows=0, reverse=True)

        # Table data. The data are not converted to code units because the
        # cooling rate normalization constant is often too large (>10^100)
        # Thus, all radiative rates are returned in cgs by this function.
        rc_tmp = rct.data['T']
        rc_lmd = rct.data['Ln']
        rc_lo3 = rct.data['LOIII']
        rc_lha = rct.data['LHa']
        rc_lx1 = rct.data['LX1'] * rc_lmd
        rc_lx2 = rct.data['LX2'] * rc_lmd
        rc_lx3 = rct.data['LX3'] * rc_lmd
        rc_lx4 = rct.data['LX4'] * rc_lmd
        rc_fh1 = rct.data['fHI']
        rc_fh2 = rct.data['fHII']

        # Derived values
        rc_lxs = (rct.data['LX1'] + rct.data['LX2'] + rct.data['LX3']) * rc_lmd

    def vmag2():
        return vx1 * vx1 + vx2 * vx2 + vx3 * vx3

    def vmag():
        return np.sqrt(vmag2())

    def rad2():
        return x1 * x1 + x2 * x2 + x3 * x3

    def rad():
        return np.sqrt(rad2())

    def te():
        return prs / rho * mu

    def cvol():
        return dx1 * dx2 * dx3

    def eqv_sph_rad2():
        return (cvol() * 3. / 4. / np.pi) ** (2./3.)

    def sph_cap():
        # return 2 * np.pi * rad2() * (1. - np.sqrt(1. - eqv_sph_rad2() / rad2()))
        return np.pi * eqv_sph_rad2()

    def cyl_rad():
        return np.sqrt(x1 * x1 + x2 * x2)

    def x1_min_cut():
        return np.where(np.less(x1, x1_min), 0, 1)

    def x1_max_cut():
        return np.where(np.greater(x1, x1_max), 0, 1)

    def x2_min_cut():
        return np.where(np.less(x2, x2_min), 0, 1)

    def x2_max_cut():
        return np.where(np.greater(x2, x2_max), 0, 1)

    def x3_min_cut():
        return np.where(np.less(x3, x3_min), 0, 1)

    def x3_max_cut():
        return np.where(np.greater(x3, x3_max), 0, 1)

    def vrad():
        return (vx1 * x1 + vx2 * x2 + vx3 * x3) / rad()

    def vtan2():
        return vmag2() - vrad() * vrad()

    def tr1_min_cut():
        return np.where(np.less(tr1, tr1_min), 0, 1)

    def tr2_min_cut():
        return np.where(np.less(tr2, tr2_min), 0, 1)

    def vmagw():
        return vmag() * tr2_min_cut()

    def vradw():
        return vrad() * tr2_min_cut()

    def vtan2w():
        return vtan2() * tr2_min_cut()

    def vtanw():
        np.sqrt(vtan2w())

    def rhow():
        return rho * tr2 * tr2_min_cut()

    def pmagw():
        return rhow() * vmag()

    def pradw():
        return rhow() * vrad()

    def ekin():
        return 0.5 * rho * vmag2()

    def ekinw():
        return ekin() * tr2 * tr2_min_cut()

    def ekinwr():
        return 0.5 * rhow() * vrad() * vrad()

    def eth():
        return 3./2. * prs

    def ethw():
        return eth() * tr2 * tr2_min_cut()

    def sfr():
        return rhow() ** (3./2.) * tr2_min_cut()

    def CO_rho():
        return np.where(np.logical_and(rho > rho_min, te() * nm.temp < te_max),
                        np.where(rho > rho_crit, rho, rho * rho), 0.)

    def CO_rho_x1_slit():
        return CO_rho() * x1_min_cut() * x1_max_cut()

    def CO_rho_jet():
        return CO_rho() * tr1_min_cut()

    def CO_rhow():
        return np.where(np.logical_and(rhow() > rho_min, te() * nm.temp < te_max),
                        np.where(rhow() > rho_crit, rhow(), rhow() * rhow()), 0.)

    def CO_rhow_jet():
        return CO_rhow() * tr1_min_cut()

    def radio():
        return prs ** 1.8 * tr1 * tr1_min_cut()

    def cyl_radius_cut():
        return np.where(np.greater(cyl_rad(), radius_max), 0., 1.)

    def theta():
        return np.arccos(cyl_rad() / rad())

    def lmd():  # In cgs
        flmd = spi.interp1d(rc_tmp, rc_lmd, bounds_error=False, fill_value=0.)
        return flmd(te() * nm.temp)

    def lmdw():  # In cgs
        return lmd() * tr2 * tr2_min_cut()

    def lo3():
        flo3 = spi.interp1d(rc_tmp, rc_lo3, bounds_error=False, fill_value=0.)
        return rhow() * rhow() * flo3(te() * nm.temp)
        # return rho * rho * flo3(te() * nm.temp)

    def lha():
        flha = spi.interp1d(rc_tmp, rc_lha, bounds_error=False, fill_value=0.)
        return rhow() * rhow() * flha(te() * nm.temp)

    def lx1():
        flx1 = spi.interp1d(rc_tmp, rc_lx1, bounds_error=False, fill_value=0.)
        return (1. - tr1) * rho * rho * flx1(te() * nm.temp)

    def lx2():
        flx2 = spi.interp1d(rc_tmp, rc_lx2, bounds_error=False, fill_value=0.)
        return (1. - tr1) * rho * rho * flx2(te() * nm.temp)

    def lx3():
        flx3 = spi.interp1d(rc_tmp, rc_lx3, bounds_error=False, fill_value=0.)
        return (1. - tr1) * rho * rho * flx3(te() * nm.temp)

    def lx4():
        flx4 = spi.interp1d(rc_tmp, rc_lx4, bounds_error=False, fill_value=0.)
        return (1. - tr1) * rho * rho * flx4(te() * nm.temp)

    def lxs():
        flxs = spi.interp1d(rc_tmp, rc_lxs, bounds_error=False, fill_value=0.)
        return (1. - tr1) * rho * rho * flxs(te() * nm.temp)

    def fh1():
        ffh1 = spi.interp1d(rc_tmp, rc_fh1, bounds_error=False, fill_value=0.)
        return ffh1(te() * nm.temp)

    def fh2():
        ffh2 = spi.interp1d(rc_tmp, rc_fh2, bounds_error=False, fill_value=0.)
        return ffh2(te() * nm.temp)

    def tcl():  # Note, lmd() is in cgs
        return eth() * nm.pres / (rho * rho * lmd()) / nm.t

    def tclw():  # Note, lmdw() is in cgs
        return ethw() * nm.pres / (rhow() * rhow() * lmdw()) / nm.t

    def tcl_by_tdyn():
        return tcl() / np.abs(lscale / vrad())

    def tclw_by_tdyn():
        return tclw() / np.abs(lscale / vradw())

    def CO_tcl():
        return np.where(tcl_by_tdyn() < 1., 1., 0.)

    def CO_tclw():
        return np.where(tclw_by_tdyn() < 1., 1., 0.)

    def CO_tcl_rho():
        return CO_tcl() * rho

    def CO_tclw_rhow():
        return CO_tclw() * rhow()

    def CO_tcl_jet():
        return CO_tcl() * tr1_min_cut()

    def CO_tclw_jet():
        return CO_tclw() * tr1_min_cut()

    def CO_tcl_rho_jet():
        return CO_tcl_rho() * tr1_min_cut()

    def CO_tclw_rhow_jet():
        return CO_tclw_rhow() * tr1_min_cut()

    function = {
        'x1'               : lambda: x1 * np.ones_like(x2) * np.ones_like(x3),
        'x2'               : lambda: x2 * np.ones_like(x1) * np.ones_like(x3),
        'x3'               : lambda: x3 * np.ones_like(x1) * np.ones_like(x2),
        'rho'              : lambda: rho,
        'vx1'              : lambda: vx1,
        'vx2'              : lambda: vx2,
        'vx3'              : lambda: vx3,
        'prs'              : lambda: prs,
        'prs_pr'           : lambda: prs,
        'tr1'              : lambda: tr1,
        'tr2'              : lambda: tr2,
        'te'               : lambda: te(),
        'te_pr'            : lambda: te() * tr2_min_cut(),
        'rhow'             : lambda: rhow(),
        'vmag'             : lambda: vmag(),
        'vmagw'            : lambda: vmagw(),
        'vradw'            : lambda: vradw(),
        'pmagw'            : lambda: pmagw(),
        'pradw'            : lambda: pradw(),
        'ekin'             : lambda: ekin(),
        'ekinw'            : lambda: ekinw(),
        'ekinwr'           : lambda: ekinwr(),
        'eth'              : lambda: eth(),
        'ethw'             : lambda: ethw(),
        'ekin/eth'         : lambda: ekin() / eth(),
        'ekinw/ethw'       : lambda: ekinw() / ethw(),
        'ekinwr/ethw'      : lambda: ekinwr() / ethw(),
        'sfr'              : lambda: sfr(),
        'sfrd'             : lambda: sfr(),
        'sfrsd'            : lambda: sfr(),
        'radio'            : lambda: radio(),
        'radiop'           : lambda: radio(),
        'Nh'               : lambda: rho,
        'CO_rho'           : lambda: CO_rho(),
        'CO_rho_x1_slit'   : lambda: CO_rho_x1_slit(),
        'CO_rho_jet'       : lambda: CO_rho_jet(),
        'CO_rhow'          : lambda: CO_rhow(),
        'CO_rhow_jet'      : lambda: CO_rhow_jet(),
        'vxw1'             : lambda: vx1 * tr2_min_cut(),
        'vxw2'             : lambda: vx2 * tr2_min_cut(),
        'vxw3'             : lambda: vx3 * tr2_min_cut(),
        'redshw_x1'        : lambda: vx1 * tr2_min_cut(),
        'redshw_x2'        : lambda: vx2 * tr2_min_cut(),
        'redshw_x3'        : lambda: vx3 * tr2_min_cut(),
        'sigmaw_x1'        : lambda: vx1 * tr2_min_cut(),
        'sigmaw_x2'        : lambda: vx2 * tr2_min_cut(),
        'sigmaw_x3'        : lambda: vx3 * tr2_min_cut(),
        'sigmaw'           : lambda: vmagw(),
        'sigmawr'          : lambda: vradw(),
        'sigmawt'          : lambda: vtanw(),
        'vradw_av'         : lambda: vradw(),
        'theta'            : lambda: theta(),
        'f_ekinwr'         : lambda: ekinwr() * cvol() / (power * time),
        'f_pradw'          : lambda: pradw() * cvol() / (momentum * time),
        'mdot_out'         : lambda: pradw() * cvol() / lscale,
        'lmd'              : lambda: lmd(),
        'lmdw'             : lambda: lmdw(),
        'lo3'              : lambda: lo3(),
        'lha'              : lambda: lha(),
        'lx1'              : lambda: lx1(),
        'lx2'              : lambda: lx2(),
        'lx3'              : lambda: lx3(),
        'lx4'              : lambda: lx4(),
        'lxs'              : lambda: lxs(),
        'fh1'              : lambda: fh1(),
        'fh2'              : lambda: fh2(),
        'tcl'              : lambda: tcl(),
        'tclw'             : lambda: tclw(),
        'tcl/tdyn'         : lambda: tcl_by_tdyn(),
        'tclw/tdyn'        : lambda: tclw_by_tdyn(),
        'CO_tcl'           : lambda: CO_tcl(),
        'CO_tclw'          : lambda: CO_tclw(),
        'CO_tcl_rho'       : lambda: CO_tcl_rho(),
        'CO_tclw_rhow'     : lambda: CO_tclw_rhow(),
        'CO_tcl_jet'       : lambda: CO_tcl_jet(),
        'CO_tclw_jet'      : lambda: CO_tclw_jet(),
        'CO_tcl_rho_jet'   : lambda: CO_tcl_rho_jet(),
        'CO_tclw_rhow_jet' : lambda: CO_tclw_rhow_jet()
    }

    result = function[var]()

    # This is mainly a hack for excluding some data beyond a certain radius
    # where disc shouldn't have been initialized (IC5063)
    if radius_max > 0:
        result *= cyl_radius_cut()

    return result


def normalization(var, nm=nm, vnorm=1.e5):

    # This is the normalization desired for presentation (plotting)
    # Not for converting between cgs and code units. Just use nm for that.

    # TODO: kpc and pc normalizations need to be more flexible

    normalizations = {
        'x1'               : nm.x / pc.pc,
        'x2'               : nm.x / pc.pc,
        'x3'               : nm.x / pc.pc,
        'rho'              : 1.,
        'vx1'              : nm.v / vnorm,
        'vx2'              : nm.v / vnorm,
        'vx3'              : nm.v / vnorm,
        'prs'              : nm.pres / pc.kboltz,
        'prs_pr'           : nm.pres / pc.kboltz,
        'tr1'              : 1.,
        'tr2'              : 1.,
        'te'               : nm.temp,
        'te_pr'            : nm.temp,
        'rhow'             : 1.,
        'vmag'             : nm.v / vnorm,
        'vmagw'            : nm.v / vnorm,
        'vradw'            : nm.v / vnorm,
        'pmagw'            : nm.v,
        'pradw'            : nm.v,
        'ekin'             : nm.pres,
        'ekinw'            : nm.pres,
        'ekinwr'           : nm.pres,
        'eth'              : nm.pres,
        'ethw'             : nm.pres,
        'ekin/eth'         : 1,
        'ekinw/ethw'       : 1,
        'ekinwr/ethw'      : 1,
        'sfr'              : (nm.m / nm.t) / (pc.msun / pc.yr),
        'sfrd'             : (nm.dens / nm.t) / (pc.msun / (pc.pc ** 3 * pc.yr)),
        'sfrsd'            : (nm.dens * nm.x / nm.t) / (pc.msun / (pc.pc * pc.yr)),
        'radio'            : 1.,
        'radiop'           : 1.,
        'Nh'               : nm.x,
        'CO_rho'           : 1.,
        'CO_rho_x1_slit'   : 1.,
        'CO_rho_jet'       : 1.,
        'CO_rhow'          : 1.,
        'CO_rhow_jet'      : 1.,
        'vxw1'             : nm.v / vnorm,
        'vxw2'             : nm.v / vnorm,
        'vxw3'             : nm.v / vnorm,
        'redshw_x1'        : nm.v / vnorm,
        'redshw_x2'        : nm.v / vnorm,
        'redshw_x3'        : nm.v / vnorm,
        'sigmaw_x1'        : nm.v / vnorm,
        'sigmaw_x2'        : nm.v / vnorm,
        'sigmaw_x3'        : nm.v / vnorm,
        'sigmaw'           : nm.v / vnorm,
        'sigmawr'          : nm.v / vnorm,
        'sigmawt'          : nm.v / vnorm,
        'vradw_av'         : nm.v / vnorm,
        'theta'            : 1.,
        'f_ekinwr'         : 1.,
        'f_pradw'          : 1.,
        'mdot_out'         : (nm.m / nm.t) / (pc.msun / pc.yr),
        'lmd'              : 1.,
        'lmdw'             : 1.,
        'lo3'              : 1.,
        'lha'              : 1.,
        'lx1'              : 1.,
        'lx2'              : 1.,
        'lx3'              : 1.,
        'lx4'              : 1.,
        'lxs'              : 1.,
        'fh1'              : 1.,
        'fh2'              : 1.,
        'tcl'              : nm.t / pc.kyr,
        'tclw'             : nm.t / pc.kyr,
        'tcl/tdyn'         : 1.,
        'tclw/tdyn'        : 1.,
        'CO_tcl'           : 1.,
        'CO_tclw'          : 1.,
        'CO_tcl_rho'       : 1.,
        'CO_tclw_rhow'     : 1.,
        'CO_tcl_jet'       : 1.,
        'CO_tclw_jet'      : 1.,
        'CO_tcl_rho_jet'   : 1.,
        'CO_tclw_rhow_jet' : 1.
    }

    return normalizations[var]


def get_var_label(var):

    # TODO: Clean up latex (mathrms) etc
    # TODO: Change () to [ ] for units

    # TODO: kpc and pc labels need to be more flexible

    labels = {
        'x1'               : r'$x_1$ [ pc ]',
        'x2'               : r'$x_2$ [ pc ]',
        'x3'               : r'$x_3$ [ pc ]',
        'rho'              : r'$\rho$ [ cm$^{-3}$ ]',
        'vx1'              : r'$v_x$ [ km s$^{-1}$ ]',
        'vx2'              : r'$v_y$ [ km s$^{-1}$ ]',
        'vx3'              : r'$v_z$ [ km s$^{-1}$ ]',
        'prs'              : r'$p/k_\mathrm{Boltz}$ [ K cm$^{-3}$ ]',
        'prs_pr'           : r'$\langle p/k_\mathrm{Boltz} \rangle$ [ K cm$^{-3}$ ]',
        'tr1'              : 'jet tracer',
        'tr2'              : 'warm phase tracer',
        'te'               : r'$T$ [ K ]',
        'te_pr'            : r'$\langle T \rangle$ [ K ]',
        'rhow'             : r'$\rho_w$ [ cm$^{-3}$ ]',
        'vmag'             : r'$|v|$ [ km s$^{-1}$ ]',
        'vmagw'            : r'$|v_w|$ [ km s$^{-1}$ ]',
        'vradw'            : r'$v_{r, w}$ [ km s$^{-1}$ ]',
        'pmagw'            : r'$|p_w|$ [ $\mu \times u$ cm$^{-2}$ s$^{-1}$ ]',
        'pradw'            : r'$p_r$ [ $\mu \times u$ cm$^{-2}$ s$^{-1}$ ]',
        'ekin'             : r'$E_\mathrm{kin}$ [ erg cm$^{-3}$ ]',
        'ekinw'            : r'$E_\mathrm{kin,w}$ [ erg cm$^{-3}$ ]',
        'ekinwr'           : r'$E_\mathrm{kin,w,r}$ [ erg cm$^{-3}$ ]',
        'eth'              : r'$E_\mathrm{th}$ [ erg cm$^{-3}$ ]',
        'ethw'             : r'$E_\mathrm{th,w}$ [ erg cm$^{-3}$ ]',
        'ekin/eth'         : r'$E_\mathrm{kin}/E_\mathrm{th}$',
        'ekinw/ethw'       : r'$E_\mathrm{kin,w}/E_\mathrm{th,w}$',
        'ekinwr/ethw'      : r'$E_\mathrm{kin,w,r}/E_\mathrm{th,w}$',
        'sfr'              : r'SFR [ M$_\odot$ yr$^{-1}$ ]',
        'sfrd'             : r'SFR [ M$_\odot$ yr$^{-1}$ pc$^{-3}$ ]',
        'sfrsd'            : r'SFR [ M$_\odot$ yr$^{-1}$ pc$^{-2}$ ]',
        'radio'            : r'Surface brightness [ arb. units ]',
        'radiop'           : r'Surface emissivity [ arb. units ]',
        'Nh'               : r'$N_\mathrm{H}$ [ cm$^{-2}$ ]',
        'CO_rho'           : r'Surface brightness [ arb. units ]',
        'CO_rho_x1_slit'   : r'Surface brightness [ arb. units ]',
        'CO_rho_jet'       : r'Surface brightness [ arb. units ]',
        'CO_rhow'          : r'Surface brightness [ arb. units ]',
        'CO_rhow_jet'      : r'Surface brightness [ arb. units ]',
        'vxw1'             : r'$\Delta v$ [ km s$^{-1}$ ]',
        'vxw2'             : r'$\Delta v$ [ km s$^{-1}$ ]',
        'vxw3'             : r'$\Delta v$ [ km s$^{-1}$ ]',
        'redshw_x1'        : r'$\Delta v$ [ km s$^{-1}$ ]',
        'redshw_x2'        : r'$\Delta v$ [ km s$^{-1}$ ]',
        'redshw_x3'        : r'$\Delta v$ [ km s$^{-1}$ ]',
        'sigmaw_x1'        : r'$\sigma$ [ km s$^{-1}$ ]',
        'sigmaw_x2'        : r'$\sigma$ [ km s$^{-1}$ ]',
        'sigmaw_x3'        : r'$\sigma$ [ km s$^{-1}$ ]',
        'sigmaw'           : r'$\sigma$ [ km s$^{-1}$ ]',
        'sigmawr'          : r'$\sigma$ [ km s$^{-1}$ ]',
        'sigmawt'          : r'$\sigma$ [ km s$^{-1}$ ]',
        'vradw_av'         : r'$v_r$ [ km s$^{-1}$ ]',
        'theta'            : r'$\theta$ [ $^\circ$ ]',
        'f_ekinwr'         : r'$E_\mathrm{kin,w,r} / E_\mathrm{inj}$',
        'f_pradw'          : r'$p_r / p_\mathrm{inj}$',
        'mdot_out'         : r'$\dot{M}_\mathrm{out}$ [ $M_\odot$ yr$^{-1}$ ]',
        'lmd'              : r'$\Lambda$ [ erg cm$^3$ s$^{-1}$ ]',
        'lmdw'             : r'$\Lambda$ [ erg cm$^3$ s$^{-1}$ ]',
        'lo3'              : r'SB [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lha'              : r'SB [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lx1'              : r'SB$_\mathrm{0.1 - 0.5 keV}$ [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lx2'              : r'SB$_\mathrm{0.5-1 keV}$ [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lx3'              : r'SB$_\mathrm{1-2 keV}$ [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lx4'              : r'SB$_\mathrm{2-10 keV}$ [ erg s$^{-1}$ cm$^{-2}$ ]',
        'lxs'              : r'SB$_\mathrm{0.1-2 keV}$ [ erg s$^{-1}$ cm$^{-2}$ ]',
        'fh1'              : r'H$\textsc{I}$ fraction',
        'fh2'              : r'H$\textsc{II}$ fraction',
        'tcl'              : r'$t_\mathrm{cool}$ [ kyr ]',
        'tclw'             : r'$t_\mathrm{cool}$ [ kyr ]',
        'tcl/tdyn'         : r'$t_\mathrm{cool}/t_\mathrm{dyn}$',
        'tclw/tdyn'        : r'$t_\mathrm{cool}/t_\mathrm{dyn}$',
        'CO_tcl'           : r'Surface brightness [ arb. units ]',
        'CO_tclw'          : r'Surface brightness [ arb. units ]',
        'CO_tcl_rho'       : r'Surface brightness [ arb. units ]',
        'CO_tclw_rhow'     : r'Surface brightness [ arb. units ]',
        'CO_tcl_jet'       : r'Surface brightness [ arb. units ]',
        'CO_tclw_jet'      : r'Surface brightness [ arb. units ]',
        'CO_tcl_rho_jet'   : r'Surface brightness [ arb. units ]',
        'CO_tclw_rhow_jet' : r'Surface brightness [ arb. units ]',
    }

    return labels[var]


def get_reduction_type(var):
    """
    :param var:   Variable name
    :return:      Reduction type

    Reduction types:
    None: No reduction.
    sum:  Summation (in 2D this is a "column" integral, in 1D its a summation over all cells).
    avg:  Weighted average.
    var:  Standard deviation.


    """

    reduction = {
        'x1'               : None,
        'x2'               : None,
        'x3'               : None,
        'rho'              : None,
        'vx1'              : None,
        'vx2'              : None,
        'vx3'              : None,
        'prs'              : None,
        'prs_pr'           : 'avg',
        'tr1'              : None,
        'tr2'              : None,
        'te'               : None,
        'te_pr'            : 'avg',
        'rhow'             : None,
        'vmag'             : None,
        'vmagw'            : None,
        'vradw'            : None,
        'pmagw'            : None,
        'pradw'            : None,
        'ekin'             : None,
        'ekinw'            : None,
        'ekinwr'           : None,
        'eth'              : None,
        'ethw'             : None,
        'ekin/eth'         : None,
        'ekinw/ethw'       : None,
        'ekinwr/ethw'      : None,
        'sfr'              : 'sum',
        'sfrd'             : None,
        'sfrsd'            : 'sum',
        'radio'            : 'sum',
        'radiop'           : None,
        'Nh'               : 'sum',
        'CO_rho'           : 'sum',
        'CO_rho_x1_slit'   : 'sum',
        'CO_rho_jet'       : 'sum',
        'CO_rhow'          : 'sum',
        'CO_rhow_jet'      : 'sum',
        'vxw1'             : None,
        'vxw2'             : None,
        'vxw3'             : None,
        'redshw_x1'        : 'avg',
        'redshw_x2'        : 'avg',
        'redshw_x3'        : 'avg',
        'sigmaw_x1'        : 'var',
        'sigmaw_x2'        : 'var',
        'sigmaw_x3'        : 'var',
        'sigmaw'           : 'var',
        'sigmawr'          : 'var',
        'sigmawt'          : 'var',
        'vradw_av'         : 'avg',
        'theta'            : None,
        'f_ekinwr'         : 'sum',
        'f_pradw'          : 'sum',
        'mdot_out'         : 'sum',
        'lmd'              : None,
        'lmdw'             : None,
        'lo3'              : 'sum',
        'lha'              : 'sum',
        'lx1'              : 'sum',
        'lx2'              : 'sum',
        'lx3'              : 'sum',
        'lx4'              : 'sum',
        'lxs'              : 'sum',
        'fh1'              : None,
        'fh2'              : None,
        'tcl'              : None,
        'tclw'             : None,
        'tcl/tdyn'         : None,
        'tclw/tdyn'        : None,
        'CO_tcl'           : 'sum',
        'CO_tclw'          : 'sum',
        'CO_tcl_rho'       : 'sum',
        'CO_tclw_rhow'     : 'sum',
        'CO_tcl_jet'       : 'sum',
        'CO_tclw_jet'      : 'sum',
        'CO_tcl_rho_jet'   : 'sum',
        'CO_tclw_rhow_jet' : 'sum',
    }

    return reduction[var]


def get_var_type(var):

    type = {
        'x1'               : 'point',
        'x2'               : 'point',
        'x3'               : 'point',
        'rho'              : 'point',
        'vx1'              : 'point',
        'vx2'              : 'point',
        'vx3'              : 'point',
        'prs'              : 'point',
        'prs_pr'           : 'point',
        'tr1'              : 'point',
        'tr2'              : 'point',
        'te'               : 'point',
        'te_pr'            : 'projection',
        'rhow'             : 'point',
        'vmag'             : 'point',
        'vmagw'            : 'point',
        'vradw'            : 'point',
        'pmagw'            : 'point',
        'pradw'            : 'point',
        'ekin'             : 'point',
        'ekinw'            : 'point',
        'ekinwr'           : 'point',
        'eth'              : 'point',
        'ethw'             : 'point',
        'ekin/eth'         : 'point',
        'ekinw/ethw'       : 'point',
        'ekinwr/ethw'      : 'point',
        'sfr'              : 'integral',
        'sfrd'             : 'point',
        'sfrsd'            : 'projection',
        'radio'            : 'projection',
        'radiop'           : 'point',
        'Nh'               : 'projection',
        'CO_rho'           : 'projection',
        'CO_rho_x1_slit'   : 'projection',
        'CO_rho_jet'       : 'projection',
        'CO_rhow'          : 'projection',
        'CO_rhow_jet'      : 'projection',
        'vxw1'             : 'point',
        'vxw2'             : 'point',
        'vxw3'             : 'point',
        'redshw_x1'        : 'projection',
        'redshw_x2'        : 'projection',
        'redshw_x3'        : 'projection',
        'sigmaw_x1'        : 'projection',
        'sigmaw_x2'        : 'projection',
        'sigmaw_x3'        : 'projection',
        'sigmaw'           : 'integral',
        'sigmawr'          : 'integral',
        'sigmawt'          : 'integral',
        'vradw_av'         : 'integral',
        'theta'            : 'point',
        'f_ekinwr'         : 'integral',
        'f_pradw'          : 'integral',
        'mdot_out'         : 'integral',
        'lmd'              : 'point',
        'lmdw'             : 'point',
        'lo3'              : 'projection',
        'lha'              : 'projection',
        'lx1'              : 'projection',
        'lx2'              : 'projection',
        'lx3'              : 'projection',
        'lx4'              : 'projection',
        'lxs'              : 'projection',
        'fh1'              : 'point',
        'fh2'              : 'point',
        'tcl'              : 'point',
        'tclw'             : 'point',
        'tcl/tdyn'         : 'point',
        'tclw/tdyn'        : 'point',
        'CO_tcl'           : 'projection',
        'CO_tclw'          : 'projection',
        'CO_tcl_rho'       : 'projection',
        'CO_tclw_rhow'     : 'projection',
        'CO_tcl_jet'       : 'projection',
        'CO_tclw_jet'      : 'projection',
        'CO_tcl_rho_jet'   : 'projection',
        'CO_tclw_rhow_jet' : 'projection',
    }

