import numpy as np

# Get the grid arrays from grid file. Assumes 3D.


def read_1d(gfp, nx):
    xg = np.loadtxt(gfp, max_rows=nx)
    xz = (xg[:, 2] + xg[:, 1]) / 2.
    xn = np.append(xg[:, 1], xg[-1, 2])
    return xn, xz


def read(grid_file, centering='nodal'):

    # Get cell center arrays from grid file
    with open(grid_file) as gfp:

        line = '#'
        while line[0] == '#':
            line = gfp.readline()

        nx1 = int(line)
        x1n, x1z = read_1d(gfp, nx1)

        nx2 = int(gfp.readline())
        x2n, x2z = read_1d(gfp, nx2)

        nx3 = int(gfp.readline())
        x3n, x3z = read_1d(gfp, nx3)

    if centering == 'zonal':
        return x1z, x2z, x3z

    elif centering == 'nodal':
        return x1n, x2n, x3n

    else:
        return False



