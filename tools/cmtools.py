import matplotlib.cm as cm
import matplotlib.colors as cl
import numpy as np
import scipy.interpolate as ip

# A faire
#  - I think ColormapEditor.multi_move can override move and 
#    ColormapEditor.mv_cmap_controlpoint.


class ColormapInterpolator():
    """
    A class to do interpolation on a Linear Segmented colormap 
    """

    def __init__(self, cmap=cm.jet):

        self.cmap = cmap
        self.ip_reds, self.ip_greens, self.ip_blues = self._ipcmfunc(cmap)

    def _ipcmfunc(self, cmap=None):
        """
        Create and return interpolation functions
        """
        if cmap == None: cmap = self.cmap

        reds = np.array(cmap._segmentdata['red']);
        ip_reds = ip.interp1d(reds[:, 0], reds[:, 1])

        greens = np.array(cmap._segmentdata['green']);
        ip_greens = ip.interp1d(greens[:, 0], greens[:, 1])

        blues = np.array(cmap._segmentdata['blue']);
        ip_blues = ip.interp1d(blues[:, 0], blues[:, 1])

        return ip_reds, ip_greens, ip_blues

    def interpcm(self, frac, cmap=None):
        """
        Linearly interpolate a Linear Segmented colormap cmap 
        (self.cmap if not provided here) and return interpolated 
        color as an RGB tuple.

        Arguments 
            frac        Fraction between 0 and 1 of color range where interpolated 
                        values lies.
            cmap        Linear Segmented colormap
        """

        if cmap != None:
            ip_reds, ip_greens, ip_blues = self._ipcmfunc(cmap)
        else:
            ip_reds, ip_greens, ip_blues = self.ip_reds, self.ip_greens, self.ip_blues

        shape = np.insert(np.array(np.shape(frac)), 0, 3.)

        # AYW 2012-03-01 11:02 JST Included tuple(ravel), but might break some code?
        # AYW 2013-09-26 15:31 JST changed ravel to reshape
        # return tuple(np.ravel((ip_reds(frac), ip_greens(frac), ip_blues(frac))))
        return np.reshape((ip_reds(frac), ip_greens(frac), ip_blues(frac)), shape).T


class ColormapEditor:
    """
    A class to do simple changes to colormaps
    """

    def __init__(self, cmap=cm.jet):
        self.cmap = cmap

    def insert(self, cmap=None, point=0, color=(1., 1., 1.,)):
        """
        Same as insert_cmap_controlpoint, just short form.
        """
        return self.insert_cmap_controlpoint(cmap, point, color)

    def pop(self, cmap=None, index=0):
        """
        Same as pop_cmap_controlpoint, just short form
        """
        return self.pop_cmap_controlpoint(cmap, index)

    def move(self, cmap=None, index=0, point=0):
        """
        Same as pop_cmap_controlpoint, just short form
        """
        return self.mv_cmap_controlpoint(cmap, index, point)

    def insert_cmap_controlpoint(self, cmap=None, point=0, color=(1., 1., 1.,)):
        """
        Insert control point in colormap. 

        This function can be used to fix the lower color limit of a cmap, which
        has a small offset. E.g., if the lowest value is 0 and the color
        is white (at exactly 0 and 0 only) then the displayed color is
        not white.
        Warning, this function changes the cmap in place.

     
        cmap    Colormap to be changes
        point   A scalar or tuple of where the r,g,b point is to be
        color   A tuple of the rgb color to insert. Default
                is (1., 1., 1.)
        """

        if cmap is None:
            cmap = self.cmap

        point = np.atleast_1d(point)
        point = 3 * list(point) if point.size < 2 else point

        for coln, pt, col in zip(['blue', 'green', 'red'], point, color):

            try:
                sd = cmap._segmentdata[coln]

            except AttributeError:
                lsc = cl.LinearSegmentedColormap.from_list('scratch', cmap.colors, cmap.N)
                sd = lsc._segmentdata[coln]


            # find position
            i = np.searchsorted(np.array(sd)[:, 0], pt)
            sd.insert(i, (pt, col, col))

        return cmap, i

    def pop_cmap_controlpoint(self, cmap=None, index=0):
        """
        Pop control point in colormap. 

        This function can be used to fix the lower color limit of a cmap, which
        has a small offset. E.g., if the lowest value is 0 and the color
        is white (at exactly 0 and 0 only) then the displayed color is
        not white.
        Warning, this function changes the cmap in place.
     
        cmap    Colormap to be changes
        point   A scalar or tuple of where the r,g,b point is to be
        color   A tuple of the rgb color to insert. Default
                is (1., 1., 1.)
        """

        if cmap == None: cmap = self.cmap

        for coln in ['green', 'red', 'blue']:
            cmap._segmentdata[coln].pop(index)

        pos = np.array(list(cmap._segmentdata.values()))[0, index, 0]
        return cmap, pos

    def get_ncp(self, cmap=None):
        """
        Obtain the number of control points in the color map
        """

        if cmap == None: cmap = self.cmap
        ncp = 0
        for coln in ['green', 'red', 'blue']:
            ncp = max(len(cmap._segmentdata[coln]), ncp)
        return ncp

    def even_controlpoints(self, cmap=None, fixed=[]):
        """
        Even the spacing between control points, except those 
        fixed
        """

        if cmap == None: cmap = self.cmap

    def mv_cmap_controlpoint(self, cmap=None, index=0, point=0):
        """
        Move a control point in colormap. 
        This is just a pop and insert manoeuvre.

        Warning, this function changes the cmap in place.
     
        cmap    Colormap to be changes
        index   Control point to be moved
        point   A scalar or tuple of where the r,g,b point is to be

        Warning, this has not been tested with tuple of points.

        """

        if cmap == None: cmap = self.cmap

        # point = np.atleast_1d(point)
        # point = 3*list(point) if point.size < 2 else point

        color = np.array(cmap._segmentdata.values())[:, index, 1]
        cmap, p = self.pop_cmap_controlpoint(cmap, index)
        cmap, i = self.insert_cmap_controlpoint(cmap, point, tuple(color))

        return cmap, i, p

    def multi_move(self, cmap=None, index=0, point=0):
        """
        Move control points in colormap. 
        This is just a pop and insert manoeuvre, but for
        multiple controlpoints we need to sort the indices to
        be moved in descending order so that we don't get the
        wrong index after a pop.

        Warning, this function changes the cmap in place.
     
        cmap    Colormap to be changes
        index   Control point, or list of control points, to be moved
        point   A scalar or tuple, or list therof, of where the 
                r,g,b point is to be moved to.

        Warning, this has not been tested with tuple of points, or lists
        of tuple of points.

        NOTE: I think this function can override move and mv_cmap_controlpoint
                
        """

        if cmap is None:
            cmap = self.cmap

        # Generalize some arguments (vectorize) to arrays
        points = np.atleast_1d(point)
        indices = np.atleast_1d(index)

        # The segmented data as an np array
        sd = np.array(list(cmap._segmentdata.values()))

        colors = []
        desc_i = np.argsort(indices)[::-1]
        for idx in indices[desc_i]:
            ncmap, pos = self.pop(cmap, idx)
            colors.append(sd[:, idx, 1])

        for i, p in enumerate(points[desc_i]):
            ncmap, idx = self.insert(cmap, p, tuple(colors[i]))

        return cmap

    def traverse(self, cmap=None, nt=1, indx=2):
        """
        Moves nt points from one side of the color bar about 
        the point indx to the other
        Negative nt means points below indx get moved to above indx
        Vice versa for positive nt 
        """
        if cmap == None: cmap = self.cmap

        # Number of control points
        ncp = self.get_ncp(cmap)

        # Check that number of indices to be transferred
        # are within bounds
        if (-nt >= indx) or (nt >= ncp):
            print('Error, Too many points to transfer.')
            return

        # Index range to be moved
        ir = range(indx + nt, indx, np.sign(-nt))

        # Destination points (just use one color for the moment)
        # Destination position interval
        # Get segmented data as numpy array for easy slicing
        # indx 0: color name
        # indx 1: control point indx
        # indx 2: (control point position, color, color)
        #          all three are fractions between 0 and 1
        sd = np.array(list(cmap._segmentdata.values()))
        pos1 = sd[0, indx, 0]
        pos2 = sd[0, indx - np.sign(nt), 0]
        pos = np.linspace(pos1, pos2, abs(nt) + 2)
        # pos = (div[:-1] + 0.5*(div[1:] - div[:-1]))

        ncmap = self.multi_move(cmap, ir, pos[1:-1])
        # cmap is changed in place
        # for i, p in zip(ir, pos):
        #    ncmap, ni, newp = self.move(cmap, i, p)

        return ncmap

    def even(self, cmap=None, idx1=0, idx2=-1):
        """
        Even the controlpoints between idx1 (lower) and idx2 (upper)
        Use idx2 = -1 to indicate highest.
        Points moved do not include limits.
        Only takes one color as a reference.
        """

        if cmap == None: cmap = self.cmap

        # Number of control points
        ncp = self.get_ncp(cmap)

        # The end index
        iend = ncp - 1 if idx2 == -1 else idx2

        sd = np.array(list(cmap._segmentdata.values()))
        pos1 = sd[0, idx1, 0]
        pos2 = sd[0, idx2, 0]
        pts = np.linspace(pos1, pos2, iend - idx1 + 1)

        # Cannot move points one by one, so pop all first then
        # insert all again. Pop all the relevant indices first
        # in descending order and get the colors in an array.
        # (cmap is changed in place. So actually no need to catch return
        # cmap with ncmap or with anything at all.)
        indices = np.arange(idx1 + 1, iend)
        pts_interior = pts[1:-1]
        cmap = self.multi_move(cmap, indices, pts_interior)
        # colors = []
        # desc_i = np.argsort(indices)[::-1]
        # for idx in indices[desc_i]:
        #     ncmap, pos = self.pop(cmap, idx)
        #     colors.append(sd[:,idx,1])

        # pts_interior = pts[1:-1]
        # for i, p in enumerate(pts_interior[desc_i]):
        #     ncmap, idx = self.insert(cmap, p, tuple(colors[i]))

        return cmap


class CustomColormaps():
    """
    A class containing custom predefined colormap 
    settings that can be registered to cmap object
    """

    # TODO: separate this into CustomColormap and CustomColormapCollection

    def __init__(self, name=None, cm_in=None, **kwargs):
        self.__dict__.update(locals());
        del self.__dict__['self']
        if name and cm_in:
            getattr(self, name)(name, cm_in, **kwargs)

    def hot_desaturated(self, name='hot_desaturated', cm_in=None):
        if cm_in is None:
            cm_in = self.cm_in

        locs = np.linspace(0.0, 1.0, 8)

        inv_range = 1. / 255.
        blues = [219., 91., 255., 0., 0., 0., 0., 76.]
        greens = [71., 0., 255., 127., 255., 96., 0., 76.]
        reds = [71., 0., 0., 0., 255., 255., 107., 224.]

        blues = [c * inv_range for c in blues]
        greens = [c * inv_range for c in greens]
        reds = [c * inv_range for c in reds]

        table = self.sd_from_list(locs, blues, greens, reds)
        sd = cm_in.colors.LinearSegmentedColormap(name, table)

        cm_in.register_cmap(name, sd)

        return self.cm_set_attr(name, cm_in)

    def hot_desaturated_wbg(self, name='hot_desaturated_wbg', cm_in=None, bgpt=0.0, vmin=None, vmax=None):
        """
        pt      Point where background value is to be inserted. If vmin or vmax 
                are not given then it is assumed to be a fraction. 
        vmin    Lower extent limit of the data that the colormap should represent
        vmax    Upper                           -
        name    Name of the colormap
        cm_in   The colormap object to update (it is updated in place anyway so no need, really)
        """

        if cm_in == None: cm_in = self.cm_in

        if vmin and vmax:
            frac = (bgpt - vmin) / (vmax - vmin)
        else:
            frac = bgpt

        cm_in, cmap = self.hot_desaturated(cm_in=cm_in)
        ce = ColormapEditor(cmap)
        ce.insert(point=frac)
        ce.traverse(nt=-1, indx=4)
        ce.even(idx1=3, idx2=-1)
        ce.even(idx1=0, idx2=3)

        sd = cmap._segmentdata
        lsc = cm_in.colors.LinearSegmentedColormap(name, sd)
        cm_in.register_cmap(name, lsc)

        return self.cm_set_attr(name, cm_in)

    def cm_set_attr(self, name, cm_in):
        try:
            cmap = cm_in.get_cmap(name)
            setattr(cm_in, name, cmap)
            return cm_in, cmap
        except:
            print('Register cmap first with cmap.regiester_cmap.')
            return

    def list_from_sd(self, sd):
        blues = []
        greens = []
        reds = []
        locs = []
        for i in range(len(sd['blue'])):
            blues.append(sd['blue'][i][1])
            greens.append(sd['green'][i][1])
            reds.append(sd['red'][i][1])
            locs.append(sd['blue'][i][0])

        return locs, blues, greens, reds

    def sd_from_list(self, locs, blues, greens, reds):

        ct_table = {}
        for rgb, li in [('blue', blues), ('green', greens), ('red', reds)]:
            col_rows = list(zip(locs, li, li))
            ct_table[rgb] = col_rows

        return ct_table
