import numpy as np

def renorm_vminmax(vmin, vmax, d, v_normalize):
    if not vmin:
        vmin = np.min(d)
    if not vmax:
        vmax = np.max(d)

    if vmin == vmax:
        if vmin > 0:
            vmax = np.max(d)
            vmin = vmax - vmin
        elif vmin < 0:
            vmin = np.min(d)
            vmax = vmin + vmax
        else:
            vmin = np.min(d)
            vmax = np.max(d)

        if v_normalize:
            d = d - vmin
            vmax = vmax - vmin
            vmin = 0

    return vmin, vmax, d

