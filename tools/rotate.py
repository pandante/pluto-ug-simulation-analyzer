import numpy as np
from scipy.interpolate import RegularGridInterpolator
from scipy.spatial.transform import Rotation as R
import tools.physconst as pc


def rotated_axes(x1d, x2d, x3d, rotations, red_i, limits='tight', zoom=1):

    nx1 = len(x1d)
    nx2 = len(x2d)
    nx3 = len(x3d)

    assert(limits in ['tight', 'max'])

    # Min max points to rotate
    d01 = [x1d[0], x2d[0], x3d[0]]
    d02 = [x1d[-1], x2d[-1], x3d[-1]]
    d11 = [x1d[-1], x2d[0], x3d[0]]
    d12 = [x1d[0], x2d[-1], x3d[-1]]
    d21 = [x1d[0], x2d[-1], x3d[0]]
    d22 = [x1d[-1], x2d[0], x3d[-1]]
    d31 = [x1d[0], x2d[0], x3d[-1]]
    d32 = [x1d[-1], x2d[-1], x3d[0]]

    if limits == 'tight':

        # Create and apply the rotation
        for rot in rotations:
            d01 = rot.apply(d01)
            d02 = rot.apply(d02)
            d11 = rot.apply(d11)
            d12 = rot.apply(d12)
            d21 = rot.apply(d21)
            d22 = rot.apply(d22)
            d31 = rot.apply(d31)
            d32 = rot.apply(d32)

        # Find min and max of new cube
        x1min = min([d01[0], d02[0], d11[0], d12[0], d21[0], d22[0], d31[0], d32[0]])
        x1max = max([d01[0], d02[0], d11[0], d12[0], d21[0], d22[0], d31[0], d32[0]])
        x2min = min([d01[1], d02[1], d11[1], d12[1], d21[1], d22[1], d31[1], d32[1]])
        x2max = max([d01[1], d02[1], d11[1], d12[1], d21[1], d22[1], d31[1], d32[1]])
        x3min = min([d01[2], d02[2], d11[2], d12[2], d21[2], d22[2], d31[2], d32[2]])
        x3max = max([d01[2], d02[2], d11[2], d12[2], d21[2], d22[2], d31[2], d32[2]])

    elif limits == 'max':
        # Find most distant corner point and use that as half image size
        d01, d02 = np.array(d01), np.array(d02)
        d11, d12 = np.array(d11), np.array(d12)
        d21, d22 = np.array(d21), np.array(d22)
        d31, d32 = np.array(d31), np.array(d32)
        diag01, diag02 = np.sqrt((d01 * d01).sum()), np.sqrt((d02 * d02).sum())
        diag11, diag12 = np.sqrt((d11 * d11).sum()), np.sqrt((d12 * d12).sum())
        diag21, diag22 = np.sqrt((d21 * d21).sum()), np.sqrt((d22 * d22).sum())
        diag31, diag32 = np.sqrt((d31 * d31).sum()), np.sqrt((d32 * d32).sum())
        diag_max = max([diag01, diag02, diag11, diag12, diag21, diag22, diag31, diag32])
        x1min = x2min = x3min = -diag_max / zoom
        x1max = x2max = x3max = -x1min

    # Construct new rotated coordinate arrays with approximately the same cell widths
    mid1 = int(nx1 / 2)
    mid2 = int(nx2 / 2)
    mid3 = int(nx3 / 2)
    ccw1 = x1d[mid1 + 1] - x1d[mid1]
    ccw2 = x2d[mid2 + 1] - x2d[mid2]
    ccw3 = x3d[mid3 + 1] - x3d[mid3]
    strc_x1 = (x1d[-1] - x1d[0]) / (nx1 * ccw1)
    strc_x2 = (x2d[-1] - x2d[0]) / (nx2 * ccw2)
    strc_x3 = (x3d[-1] - x3d[0]) / (nx3 * ccw3)
    incr_x1 = (x1max - x1min) / (x1d[-1] - x1d[0])
    incr_x2 = (x2max - x2min) / (x2d[-1] - x2d[0])
    incr_x3 = (x3max - x3min) / (x3d[-1] - x3d[0])
    x1 = np.linspace(x1min, x1max, int(nx1 * incr_x1 * strc_x1 / red_i))
    x2 = np.linspace(x2min, x2max, int(nx2 * incr_x2 * strc_x2 / red_i))
    x3 = np.linspace(x3min, x3max, int(nx3 * incr_x3 * strc_x3 / red_i))

    return x1, x2, x3

