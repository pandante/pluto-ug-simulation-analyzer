import sys
import getopt


def signature():
    print('')
    print('python <scriptname>.py -r <runfile> -R <runname> -i <inifile> -s <setting> [data_dir] [out_dir]')
    print('')
    print('The defaults are as follows')
    print('runfile :  "runs.ini"')
    print('inifile :  "<script_name> + .ini"')
    print('runname :  "" , that is, use info in [DEFAULT]')
    print('setting :  "" , that is, use info in [DEFAULT]')
    print('out_dir :  "."')
    print('data_dir : "."')
    print('')


class IOManager(object):

    def __init__(self, argv, script_name, runfile='runs.ini'):

        super().__init__()

        self._cli_opts(argv, script_name, runfile)

    def _cli_opts(self, argv, script_name, runfile):

        self.runfile = runfile
        self.inifile = script_name + '.ini'
        self.runname = ''
        self.setting = ''
        self.out_dir = '.'
        self.data_dir = '.'

        # Parse script arguments
        try:
            opts, args = getopt.getopt(argv, 'hr:R:i:s:', ['runfile=', 'runname=', 'inifile=', 'setting='])

        except getopt.GetoptError:
            signature()
            sys.exit(2)

        for opt, arg in opts:

            if opt == '-h':
                signature()
                sys.exit()

            elif opt in ("-r", "--runfile"):
                self.runfile = arg

            elif opt in ("-R", "--runname"):
                self.runname = arg

            elif opt in ("-i", "--inifile"):
                self.inifile = arg

            elif opt in ("-s", "--setting"):
                self.setting = arg

        if len(args) > 1:
            self.data_dir = args[0]
            self.out_dir = args[1]

        elif len(args) > 0:
            self.out_dir = args[0]
