import numpy as np
import tools.norm as norm


class RadiativeCooling():
    """
    Almost Empty base class. Currently only contains the normalization object.
    """

    def __init__(self, nm=norm.PhysNorm(x=1., m=1., t=1., curr=1., temp=1.)):
        """
        nm     normalization of output quantities (w.r.t cgs)
        """

        self.__dict__.update(locals())
        del self.__dict__['self']


class RCTable(RadiativeCooling):
    def __init__(self, fname='cooltable.dat',
                 dtype=np.dtype([('T', '<f8'), ('L', '<f8')]),
                 skiprows=0, reverse=False):

        """
        Radiative cooling table
        """

        super(RCTable, self).__init__()

        fh = open(fname, 'r')

        if reverse:
            data = np.loadtxt(fname, dtype=dtype, skiprows=skiprows)[::-1]
        else:
            data = np.loadtxt(fname, dtype=dtype, skiprows=skiprows)

        fh.close()
        del fh

        self.__dict__.update(locals())
        del self.__dict__['self']

        self.d = self.data

    def fit_power_law(self, L0=None, alpha=-0.5, tmin=5.0, tmax=7.5):
        """
        Powerlaw fit using numpy.polyfit. From version 1.7 this will also be
        able to return the covariance matrix.
        """
        x = self.d['T']
        y = self.d['L']
        r = np.logical_and(x > tmin, x < tmax)
        self.fit = np.polyfit(x[r], y[r], 1)


class RCBrems(RadiativeCooling):
    """
    Thermal Bremsstrahlung. See Ribicki & Lightman.
    Cooling rate = 1.4e-27 * gaunt * sqrt(T)
    """

    def __init__(self, iT0=1.4e-27):
        """
        iT0      The constant 1.4e-27 * gaunt
        """

        super(RCBrems, self).__init__()

        self.__dict__.update(locals())
        del self.__dict__['self']

    def cool(self, T):
        """
        T       Temperature in Kelvin
        """

        return self.iT0 * np.sqrt(T)


class RCRelBrems(RadiativeCooling):
    """
    Bremsstrahlung including relativistic correction.
    See e.g. Krause & Alexander (2007).

    Cooling rate in SI:
        2.05e-40 * gaunt sqrt(T) (1 + 4.4e-10 T)
    Cooling rate in cgs:
        1.40e-27 * gaunt sqrt(T) (1 + 4.4e-10 T)

    First part is normal Bremsstrahlung. Second part is relativistic correction.
    """

    def __init__(self, iT0=1.4e-27, iT1=4.4e-10):
        super(RCRelBrems, self).__init__()

        self.__dict__.update(locals())
        del self.__dict__['self']

        # Thermal Bremsstrahlung instance
        self.thermal = RCBrems(iT0=iT0)

    def cool(self, T):
        """
        T       Temperature in Kelvin
        """

        return self.thermal.cool(T) * (1. + self.iT1 * T)

    def cool_correct(self, T, L):
        """
        Just add the correction term to an existing cooling function which has
        thermal Bremsstrahlung but not the relativistic correction yet.
        T       Temperature in Kelvin
        L       Cooling function
        """

        return L + self.thermal.cool(T) * self.iT1 * T


class RCPowerLaw(RadiativeCooling):
    def __init__(self):
        super(RCPowerLaw, self).__init__()

        pass

    def cool(self, T=1.e4, alpha=-0.5, Lambda0=1.e-20):
        """
        Power-law cooling function, curried below.

        Returns
        -------
        Lambda0 x T ^ alpha, the cooling per unit density squared (by default in cgs erg cm^3 s^-1)

        Parameters
        ----------
        T           Temperature (K)
        alpha       Power law index (dimensionless)
        Lambda0     Normalization (erg s^-1 cm^-3)
        nm          A PhysNorm object. Provides output in normalized units. Default is cgs units.

        """

        return Lambda0 * T ** alpha / norm.cool
