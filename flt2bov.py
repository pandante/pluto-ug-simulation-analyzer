import numpy as np
import multiprocessing as mp
import sys
import os
import configparser
from pluto_ug_sim import PlutoUGSim
from io_manager import IOManager
from mp_assistant import slices
import physconst as pc

# ______________________________________________________________________________________________________________________
# Description

# This script creates loads of BOV headers.
# To launch with test example, use parameters: -R 00deg-local -s test

nproc = 1

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here for the moment....

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # TODO: make ncpus a setting (also in other scripts)
        # Slice settings

        self.vars = confi.get(setting, 'vars').split()
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')
        self.divide_brick = confi.getboolean(setting, 'divide_brick')
        if self.divide_brick:
            self.nbricklets = self.multi_getint(confi, setting, 'nbricklets', 3)
        self.bov_fbasenm = confi.get(setting, 'bov_fbasenm')


def create_fname(basenm, var, time):
    """ Return file basename for bov output """

    fname = basenm + var + '-' + format(time, '>04d')

    return fname


def dump_bov(var, data, time):

    hfname = create_fname(par.bov_fbasenm, var, time)
    hfname = iom.out_dir + '/' + hfname + '.bof'

    data.tofile(hfname)

def dump_bov_header(var, time):

    fbname = create_fname(par.bov_fbasenm, var, time)
    hfname = iom.out_dir + '/' + fbname + '.bov'
    bfname = iom.out_dir + '/' + fbname + '.bof'


    sz_x1 = par.x1max - par.x1min
    sz_x2 = par.x2max - par.x2min
    sz_x3 = par.x3max - par.x3min

    header = [
        'TIME: ' + format(int(par.tdnorm / pc.kyr * (time - par.time_j)), '>5d'),
        'DATA_FILE: ' + bfname,
        'DATA_SIZE: ' + format(par.nx1, ' d') + format(par.nx2, ' d') + format(par.nx3, ' d'),
        'DATA_FORMAT: ' + 'FLOAT',
        'VARIABLE: ' + var,
        'DATA_ENDIAN: ' + 'LITTLE',
        'CENTERING: ' + 'zonal',
        'BRICK_ORIGIN: ' + format(par.x1min, ' 8.4f') + format(par.x2min, ' 8.4f') + format(par.x3min, ' 8.4f'),
        'BRICK_SIZE: ' + format(sz_x1, ' 8.4f') + format(sz_x2, ' 8.4f') + format(sz_x3, ' 8.4f'),
    ]
    if par.divide_brick:
        header += [
            'DIVIDE_BRICK: ' + 'true',
            'DATA_BRICKLETS: ' + format(int(par.nx1 / par.nbricklets[0]), ' d')
                               + format(int(par.nx2 / par.nbricklets[1]), ' d')
                               + format(int(par.nx3 / par.nbricklets[2]), ' d')
    ]

    fp = open(hfname, 'w')
    fp.writelines('\n'.join(header))

def mp_bovver(times):

    # Loop over all timesteps
    for itime, time in enumerate(times):

        # Data variables
        dvars = ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr1', 'tr2']

        # Loop over all data variables
        for ivar, dvar in enumerate(dvars):

            # If data variable is required add to output, otherwise leave it as None
            if dvar in par.vars:

                # Read data slice
                scratch = par.read_var(dvar, time)

                dump_bov(dvar, scratch, time)
                dump_bov_header(dvar, time)

                del scratch


# ______________________________________________________________________________________________________________________
# Main bit


# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

# All time steps and number of time steps
all_times = range(par.time_i, par.time_f + 1, par.time_delta)
nt = len(all_times)

# Processes
if nproc > 1:
    pool = mp.Pool(nproc)
    pool.map(mp_bovver, [all_times[i] for i in slices(nt, nproc)])
    pool.close()
    pool.join()

else:
    mp_bovver(all_times)

