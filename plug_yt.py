import yt
from yt.units import kboltz, amu, speed_of_light, kpc
from yt import derived_field
import matplotlib.pyplot as pl
import numpy as np
import configparser
import sys
import os
from expressions import need_vars, expression
from pluto_ug_sim import PlutoUGSim
from io_manager import IOManager

yt.enable_parallelism()


nproc = 4
# nproc = 16


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Analysis settings
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')
        self.vars = confi.get(setting, 'vars').split()

        self.fbasenm = confi.get(setting, 'fbasenm')


# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

mu = 0.6063
tnorm = (kpc / speed_of_light).in_cgs()
mnorm = (mu * amu.v / kpc.v ** 3)

times = range(par.time_i, par.time_f + 1, par.time_delta)

for time in times:

    # Read data file for this timestep
    rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data(need_vars(par.vars), time)

    # Create yt structure
    data = {}
    for v in need_vars(par.vars):
        if v == 'rho':
            data['rho'] = (rho, "g/cm**3")
        if v == 'vx1':
            data['vx1'] = (vx1, "cm/s")
        if v == 'vx2':
            data['vx2'] = (vx2, "cm/s")
        if v == 'vx3':
            data['vx3'] = (vx3, "cm/s")
        if v == 'prs':
            data['prs'] = (prs, "dyn/cm**2")
        if v == 'tr1':
            data['tr1'] = (tr1, "")
        if v == 'tr2':
            data['tr2'] = (tr2, "")

    sim_time = (time - par.time_j) * par.tdnorm

    bbox = np.array([[par.x3min, par.x3max], [par.x2min, par.x2max], [par.x1min, par.x1max]])
    ds = yt.load_uniform_grid(data, par.shape,
                              sim_time=sim_time,
                              length_unit="kpc",
                              time_unit=tnorm,
                              mass_unit=mnorm,
                              bbox=bbox, nprocs=nproc)

    fields = list(data.keys())
    field_units = [i[1] for i in data.values()]
    cg = ds.covering_grid(level=0, left_edge=[par.x3min, par.x2min, par.x1min], dims=ds.domain_dimensions)

    cg.write_to_gdf('gdffile.gdf', fields, nproc, field_units)

    gdf_data = yt.frontends.gdf.GDFDataset('gdffile.gdf')


    # The below is an example
    # p = yt.ProjectionPlot(ds, 'y', 'rho')
    # p.set_cmap('Spectral')
    # p.show()


