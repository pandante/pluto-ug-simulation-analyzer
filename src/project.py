class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # TODO: Some scripts use multi-get for _min and _maxs (e.g. slplot). Make consistent.
        #       In principle, only one value should be used for consistency, but I can
        #       imagine cases where changing values may be useful, e.g. testing.

        # Project settings
        self.vars = confi.get(setting, 'vars').split()
        self.npvars = len(self.vars)
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.rot1_i = confi.getfloat(setting, 'rot1_i')
        self.rot1_f = confi.getfloat(setting, 'rot1_f')
        self.rot1_delta = confi.getfloat(setting, 'rot1_delta')
        self.rot2_i = confi.getfloat(setting, 'rot2_i')
        self.rot2_f = confi.getfloat(setting, 'rot2_f')
        self.rot2_delta = confi.getfloat(setting, 'rot2_delta')
        self.rot3_i = confi.getfloat(setting, 'rot3_i')
        self.rot3_f = confi.getfloat(setting, 'rot3_f')
        self.rot3_delta = confi.getfloat(setting, 'rot3_delta')
        self.rot_data = confi.getboolean(setting, 'rot_data')
        self.rot_data_reduction = confi.getfloat(setting, 'rot_data_reduction')

        self.axis1 = confi.getboolean(setting, 'axis1')
        self.axis2 = confi.getboolean(setting, 'axis2')
        self.axis3 = confi.getboolean(setting, 'axis3')
        self.axes = [i + 1 for i, a in enumerate([self.axis1, self.axis2, self.axis3]) if a]

        self.tr1_min = confi.getfloat(setting, 'tr1_min')
        self.tr2_min = confi.getfloat(setting, 'tr2_min')
        self.rho_min = confi.getfloat(setting, 'rho_min')
        self.rho_max = confi.getfloat(setting, 'rho_max')
        self.te_min = confi.getfloat(setting, 'te_min')
        self.te_max = confi.getfloat(setting, 'te_max')
        self.rho_crit = confi.getfloat(setting, 'rho_crit')
        self.radius_max = confi.getfloat(setting, 'radius_max')
        # TODO: do sep_out_var_dir and sep_in_var_dir

        # Output file basename
        self.fbasenm = confi.get(setting, 'fbasenm')


def project(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out):
    """
    Do projection (reduction)
    """

    # Variable to be projected
    data = expression(pvar, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min, te_max=par.te_max,
                      tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max, rho_crit=par.rho_crit, nm=nm)

    # Weighted sum
    if reduction == 'sum':

        if pl_axis == 1:
            dx = par.dx1[None, None, :]
        elif pl_axis == 2:
            dx = par.dx2[None, :, None]
        elif pl_axis == 3:
            dx = par.dx3[:, None, None]

        out[:] = np.sum(data * dx, axis=3-pl_axis)

    # Weighted average
    elif reduction == 'avg':

        weights = expression('rho', x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min,
                             te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                             rho_crit=par.rho_crit, nm=nm)

        out[:] = np.average(data, axis=3-pl_axis, weights=weights)

    # Weighted standard deviation (from variance)
    elif reduction == 'var':

        weights = expression('rho', x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min,
                             te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                             rho_crit=par.rho_crit, nm=nm)

        average = np.average(data, axis=3-pl_axis, weights=weights)
        if pl_axis == 1:
            average = average[:, :, None]
        if pl_axis == 2:
            average = average[:, None, :]
        if pl_axis == 3:
            average = average[None, :, :]

        variance = np.average((data - average) ** 2, axis=3-pl_axis, weights=weights)
        out[:] = np.sqrt(variance)

    return out


def mp_project_slice(i):
    """
    Effectively currying in order to
    - preserve the signature of project function,
    - function passed to map only takes one argument
    """

    # Array slices
    if pl_axis == 1:
        sl = np.s_[i, :, :]

    if pl_axis == 2:
        sl = np.s_[i, :, :]

    if pl_axis == 3:
        sl = np.s_[:, i, :]

    return project(gx1,
                   gx2[sl] if pl_axis == 3 else gx2,
                   gx3[sl] if pl_axis in [1, 2] else gx3,
                   grho[sl] if grho is not None else None,
                   gvx1[sl] if gvx1 is not None else None,
                   gvx2[sl] if gvx2 is not None else None,
                   gvx3[sl] if gvx3 is not None else None,
                   gprs[sl] if gprs is not None else None,
                   gtr1[sl] if gtr1 is not None else None,
                   gtr2[sl] if gtr2 is not None else None,
                   gout[i, :])


def mp_project_init(x1, x2, x3, rho, v1, v2, v3, prs, tr1, tr2, out):
    """
    Called on each child process initialization.
    Create global variables for all big variables project_slice() depends on
    """

    global gx1, gx2, gx3, gvx1, gvx2, gvx3, grho, gprs, gtr1, gtr2, gout
    gx1, gx2, gx3, gvx1, gvx2, gvx3 = x1, x2, x3, v1, v2, v3
    grho, gprs, gtr1, gtr2, gout = rho, prs, tr1, tr2, out


def project_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2):
    """
    Initialte projections using multiple processes.
    """

    nx1, nx2, nx3 = len(x1), len(x2), len(x3)

    # Shared array where result will be stored. And make it a numpy array.
    if pl_axis == 1:
        size = nx3 * nx2
        grid = [nx3, nx2]
        sli = slices(nx3, nproc)

    if pl_axis == 2:
        size = nx3 * nx1
        grid = [nx3, nx1]
        sli = slices(nx3, nproc)

    if pl_axis == 3:
        size = nx2 * nx1
        grid = [nx2, nx1]
        sli = slices(nx2, nproc)

    out = mp.RawArray(ctypes.c_float, size)
    out = tonumpyarray(out, grid, par.dfile_dtype)

    # Start processes
    pool = mp.Pool(nproc,
                   initializer=mp_project_init,
                   initargs=(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out))

    # Slices and process numbers as arguments to worker function
    # sli = zip(list(slices(nx3, nproc)), range(nproc))

    # Perform projections
    pool.map(mp_project_slice, list(sli))
    pool.close()
    pool.join()

    return out

