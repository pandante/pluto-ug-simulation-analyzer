import numpy as np
import numpy.ma as ma
import multiprocessing as mp
import sys
import os
import subprocess
import configparser
from tools.expressions import get_var_label
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from mp_assistant import slices
import tools.physconst as pc
import matplotlib
import matplotlib.colors as colors
matplotlib.use('Agg')
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import seaborn
import tools.mpl_aesth as mpla
# seaborn.set(font_scale=1.0)
mpla.adjust_rcParams(use_kpfonts=True)

# pl.ion()

# ______________________________________________________________________________________________________________________
# Description

# This script creates data-binning plots of any variable available in expressions.py
# using the outputs of data_binning.py.
# To launch with test example use parameters: ...

# nproc = mp.cpu_count()
nproc = 1

# ______________________________________________________________________________________________________________________
# For debug

# Nothing here at the moment


# ______________________________________________________________________________________________________________________
# Functions

class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Plot settings
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.vars = confi.get(setting, 'vars').split()
        self.nvars = len(self.vars)
        self.weight = confi.get(setting, 'weight')
        self.nbins1 = confi.getint(setting, 'nbins1')
        self.nbins2 = confi.getint(setting, 'nbins2')
        self.nbins = [self.nbins1, self.nbins2]
        self.vmin1 = confi.getint(setting, 'vmin1')
        self.vmax1 = confi.getint(setting, 'vmax1')
        self.vmin2 = confi.getint(setting, 'vmin2')
        self.vmax2 = confi.getint(setting, 'vmax2')
        self.log1 = confi.getboolean(setting, 'log1')
        self.log2 = confi.getboolean(setting, 'log2')
        self.logh = confi.getboolean(setting, 'logh')
        self.vnorm = confi.getfloat(setting, 'vnorm')
        self.normed = confi.getboolean(setting, 'normed')
        self.vmin = confi.getfloat(setting, 'vmin')

        self.tr1_cut = confi.getfloat(setting, 'tr1_min')
        self.tr2_cut = confi.getfloat(setting, 'tr2_min')
        self.te_cut = confi.getfloat(setting, 'te_min')
        self.rho_cut = confi.getfloat(setting, 'rho_min')
        self.rho_crit = confi.getfloat(setting, 'rho_crit')
        self.radius_cut = confi.getfloat(setting, 'radius_max')

        self.cmap1 = confi.get(setting, 'cmap1')
        self.cmap2 = confi.get(setting, 'cmap2')
        self.ncontours = confi.getint(setting, 'ncontours')

        self.add_time = confi.getboolean(setting, 'add_time')
        self.make_movie = confi.getboolean(setting, 'make_movie')

        self.db_fbasenm = confi.get(setting, 'db_fbasenm')
        self.pl_fbasenm = confi.get(setting, 'pl_fbasenm')


def create_fname(basenm, time=None):

    # Add binning resolution
    fname = basenm + '-'.join(par.vars) + '-' + format(par.nbins1, '>03d') + \
            (('x' + format(par.nbins2, '>03d')) if par.nvar > 1 else '')

    # Time
    if time is None:
        pass

    elif time == 'movie':
        fname += '-%4d'

    else:
        fname += '-' + format(time, '>04d')

    return fname


def movie():

    ifname = iom.out_dir + '/' + create_fname(par.pl_fbasenm, 'movie') + '.png'
    mfname = iom.out_dir + '/' + create_fname(par.pl_fbasenm) + '.mp4'

    cmd = 'ffmpeg -start_number ' + str(par.time_i) + ' -i ' + ifname
    cmd += ' -y -vcodec mpeg4 -framerate 25 -filter:v "setpts=3.0*PTS" -b:v 18000000 '
    cmd += mfname
    print('\n')
    print('Generating movie with following command... ')
    print(cmd)
    print('\n')

    subprocess.call(cmd.split(), shell=True)

    fm = open(mfname + '.make', 'w')
    fm.write(cmd)
    subprocess.call(['chmod', '775', mfname + '.make'], shell=True)
    fm.close()


def mp_dbplot(times):

    # TODO: insert assert statements to check whether variable has correct reduction

    # 2-D Histogram
    if par.nvars > 1:

        # Loop over all timesteps
        for time in times:

            dbname = iom.data_dir + '/' + create_fname(par.db_fbasenm, time) + '.dbn'
            his = np.loadtxt(dbname)

            # plot histogram and contour plot
            if par.logh:
                his = ma.log10(his)
            his = np.ma.masked_invalid(his)
            seaborn.set_style('darkgrid', {'grid.color': '0.93'})

            cmap1 = cm.get_cmap(par.cmap1)
            pl.imshow(his[::-1, :],
                      extent=(h1[0], h1[-1], h2[0], h2[-1]),
                      aspect='auto', cmap=cmap1)
            # cbp = pl.colorbar(pad=0.02, aspect=30)
            cbp = pl.colorbar()

            # TODO: add this to your colormap tools (and learn from it)
            def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
                new_cmap = colors.LinearSegmentedColormap.from_list(
                    'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
                    cmap(np.linspace(minval, maxval, n)))
                return new_cmap

            cmap2 = cm.get_cmap(par.cmap2)
            new_cmap = truncate_colormap(cmap2, 0.3, 0.9)
            # cont = np.linspace(par.vmin, ...)
            pl.contour(h1, h2, his, par.ncontours, cmap=new_cmap)
            cbc = pl.colorbar(cax=cbp.ax)
            if par.logh:
                cbc.set_label(r'$\log$(probability density) [ arbitrary units ]')
            else:
                cbc.set_label(r'probability density [ arbitrary units ]')

            l1 = get_var_label(par.vars[0])
            if par.log1:
                l1 = r'$\log$ ' + l1
            pl.xlabel(l1)

            l2 = get_var_label(par.vars[1])
            if par.log2:
                l2 = r'$\log$ ' + l2
            pl.ylabel(l2)

            # Time in plot
            if par.add_time:
                time_text = r'$t = $' + format(int(par.tdnorm / pc.kyr * (time - par.time_j)), '>5d') + ' kyr'
                pl.text(0.80, 1.02, time_text, color='k', transform=pl.gca().transAxes)

            # Save figure
            ifname = iom.out_dir + '/' + create_fname(par.pl_fbasenm, time) + '.png'
            pl.savefig(ifname, bbox_inches='tight', dpi=300)

            pl.close(pl.gcf())

    # 1-D histogram
    else:
        dbname = iom.data_dir + '/' + create_fname(par.db_fbasenm) + '.dbn'
        his = np.loadtxt(dbname)

        for time in times:

            h = his[time, :]
            pl.hist(h1, bins1, weights=h)

            l1 = get_var_label(par.vars[0])
            if par.log1:
                l1 = r'$\log$ ' + l1
            pl.xlabel(l1)

            if par.logh:
                pl.ylabel(r'$\log$(probability density) [ arbitrary units ]')
            else:
                pl.ylabel(r'probability density [ arbitrary units ]')

            # Save figure
            ifname = iom.out_dir + '/' + create_fname(par.pl_fbasenm, time) + '.png'
            pl.savefig(ifname, bbox_inches='tight', dpi=300)

            pl.close(pl.gcf())


# ______________________________________________________________________________________________________________________
# Main bit


# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

# All time steps and number of time steps
times = range(par.time_i, par.time_f + 1)
nt = len(times)

# Reconstruct histogram bin centers
bins1 = np.linspace(par.vmin1, par.vmax1, par.nbins1 + 1)
bins2 = np.linspace(par.vmin2, par.vmax2, par.nbins2 + 1)
h1 = (bins1[:-1] + bins1[1:]) / 2.
h2 = (bins2[:-1] + bins2[1:]) / 2.

# All time steps and number of time steps
all_times = range(par.time_i, par.time_f + 1, par.time_delta)
nt = len(all_times)

times_slices = list(slices(nt, nproc))

# Processes
if nproc > 1:
    pool = mp.Pool(nproc)
    pool.map(mp_dbplot, [all_times[i] for i in times_slices])
    pool.close()
    pool.join()

else:
    mp_dbplot(all_times)


if par.make_movie:
     movie()
