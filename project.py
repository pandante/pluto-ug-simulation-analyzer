import numpy as np
import multiprocessing as mp
import ctypes
import sys
import os
import configparser
from scipy.interpolate import RegularGridInterpolator
from scipy.spatial.transform import Rotation as R
import tools.norm as norm
import tools.rotate as rotate
import tools.physconst as pc
from tools.io_manager import IOManager
from tools.expressions import need_vars, expression, get_reduction_type
from pluto_ug_sim import PlutoUGSim
from mp_assistant import slices, tonumpyarray

# from src.project import mp_project_init, mp_project_slice, project_mp, project, Parameters

# ______________________________________________________________________________________________________________________
# Description

# This script creates projections along axes of a 3D cartesian PLUTO
# simulation, on a shared memory node, using the multiprocessing module.
# A (non-locking) shared array is used.

# To launch use something like:
#
# python project.py -r runs.ini -R <run> -i project.ini -s <setting> [data_dir] [out_dir]
#
# where   <run> is configuration section name in runs.ini
#         <setting> is configuration section name in project.ini

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here at the moment

# ______________________________________________________________________________________________________________________
# Global settings

# Number of processors
# TODO: This should be an argument to the script
nproc = 1

# ______________________________________________________________________________________________________________________
# Functions

class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # TODO: Some scripts use multi-get for _min and _maxs (e.g. slplot). Make consistent.
        #       In principle, only one value should be used for consistency, but I can
        #       imagine cases where changing values may be useful, e.g. testing.

        # Project settings
        self.vars = confi.get(setting, 'vars').split()
        self.npvars = len(self.vars)
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.rot1_i = confi.getfloat(setting, 'rot1_i')
        self.rot1_f = confi.getfloat(setting, 'rot1_f')
        self.rot1_delta = confi.getfloat(setting, 'rot1_delta')
        self.rot2_i = confi.getfloat(setting, 'rot2_i')
        self.rot2_f = confi.getfloat(setting, 'rot2_f')
        self.rot2_delta = confi.getfloat(setting, 'rot2_delta')
        self.rot3_i = confi.getfloat(setting, 'rot3_i')
        self.rot3_f = confi.getfloat(setting, 'rot3_f')
        self.rot3_delta = confi.getfloat(setting, 'rot3_delta')
        self.rot_data = confi.getboolean(setting, 'rot_data')
        self.rot_data_reduction = confi.getfloat(setting, 'rot_data_reduction')
        self.rot_limits = confi.get(setting, 'rot_limits')
        self.rot_zoom = confi.getfloat(setting, 'rot_zoom')

        self.axis1 = confi.getboolean(setting, 'axis1')
        self.axis2 = confi.getboolean(setting, 'axis2')
        self.axis3 = confi.getboolean(setting, 'axis3')
        self.axes = [i + 1 for i, a in enumerate([self.axis1, self.axis2, self.axis3]) if a]

        self.tr1_min = confi.getfloat(setting, 'tr1_min')
        self.tr2_min = confi.getfloat(setting, 'tr2_min')
        self.rho_min = confi.getfloat(setting, 'rho_min')
        self.rho_max = confi.getfloat(setting, 'rho_max')
        self.te_min = confi.getfloat(setting, 'te_min')
        self.te_max = confi.getfloat(setting, 'te_max')
        self.rho_crit = confi.getfloat(setting, 'rho_crit')
        self.radius_max = confi.getfloat(setting, 'radius_max')
        # TODO: do sep_out_var_dir and sep_in_var_dir

        # Output file basename
        self.fbasenm = confi.get(setting, 'fbasenm')


def project(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out):
    """
    Do projection (reduction)
    """

    # Variable to be projected
    data = expression(pvar, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min, te_max=par.te_max,
                      tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max, rho_crit=par.rho_crit, nm=nm)

    # Weighted sum
    if reduction == 'sum':

        if pl_axis == 1:
            dx = par.dx1[None, None, :]
        elif pl_axis == 2:
            dx = par.dx2[None, :, None]
        elif pl_axis == 3:
            dx = par.dx3[:, None, None]

        out[:] = np.sum(data * dx, axis=3-pl_axis)

    # Weighted average
    elif reduction == 'avg':

        weights = expression('rho', x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min,
                             te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                             rho_crit=par.rho_crit, nm=nm)
        weights = np.ma.masked_less(weights, par.rho_min)

        out[:] = np.average(data, axis=3-pl_axis, weights=weights)

    # Weighted standard deviation (from variance)
    elif reduction == 'var':

        weights = expression('rho', x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min,
                             te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                             rho_crit=par.rho_crit, nm=nm)
        weights = np.ma.masked_less(weights, par.rho_min)

        average = np.average(data, axis=3-pl_axis, weights=weights)
        if pl_axis == 1:
            average = average[:, :, None]
        if pl_axis == 2:
            average = average[:, None, :]
        if pl_axis == 3:
            average = average[None, :, :]

        variance = np.average((data - average) ** 2, axis=3-pl_axis, weights=weights)
        out[:] = np.sqrt(variance)

    return out


def mp_project_slice(i):
    """
    Effectively currying in order to
    - preserve the signature of project function,
    - function passed to map only takes one argument
    """

    # Array slices
    if pl_axis == 1:
        sl = np.s_[i, :, :]

    if pl_axis == 2:
        sl = np.s_[i, :, :]

    if pl_axis == 3:
        sl = np.s_[:, i, :]

    return project(gx1,
                   gx2[sl] if pl_axis == 3 else gx2,
                   gx3[sl] if pl_axis in [1, 2] else gx3,
                   grho[sl] if grho is not None else None,
                   gvx1[sl] if gvx1 is not None else None,
                   gvx2[sl] if gvx2 is not None else None,
                   gvx3[sl] if gvx3 is not None else None,
                   gprs[sl] if gprs is not None else None,
                   gtr1[sl] if gtr1 is not None else None,
                   gtr2[sl] if gtr2 is not None else None,
                   gout[i, :])


def mp_project_init(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out):
    """
    Called on each child process initialization.
    Create global variables for all big variables project_slice() depends on
    """

    global gx1, gx2, gx3, gvx1, gvx2, gvx3, grho, gprs, gtr1, gtr2, gout
    gx1, gx2, gx3, gvx1, gvx2, gvx3 = x1, x2, x3, vx1, vx2, vx3
    grho, gprs, gtr1, gtr2, gout = rho, prs, tr1, tr2, out


def project_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2):
    """
    Initialte projections using multiple processes.
    """

    nx1, nx2, nx3 = par.nx1, par.nx2, par.nx3

    # Shared array where result will be stored. And make it a numpy array.
    if pl_axis == 1:
        size = nx3 * nx2
        grid = [nx3, nx2]
        sli = slices(nx3, nproc)

    if pl_axis == 2:
        size = nx3 * nx1
        grid = [nx3, nx1]
        sli = slices(nx3, nproc)

    if pl_axis == 3:
        size = nx2 * nx1
        grid = [nx2, nx1]
        sli = slices(nx2, nproc)

    out = mp.RawArray(ctypes.c_float, size)
    out = tonumpyarray(out, grid, par.dfile_dtype)

    # Start processes
    if nproc > 1:
        pool = mp.Pool(nproc,
                       initializer=mp_project_init,
                       initargs=(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out))

        # Slices and process numbers as arguments to worker function
        # sli = zip(list(slices(nx3, nproc)), range(nproc))

        # Perform projections
        pool.map(mp_project_slice, list(sli))
        pool.close()
        pool.join()
    else:
        mp_project_init(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, out)
        mp_project_slice(list(sli)[0])

    return out


def main(argv):
    """
    Driver routine
    :param argv:  see set_params function
    """

    # Get runfile and runname, dat_dir, inifile, setting
    global iom
    iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

    # Read settings parameter
    global par
    par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

    # Normalizations
    global nm
    mu = 0.60364
    mmpp = mu * pc.amu
    kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
    nm = norm.PhysNorm(x=par.xdnorm, v=par.vdnorm, dens=mmpp, curr=1., temp=kelvin)

    # The processes will be in sync so that we always have
    # the same time, theta, and phi at any given time
    # Automatically gets inherited (copied) to sub-processes
    global time

    # Loop over all time steps
    for time in range(par.time_i, par.time_f + 1, par.time_delta):

        # TODO: if files are read in the loops over angle should come before the data reading,
        #       else they should come after data reading.

        if not par.rot_data:
            rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars), time)

        for rot1 in np.arange(par.rot1_i, par.rot1_f + 1., par.rot1_delta):
            for rot2 in np.arange(par.rot2_i, par.rot2_f + 1., par.rot2_delta):
                for rot3 in np.arange(par.rot3_i, par.rot3_f + 1., par.rot3_delta):

                    rotvec_1 = [np.deg2rad(rot1), 0, 0]
                    rotvec_2 = [0, np.deg2rad(rot2), 0]
                    rotvec_3 = [0, 0, np.deg2rad(rot3)]
                    rot_1 = R.from_rotvec(rotvec_1)
                    rot_2 = R.from_rotvec(rotvec_2)
                    rot_3 = R.from_rotvec(rotvec_3)

                    # Read data file for this timestep
                    if par.rot_data:
                        # TODO: really should be rotating the nodal coords.
                        x1, x2, x3 = rotate.rotated_axes(par.x1, par.x2, par.x3, [rot_1, rot_2, rot_3],
                                                         par.rot_data_reduction, par.rot_limits, par.rot_zoom)
                        par.update_grid_params(x1, x2, x3, centering='zonal')
                        rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars), time,
                                                                             [rot1, rot2, rot3])
                    elif np.abs(rot1) + np.abs(rot2) + np.abs(rot3) > 0.:
                        # TODO: Do rotation here - not programmed yet.

                        # TODO: really should be rotating the nodal coords.
                        par.update_grid_params(x1, x2, x3, centering='zonal')

                        pass

                    x1, x2, x3 = par.x1d, par.x2d, par.x3d

                    global pl_axis, pvar, ivar, reduction

                    for pl_axis in par.axes:

                        for ivar, pvar in enumerate(par.vars):

                            reduction = get_reduction_type(pvar)

                            # Do projection
                            out = project_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2)

                            # Dump output file
                            fname = f'{iom.out_dir}/{par.fbasenm}-{pvar}-ax{pl_axis:>01d}-{time:>04d}.prj'
                            np.savetxt(fname, out)

                            print(f'pl_axis = {pl_axis}  pvar = {pvar}')


if __name__ == "__main__":
    main(sys.argv[1:])
