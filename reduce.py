import numpy as np
import multiprocessing as mp
import sys
import os
import configparser
import tools.norm as norm
import tools.physconst as pc
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from mp_assistant import slices
from tools.expressions import need_vars, expression
from tools.expressions import get_reduction_type

# ______________________________________________________________________________________________________________________
# Description

# This script creates reductions along axes of a 3D cartesian PLUTO
# simulation, on a shared memory node, using the multiprocessing module.
# A (non-locking) shared array is used.


# To launch with test example, use parameters: -r reduce.ini -s test 00deg-local

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here at the moment

# ______________________________________________________________________________________________________________________
# Global settings

# Number of processors
nproc = 4

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Project settings
        self.vars = confi.get(setting, 'vars').split()
        self.weight = confi.get(setting, 'weight')
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.tr1_min = confi.getfloat(setting, 'tr1_min')
        self.tr2_min = confi.getfloat(setting, 'tr2_min')
        self.rho_min = confi.getfloat(setting, 'rho_min')
        self.rho_max = confi.getfloat(setting, 'rho_max')
        self.te_min = confi.getfloat(setting, 'te_min')
        self.te_max = confi.getfloat(setting, 'te_max')
        self.rho_crit = confi.getfloat(setting, 'rho_crit')
        self.radius_max = confi.getfloat(setting, 'radius_max')

        self.fbasenm = confi.get(setting, 'fbasenm')
        # TODO: do sep_out_var_dir and sep_in_var_dir


def reduce(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2):
    """
    Do reduction
    """

    # Variable to be reduced
    data = expression(pvar, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, par.rho_max, te_min=par.te_min,
                      tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max, rho_crit=par.rho_crit,
                      time=time_code, power=power_code, momentum=pdot_code, dx1=par.dx1, dx2=par.dx2, dx3=par.dx3)

    if reduction in ['avg', 'var']:
        weight = expression(par.weight, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, par.rho_max, te_min=par.te_min,
                            tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max, rho_crit=par.rho_crit,
                            time=time_code, power=power_code, momentum=pdot_code, dx1=par.dx1, dx2=par.dx2, dx3=par.dx3)

    # Weighted standard deviation
    if reduction == 'sum':
        out = np.sum(data)

        return out

    # Weighted average
    elif reduction == 'avg':

        out, sw = np.average(data, weights=weight, returned=True)

        return out, sw

    # Weighted standard deviation
    elif reduction == 'var':
        out, sw = np.average((data - average) ** 2, weights=weight, returned=True)

        return out, sw


def mp_reduce_slice(i):
    """
    Effectively currying in order to
    - preserve the signature of reduce function,
    - function passed to map only takes one argument
    """

    isl = np.s_[i, :, :]

    return reduce(gx1, gx2, gx3[isl],
                  grho[isl] if grho is not None else None,
                  gvx1[isl] if gvx1 is not None else None,
                  gvx2[isl] if gvx2 is not None else None,
                  gvx3[isl] if gvx3 is not None else None,
                  gprs[isl] if gprs is not None else None,
                  gtr1[isl] if gtr1 is not None else None,
                  gtr2[isl] if gtr2 is not None else None)


def mp_reduce_init(x1, x2, x3, rho, v1, v2, v3, prs, tr1, tr2):
    """
    Called on each child process initialization.
    Create global variables for all big variables reduce_slice() depends on
    """

    global gx1, gx2, gx3, gvx1, gvx2, gvx3, grho, gprs, gtr1, gtr2
    gx1, gx2, gx3, gvx1, gvx2, gvx3 = x1, x2, x3, v1, v2, v3
    grho, gprs, gtr1, gtr2 = rho, prs, tr1, tr2


def reduce_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2):
    """
    Initialize reductions using multiple processes.
    """
    # out = mp.RawArray(ctypes.c_float, nproc)
    # out = tonumpyarray(out, nproc, par.dfile_dtype)
    #
    # wgt = mp.RawArray(ctypes.c_float, nproc)
    # wgt = tonumpyarray(out, nproc, par.dfile_dtype)

    # Start processes
    pool = mp.Pool(nproc,
                   initializer=mp_reduce_init,
                   initargs=(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2))

    # Slices and process numbers as arguments to worker function
    # sli = zip(list(slices(par.nx3, nproc)), range(nproc))
    sli = list(slices(par.nx3, nproc))

    # Perform reductions
    result = pool.map(mp_reduce_slice, sli)
    pool.close()
    pool.join()

    if reduction == 'sum':
        return result

    elif reduction in ['avg', 'var']:
        out = [r[0] for r in result]
        w = [r[1] for r in result]
        return out, w


def create_fname(basenm, var):

    fname = basenm + var

    return fname


def main(argv):
    """
    Driver routine
    :param argv:  see set_params function
    """

    # Get runfile and runname, dat_dir, inifile, setting
    global iom
    iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

    # Read settings parameter
    global par
    par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

    # Normalizations
    global kelvin, mu, nm
    mu = 0.60364
    kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
    nm = norm.PhysNorm(dens=mu*pc.amu, x=par.xdnorm, v=par.vdnorm, temp=kelvin, curr=1.)

    global time_code, power_code, pdot_code

    # Power in code units
    power_code = par.power / nm.epwr

    # Momentum injection rate in code units
    pdot_code = par.power / pc.c / nm.pdot

    # Create position space (the same for a given simulation)
    x1, x2, x3 = par.x_space()

    # The processes will be in sync so that we always have
    # the same time, theta, and phi at any given time
    # Automatically gets inherited (copied) to sub-processes
    global time

    times = range(par.time_i, par.time_f + 1, par.time_delta)

    # Loop over all time steps
    for time in times:

        print('time = ' + str(time))

        # Time in code units
        time_code = time * par.tdnorm / nm.t

        # Read data file for this timestep
        rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars + [par.weight]), time, [0, 0, 0])

        global pvar, ivar, reduction, average

        out_vars = [time]
        for ivar, pvar in enumerate(par.vars):

            reduction = get_reduction_type(pvar)

            # Do reduction
            # TODO: differentiate between multi-processing case and nproc = 1 case
            # TODO: In other programs, rather than using shared variables for small output use map output
            if reduction == 'sum':
                out = reduce_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2)
                result = np.sum(out)

            elif reduction == 'avg':
                out, w = reduce_mp(x1, x2, x3, rho, vx1 , vx2, vx3, prs, tr1, tr2)
                result = np.average(out, weights=w)

            elif reduction == 'var':
                reduction = 'avg'
                out, w, = reduce_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2)
                average = np.average(out, weights=w)
                reduction = 'var'
                out, w = reduce_mp(x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2)
                result = np.average(out, weights=w)
                result = np.sqrt(result)

            out_vars.append(result)

            print('pvar = ' + pvar)

        # Dump output file
        fname = iom.out_dir + '/' + create_fname(par.fbasenm, '-'.join(par.vars)) + '.red'
        with open(fname, 'ab') as fh:
            np.savetxt(fh, np.array(out_vars)[None, :])



if __name__ == "__main__":
    main(sys.argv[1:])
