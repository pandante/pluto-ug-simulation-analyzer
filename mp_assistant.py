import numpy as np
import multiprocessing as mp
import multiprocessing.pool


def tonumpyarray(mp_arr, shape, dtype):
    """
    Convert shared multiprocessing array to numpy array.
    These are only for mp.RawArrays.
    For mp.Arrays, need to use mp_arr.get_obj().

    Note, no data copying is performed here.
    """
    return np.frombuffer(mp_arr, dtype=dtype).reshape(shape)


def slices(nitems, mslices):
    """
    Split nitems into mslices pieces.

    >>> list(slices(10, 3))
    [slice(0, 4, 1), slice(4, 8, 1), slice(8, 10, 1)]
    >>> list(slices(1, 3))
    [slice(0, 1, 1), slice(1, 1, 1), slice(2, 1, 1)]
    """

    step = nitems // mslices + 1
    for i in range(mslices):
        yield slice(i * step, min(nitems, (i + 1) * step))


# From http://stackoverflow.com/questions/6974695/python-process-pool-non-daemonic
class NoDaemonProcess(mp.Process):
    # make 'daemon' attribute always return False
    def _get_daemon(self):
        return False
    def _set_daemon(self, value):
        pass
    daemon = property(_get_daemon, _set_daemon)


# We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
# because the latter is only a wrapper function, not a proper class.
class MyPool(mp.pool.Pool):
    Process = NoDaemonProcess
