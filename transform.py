import numpy as np
import scipy.interpolate as sip
import scipy.spatial.transform as stf
import multiprocessing as mp
import sys
import os
import configparser
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from mp_assistant import slices

# ______________________________________________________________________________________________________________________
# Description

# This script creates loads of slice files for a simulation
# To launch with test example, use parameters: -R 00deg-local -s test

nproc = 1

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here for the moment....

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # TODO: make ncpus a setting (also in other scripts)
        # Slice settings
        self.theta = confi.getboolean(setting, 'theta')
        self.phi = confi.getboolean(setting, 'phi')

        self.vars = confi.get(setting, 'vars').split()
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.slice_fbasenm = confi.get(setting, 'slice_fbasenm')


def create_fname(basenm, var, theta, phi, time):

    fname = basenm + var + '-th' + format(theta, '>03d') + '-ph' + format(theta, '>03d')'-' + format(intcpt, '>04d') + '-' + format(time, '>04d')

    return fname


def dump_slice(var, data, pl_axis, time):

    hfname = create_fname(par.transform_fbasenm, var, pl_axis, par.intercepts[pl_axis-1], time)
    hfname = iom.out_dir + '/' + hfname + '.' + par.dfile_ext

    np.savetxt(hfname, data)


def mp_slicer(times):

    # Loop over all timesteps
    for itime, time in enumerate(times):

        # Data variables
        dvars = ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr1', 'tr2']

        # Loop over all data variables
        for ivar, dvar in enumerate(dvars):

            # If data variable is required add to output, otherwise leave it as None
            if dvar in par.vars:

                # Read data slice
                scratch = par.read_var(dvar, time)

                if par.axis3:
                    dump_slice(dvar, scratch[par.ax3_intcpt, :, :], 3, time)

                if par.axis2:
                    dump_slice(dvar, scratch[:, par.ax2_intcpt, :], 2, time)

                if par.axis1:
                    dump_slice(dvar, scratch[:, :, par.ax1_intcpt], 1, time)

                del scratch


# ______________________________________________________________________________________________________________________
# Main bit


# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

# All time steps and number of time steps
all_times = range(par.time_i, par.time_f + 1, par.time_delta)
nt = len(all_times)

# Processes
if nproc > 1:
    pool = mp.Pool(nproc)
    pool.map(mp_slicer, [all_times[i] for i in slices(nt, nproc)])
    pool.close()
    pool.join()

else:
    mp_slicer(all_times)


