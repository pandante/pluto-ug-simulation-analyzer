import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl

check = False

if check:
    ndec = 1
else:
    ndec = 8

nc = 64 * ndec
fnum = 203
shp = (nc, nc, nc)
ncells = nc ** 3
dt = 'float32'
dvb = np.dtype(dt).alignment * ncells

irho = 0
ivx1 = 1
ivx2 = 2
ivx3 = 3
iprs = 4
itr1 = 5
itr2 = 6

fh = open('data.' + format(fnum, '>04d') + ('-s' if check else '') + '.flt', 'r')

fh.seek(irho * dvb)
rho = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(ivx1 * dvb)
vx1 = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(ivx2 * dvb)
vx2 = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(ivx3 * dvb)
vx3 = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(iprs * dvb)
prs = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(itr1 * dvb)
tr1 = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.seek(itr2 * dvb)
tr2 = np.fromfile(fh, dt, ncells).reshape(shp)[::ndec, ::ndec, ::ndec]

fh.close()

if check:
    pl.imshow(np.log10(rho[:, nc/2, :]))
    pl.colorbar()
    pl.savefig('rho-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(vx1[:, nc/2, :])
    pl.colorbar()
    pl.savefig('vx1-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(vx2[:, nc/2, :])
    pl.colorbar()
    pl.savefig('vx2-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(vx3[:, nc/2, :])
    pl.colorbar()
    pl.savefig('vx3-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(np.log10(prs[:, nc/2, :]))
    pl.colorbar()
    pl.savefig('prs-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(tr1[:, nc/2, :])
    pl.colorbar()
    pl.savefig('tr1-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

    pl.imshow(tr2[:, nc/2, :])
    pl.colorbar()
    pl.savefig('tr2-' + format(fnum, '>04d') + '-s.png')
    pl.close(pl.gcf())

else:
    out = np.r_[rho.ravel(),
                vx1.ravel(),
                vx2.ravel(),
                vx3.ravel(),
                prs.ravel(),
                tr1.ravel(),
                tr2.ravel()]

    out.tofile('data.' + format(fnum, '>04d') + '-s.flt')

