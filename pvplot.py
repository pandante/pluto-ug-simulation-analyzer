import numpy as np
import numpy.ma as ma
import multiprocessing as mp
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import matplotlib.colors as colors
# from mpl_toolkits.axes_grid1 import make_axes_locatable
import sys
import os
import physconst as pc
import subprocess
import configparser
from pluto_ug_sim import PlutoUGSim
from io_manager import IOManager
from mp_assistant import slices
import seaborn
# seaborn.set_style('darkgrid', {'grid.color': '0.93', 'figure.facecolor': 'black'})
seaborn.set(rc={'grid.color': '0.5', 'figure.facecolor': '0.2'})
seaborn.set(font_scale=1.5)


# pl.ion()

# ______________________________________________________________________________________________________________________
# Description

# To launch with test example, use: -R 00deg -s a-127 pvplots/00deg pvplots/00deg

nproc = 1

# ______________________________________________________________________________________________________________________
# For debug

# Nothing here at the moment


# ______________________________________________________________________________________________________________________
# Functions

class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Plot settings
        self.vars = confi.get(setting, 'vars').split()
        self.npvars = len(self.vars)
        self.hide = self.multi_getboolean(confi, setting, 'hide', self.npvars)

        self.x_slit_min = confi.getfloat(setting, 'x_slit_min')
        self.x_slit_max = confi.getfloat(setting, 'x_slit_max')
        self.v_los_min = confi.getfloat(setting, 'v_los_min')
        self.v_los_max = confi.getfloat(setting, 'v_los_max')
        self.vnorm = confi.getfloat(setting, 'vnorm')

        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.theta_i = confi.getint(setting, 'theta_i')
        self.theta_f = confi.getint(setting, 'theta_f')
        self.theta_delta = confi.getint(setting, 'theta_delta')

        self.phi_i = confi.getint(setting, 'phi_i')
        self.phi_f = confi.getint(setting, 'phi_f')
        self.phi_delta = confi.getint(setting, 'phi_delta')

        self.nbins_x = confi.getint(setting, 'nbins_x')
        self.nbins_v = confi.getint(setting, 'nbins_v')
        self.nbins = [self.nbins_x, self.nbins_v]

        self.vmin = self.multi_getfloat(confi, setting, 'vmin', self.npvars)
        self.vmax = self.multi_getfloat(confi, setting, 'vmax', self.npvars)
        self.contour_add = confi.getint(setting, 'contour_add')
        self.rescale = self.multi_getboolean(confi, setting, 'rescale', self.npvars)

        self.add_time = confi.getboolean(setting, 'add_time')
        self.plot_rotation_curve = confi.getboolean(setting, 'plot_rotation_curve')

        self.rho_min = self.multi_getfloat(confi, setting, 'rho_min', self.npvars)
        self.rho_max = self.multi_getfloat(confi, setting, 'rho_max', self.npvars)
        self.te_min = self.multi_getfloat(confi, setting, 'te_min', self.npvars)
        self.te_max = self.multi_getfloat(confi, setting, 'te_max', self.npvars)
        self.tr1_min = self.multi_getfloat(confi, setting, 'tr1_min', self.npvars)
        self.tr2_min = self.multi_getfloat(confi, setting, 'tr2_min', self.npvars)

        self.colorbar = confi.getboolean(setting, 'colorbar')
        self.x_labels = confi.getboolean(setting, 'x_labels')
        self.y_labels = confi.getboolean(setting, 'y_labels')

        self.output = confi.get(setting, 'output').split()
        self.make_movie = confi.getboolean(setting, 'make_movie')

        self.hist_fbasenm = confi.get(setting, 'hist_fbasenm')
        self.plot_fbasenm = confi.get(setting, 'plot_fbasenm')


def create_fname(pvar, ivar, basenm, ph, th, time=None):

    # Add binning resolution
    basenm += '-' + pvar + '-' + format(par.nbins_x, '>03d') + 'x' + format(par.nbins_v, '>03d')

    # Cut labels
    # TODO: this would need to be included again if we have a better threshold framework
    # if par.tr1_min[ivar] > 0.:
    #     basenm += '-j'
    #
    # if par.rho_min[ivar] > 0.:
    #     basenm += '-n' + format(par.rho_min[ivar], '0>4.0f')
    #
    # if par.te_max[ivar] > 0.:
    #     basenm += '-t' + format(par.te_max[ivar], '0>4.0f')

    # Time
    if time is None:
        pass

    elif time == 'movie':
        basenm += '-%4d'

    else:
        basenm += '-' + format(time, '>04d')

    # Theta, and phi
    basenm += '-' + format(th, '>03d') + '-' + format(ph, '>03d')

    return basenm


def movie(pvar, ivar, phi, theta):

    ifname = iom.out_dir + '/' + create_fname(pvar, ivar, par.plot_fbasenm, phi, theta, 'movie') + '.png'
    mfname = iom.out_dir + '/' + create_fname(pvar, ivar, par.plot_fbasenm, phi, theta) + '.mp4'
    cmd = 'ffmpeg -start_number ' + str(par.time_i) + ' -i ' + ifname
    cmd += ' -y -vcodec mpeg4 -framerate 25 -filter:v "setpts=3.0*PTS" -b:v 18000000 '
    cmd += mfname
    print('\n')
    print('Generating movie with following command... ')
    print(cmd)
    print('\n')

    # subprocess.call(cmd, shell=True)

    fm = open(mfname + '.make', 'w')
    fm.write(cmd)
    fm.close()
    subprocess.call(['chmod 775 ' + mfname + '.make'], shell=True)

    subprocess.call(['./' + mfname + '.make'], shell=True)


def mp_pvplot(times):

    # Loop over all timesteps
    for time in times:

        pt_vars = [v for i, v in enumerate(par.vars) if par.hide[i] == 0]
        ipt_vars = [i for i, v in enumerate(par.vars) if par.hide[i] == 0]

        for ivar, pvar in zip(ipt_vars, pt_vars):

            vmin = None if par.vmin[ivar] == par.vmax[ivar] else par.vmin[ivar]
            vmax = None if par.vmin[ivar] == par.vmax[ivar] else par.vmax[ivar]
            vrange = np.abs(par.vmin[ivar]) if vmin is None else vmax - vmin
            rescale = par.rescale[ivar]

            # Loop over disk tilt angles.
            for theta in range(par.theta_i, par.theta_f + 1, par.theta_delta):
                for phi in range(par.phi_i, par.phi_f + 1, par.phi_delta):

                    hfname = iom.data_dir + '/' + create_fname(pvar, ivar, par.hist_fbasenm, phi, theta,
                                                               time) + '.hst'

                    # Read file and flip for plotting
                    hd = np.loadtxt(hfname)
                    hd = hd[:, ::-1]

                    # plot histogram and contour plot
                    his = ma.log10(hd.T)
                    # his = np.where(np.isnan(his), 0, his)
                    # his -= par.vmin[ivar]

                    fig, ax = pl.subplots(1, 1, figsize=pl.figaspect(5./7.))

                    # If par.vmin = par.vmax, either rescale such that colormap covers
                    # a) vmax - vrange -> vmax, if par.vmin > 0, or
                    # b) vmin -> vmin + vrange
                    # The value of abs(par.vmin) = abs(par.vmin) is used for vrange
                    # TODO: Do we really need the float casting here?
                    his = np.ma.masked_invalid(his)
                    if rescale:
                        if vmin is None:
                            if par.vmin[ivar] > 0:
                                his -= np.nanmax(his) - vrange
                            elif par.vmin[ivar] < 0:
                                his -= np.nanmin(his)
                            else:
                                his -= np.nanmin(his)
                                vrange = np.nanmax(his)
                        else:
                            his -= np.float(vmin)
                        vmin, vmax = 0., vrange

                    else:
                        if vmin is None:
                            if par.vmin[ivar] > 0:
                                vmax = np.nanmax(his)
                                vmin = vmax - vrange
                            elif par.vmin[ivar] < 0:
                                vmin = np.nanmin(his)
                                vmax = vmin + vrange
                            else:
                                vmin = np.nanmin(his)
                                vmax = np.nanmax(his)
                                vrange = vmax - vmin

                    # Plot a background
                    pl.imshow(0.8 * np.ones((par.nbins_x, par.nbins_v)), vmin=0, vmax=1,
                              extent=(h1[0], h1[-1], h2[0], h2[-1]),
                              aspect='auto', cmap=cm.Greys)

                    # Plot the histogram
                    pl.imshow(his[::-1, :], vmin=vmin, vmax=vmax, extent=(h1[0], h1[-1], h2[0], h2[-1]),
                              aspect='auto', cmap=cm.Oranges, zorder=10)

                    # Control colorbar. Colorbar is added and removed so that the axes retain desired aspect ratio.
                    cbp = pl.colorbar(pad=0.02, aspect=30)
                    if not par.colorbar:
                        fig.delaxes(fig.axes[1])

                    # Modify colormap for contours
                    # TODO: add this to your colormap tools (and learn from it)
                    def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
                        new_cmap = colors.LinearSegmentedColormap.from_list(
                            'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
                            cmap(np.linspace(minval, maxval, n)))
                        return new_cmap

                    new_cmap = truncate_colormap(cm.pink, 0.3, 0.9)

                    # Contour plot
                    ncont = 2 * vrange + par.contour_add
                    cont = np.linspace(vmin, vmax, ncont)
                    pl.contour(h1, h2, his, cont, vmin=vmin, vmax=vmax, cmap=new_cmap, linewidths=1, zorder=11)

                    # Colorbar for contour plot
                    if par.colorbar:
                        cbc = pl.colorbar(cax=cbp.ax)
                        cbc.set_label(r'$\log$(brightness) [ arbitrary units ]')

                    # Control axis labels
                    if par.x_labels:
                        pl.xlabel(r'$X$ [ kpc ]')
                    else:
                        pl.tick_params(labelbottom=False)

                    if par.y_labels:
                        pl.ylabel(r'$v$ [ km s$^{-1}$ ]')
                    else:
                        pl.tick_params(labelleft=False)
                        pl.xlim(par.x_slit_min * 0.99, par.x_slit_max)

                    # Plot rotation curve
                    if par.plot_rotation_curve:


                        def hernquist_potential(r, mass, a):
                            """
                            :param r:       spherical radius in kpc
                            :param mass:    in Msun
                            :param a:       scale height in kpc
                            :return:        potential in (km / s)^2
                            """

                            phi = -pc.newton * mass * pc.msun / ((r + a) * pc.kpc)

                            return phi * 1.e-10

                        def hernquist_circ_vel(r, mass, a, theta=90):
                            """
                            :param r:       cylindrical radius in kpc
                            :param mass:    total mass in Msun
                            :param a:       scale height in kpc
                            :param theta:   viewing angle in deg
                            :return:        circular velocity in km / s
                            """
                            roa = r / a
                            vcirc = (np.sqrt(pc.newton * mass * pc.msun / (a * pc.kpc)) *
                                     np.sqrt(roa) / (1. + roa) * np.sin(np.deg2rad(theta)))
                            return vcirc * 1.e-5

                        def isothermal_disc(phi, phi_r0, phi_00, rho0, wturb, rot=0.9):
                            """
                            :param phi:      potential (r, z) in (km / s)^2
                            :param phi_r0:   potential (r, 0) in (km / s)^2  (must be correctly broadcastable with phi)
                            :param phi_00:   potential (0, 0) in (km / s)^2  (must be correctly broadcastable with phi)
                            :param rho0:     in g / cm^3
                            :param wturb:    in km / s
                            :param rot:      rotational parameter (no units)
                            :return:         density as a function of r in g / cm^3
                            """
                            rho = rho0 * np.exp(
                                -(phi - rot * rot * phi_r0 - (1. - rot * rot) * phi_00) / (wturb * wturb))

                            return rho

                        def project_rotation_curve():

                            # Number of cells
                            nx1 = nx2 = nx3 = 100

                            # Cell centers
                            x1l = np.linspace(par.x1min, par.x1max, nx1, endpoint=False) + par.dx1 / 2.
                            x2l = np.linspace(par.x2min, par.x2max, nx2, endpoint=False) + par.dx2 / 2.
                            x3l = np.linspace(par.x3min, par.x3max, nx3, endpoint=False) + par.dx3 / 2.

                            # Make broadcastable for 3D calculations
                            x1 = x1l[None, None, :]
                            x2 = x2l[None, :, None]
                            x3 = x3l[:, None, None]

                            # Make broadcastable for 3D calculations
                            # x1, x2, x3 = par.x_space()

                            # Create various distance variables, making sure they have shape (nx3, nx2, nx1)
                            cyl_rad = np.sqrt(x1 * x1 + x2 * x2 + np.zeros_like(x3))
                            sph_rad = np.sqrt(x1 * x1 + x2 * x2 + x3 * x3)
                            height = x3 + np.zeros_like(x1) + np.zeros_like(x2)

                            # Circular velocity projected onto x1-x3 plane
                            circ_vel = hernquist_circ_vel(cyl_rad, 1.67e11, 2.8) * x1 / cyl_rad

                            # Hernquist Density profile (used for weighting). Cut out a cylindrical volume.
                            # rho = hernquist_density(sph_rad, 1.67e11, 2.8)
                            # rho = np.where(np.less(cyl_rad, 1.), rho, 1.e-30)
                            # rho = np.where(np.less(np.absolute(height), 0.222), rho, 1.e-30)

                            # Average over line of sight = y-axis = axis = 1, then across z-axis = axis = 0.
                            # cvav_1, rhosum_1 = np.average(circ_vel, axis=1, weights=rho, returned=True)
                            # cvav_s, rhosum_s = np.average(cvav_1, axis=0, weights=rhosum_1, returned=True)

                            # Isothermal density profile
                            phi = hernquist_potential(sph_rad, 1.67e11, 2.8)
                            phi_r0 = hernquist_potential(cyl_rad, 1.67e11, 2.8)
                            phi_00 = phi[0, 0, 0]
                            rho = isothermal_disc(phi, phi_r0, phi_00, 200., 40, 0.9)
                            rho = np.where(np.less(cyl_rad, 1.), rho, 1.e-30)
                            rho = np.where(np.less(np.absolute(height), 0.222), rho, 1.e-30)

                            # Average over line of sight = y-axis = axis = 1, then across z-axis = axis = 0.
                            icvav_1, irhosum_1 = np.average(circ_vel, axis=1, weights=rho, returned=True)
                            icvav_s, irhosum_s = np.average(icvav_1, axis=0, weights=irhosum_1, returned=True)

                            # Same but for density squared
                            # icvav2_1, irhosum2_1 = np.average(circ_vel, axis=1, weights=rho * rho, returned=True)
                            # icvav2_s, irhosum2_s = np.average(icvav2_1, axis=0, weights=irhosum2_1, returned=True)

                            return x1[0, 0, :], icvav_s

                        # Analytic, non-projected rotation curve
                        slitx = np.linspace(0, 1, 173)
                        slitv = hernquist_circ_vel(slitx, 1.67e11, 2.8)
                        slitx = np.r_[-slitx[::-1], slitx]
                        slitv = np.r_[slitv[::-1], -slitv]
                        pl.plot(slitx, slitv, 'w--', label=r'$\theta=90$', zorder=200)

                        # Plot projected rotation curve
                        slitx, slitv = project_rotation_curve()
                        pl.plot(slitx, -slitv, 'w-', zorder=200)

                    # Time in plot
                    if par.add_time:
                        time_text = r'$t = $' + format(par.tdnorm / pc.Myr * (time - par.time_j), '>4.2f') + ' Myr'
                        pl.text(0.7, 1.02, time_text, color='k', transform=pl.gca().transAxes)

                    # Save figure
                    for ext in par.output:
                        ifname = iom.out_dir + '/' + create_fname(pvar, ivar, par.plot_fbasenm, phi, theta,
                                                                  time) + '.' + ext
                        pl.savefig(ifname, bbox_inches='tight', dpi=300)

                    pl.close(pl.gcf())


# ______________________________________________________________________________________________________________________
# Main bit


# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

# All time steps and number of time steps
times = range(par.time_i, par.time_f + 1)
nt = len(times)

# Reconstruct histogram bin centers
h1 = np.linspace(par.x_slit_min, par.x_slit_max, par.nbins_x + 1)
h2 = np.linspace(par.v_los_min, par.v_los_max, par.nbins_v + 1)
h1 = (h1[:-1] + h1[1:]) / 2.
h2 = (h2[:-1] + h2[1:]) / 2.


# All time steps and number of time steps
all_times = range(par.time_i, par.time_f + 1, par.time_delta)
nt = len(all_times)
times_slices = list(slices(nt, nproc))

# Processes
if nproc > 1:
    pool = mp.Pool(nproc)
    pool.map(mp_pvplot, [all_times[i] for i in times_slices])
    pool.close()
    pool.join()

else:
    mp_pvplot(all_times)


if par.make_movie:

    pt_vars = [v for i, v in enumerate(par.vars) if par.hide[i] == 0]
    ipt_vars = [i for i, v in enumerate(par.vars) if par.hide[i] == 0]

    for ivar, pvar in zip(ipt_vars, pt_vars):

        # Loop over disk tilt angles.
        for theta in range(par.theta_i, par.theta_f + 1, par.theta_delta):
            for phi in range(par.phi_i, par.phi_f + 1, par.phi_delta):
                movie(pvar, ivar, phi, theta)
