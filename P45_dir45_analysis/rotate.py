
import numpy as np
from scipy.interpolate import RegularGridInterpolator
from scipy.spatial.transform import Rotation as R
# import multiprocessing as mp
# import ctypes
# from mp_assistant import slices, tonumpyarray
import sys
import os
import tools.norm as norm
import configparser
import tools.physconst as pc
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from tools.expressions import need_vars
import tools.rotate as rotate

# TODO: consider file reading as in https://stackoverflow.com/questions/60493766/read-binary-flatfile-and-skip-bytes


# ______________________________________________________________________________________________________________________
# Description

# This script creates rotated data cubes of a full PLUTO data set
# A shared memory version is not yet available.

# To launch use something like:
#
# python project.py -r runs.ini -R <run> -i project.ini -s <setting> [data_dir] [out_dir]
#
# where   <run> is configuration section name in runs.ini
#         <setting> is configuration section name in project.ini

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here at the moment

# ______________________________________________________________________________________________________________________
# Global settings

# Number of processors
nproc = 1

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # TODO: Some scripts use multi-get for _min and _maxs (e.g. slplot). Make consistent.
        #       In principle, only one value should be used for consistency, but I can
        #       imagine cases where changing values may be useful, e.g. testing.

        # Project settings
        self.vars = confi.get(setting, 'vars').split()
        self.rot1_i = confi.getfloat(setting, 'rot1_i')
        self.rot1_f = confi.getfloat(setting, 'rot1_f')
        self.rot1_delta = confi.getfloat(setting, 'rot1_delta')
        self.rot2_i = confi.getfloat(setting, 'rot2_i')
        self.rot2_f = confi.getfloat(setting, 'rot2_f')
        self.rot2_delta = confi.getfloat(setting, 'rot2_delta')
        self.rot3_i = confi.getfloat(setting, 'rot3_i')
        self.rot3_f = confi.getfloat(setting, 'rot3_f')
        self.rot3_delta = confi.getfloat(setting, 'rot3_delta')

        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.rot_data_reduction = confi.getfloat(setting, 'rot_data_reduction')
        self.rot_limits = confi.get(setting, 'rot_limits')
        self.rot_zoom = confi.getfloat(setting, 'rot_zoom')



def main(argv):
    """
    Driver routine
    :param argv:  see set_params function
    """

    # Get runfile and runname, dat_dir, inifile, setting
    global iom
    iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

    # Read settings parameter
    global par
    par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

    # Normalizations
    global nm
    mu = 0.60364
    mmpp = mu * pc.amu
    kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
    nm = norm.PhysNorm(x=par.xdnorm, v=par.vdnorm, dens=mmpp, curr=1., temp=kelvin)

    # The processes will be in sync so that we always have
    # the same time, theta, and phi at any given time
    # Automatically gets inherited (copied) to sub-processes
    global time

    # Set x, y ,z here

    # Loop over all time steps
    for time in range(par.time_i, par.time_f + 1, par.time_delta):

        for rot1 in np.arange(par.rot1_i, par.rot1_f + 1., par.rot1_delta):
            for rot2 in np.arange(par.rot2_i, par.rot2_f + 1., par.rot2_delta):
                for rot3 in np.arange(par.rot3_i, par.rot3_f + 1., par.rot3_delta):

                    # Read data file for this timestep
                    # Do this for every rotation in order to reset these variables and reduce memory footprint
                    rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data(need_vars(par.vars), time)

                    rotvec_1 = [np.deg2rad(rot1), 0, 0]
                    rotvec_2 = [0, np.deg2rad(rot2), 0]
                    rotvec_3 = [0, 0, np.deg2rad(rot3)]
                    rot_1 = R.from_rotvec(rotvec_1)
                    rot_2 = R.from_rotvec(rotvec_2)
                    rot_3 = R.from_rotvec(rotvec_3)
                    irot_1 = rot_1.inv()
                    irot_2 = rot_2.inv()
                    irot_3 = rot_3.inv()

                    # Rotate velocity vectors
                    v = np.moveaxis(np.array([np.ravel(vx1), np.ravel(vx2), np.ravel(vx3)]), 0, -1)
                    v = rot_1.apply(v)
                    v = rot_2.apply(v)
                    v = rot_3.apply(v)
                    vx1 = v[:, 0].reshape(par.nx3, par.nx2, par.nx1)
                    vx2 = v[:, 1].reshape(par.nx3, par.nx2, par.nx1)
                    vx3 = v[:, 2].reshape(par.nx3, par.nx2, par.nx1)

                    # Free memory
                    del v

                    intp_func_rho = RegularGridInterpolator((par.x3, par.x2, par.x1), rho, bounds_error=False, fill_value=0)
                    intp_func_vx1 = RegularGridInterpolator((par.x3, par.x2, par.x1), vx1, bounds_error=False, fill_value=0)
                    intp_func_vx2 = RegularGridInterpolator((par.x3, par.x2, par.x1), vx2, bounds_error=False, fill_value=0)
                    intp_func_vx3 = RegularGridInterpolator((par.x3, par.x2, par.x1), vx3, bounds_error=False, fill_value=0)
                    intp_func_prs = RegularGridInterpolator((par.x3, par.x2, par.x1), prs, bounds_error=False, fill_value=0)
                    intp_func_tr1 = RegularGridInterpolator((par.x3, par.x2, par.x1), tr1, bounds_error=False, fill_value=0)
                    intp_func_tr2 = RegularGridInterpolator((par.x3, par.x2, par.x1), tr2, bounds_error=False, fill_value=0)

                    del rho
                    del vx1
                    del vx2
                    del vx3
                    del prs
                    del tr1
                    del tr2

                    # Grid to be interpolated on
                    # Create the ideal grid with rotated_axes, then inverse rotate points
                    # TODO: we should really rotate the nodal grid so as to obatin the correct
                    x1, x2, x3 = rotate.rotated_axes(par.x1, par.x2, par.x3, [rot_1, rot_2, rot_3],
                                                     par.rot_data_reduction, par.rot_limits, par.rot_zoom)
                    nx1, nx2, nx3 = len(x1), len(x2), len(x3)
                    # Note, order of points doesn't matter for rotation, but interpolation is a function of (x3, x2, x1) so keep that order.
                    x3g, x2g, x1g = np.meshgrid(x3, x2, x1, indexing='ij')
                    # Note, rotations are right-handed and the position vectors are [x, y, z]
                    xg = np.moveaxis(np.array([np.ravel(x1g), np.ravel(x2g), np.ravel(x3g)]), 0, -1)
                    xg = irot_3.apply(xg)
                    xg = irot_2.apply(xg)
                    xg = irot_1.apply(xg)
                    xg = xg.reshape(nx3, nx2, nx1, 3)
                    # Swap coordinate 1 and 3
                    xg[:, :, :, [0, 2]] = xg[:, :, :, [2, 0]]
                    # del x1g
                    # del x2g
                    # del x3g

                    # Interpolate
                    rho = intp_func_rho(xg)
                    vx1 = intp_func_vx1(xg)
                    vx2 = intp_func_vx2(xg)
                    vx3 = intp_func_vx3(xg)
                    prs = intp_func_prs(xg)
                    tr1 = intp_func_tr1(xg)
                    tr2 = intp_func_tr2(xg)

                    del xg
                    del intp_func_rho
                    del intp_func_vx1
                    del intp_func_vx2
                    del intp_func_vx3
                    del intp_func_prs
                    del intp_func_tr1
                    del intp_func_tr2

                    fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir)
                    np.array([rho, vx1, vx2, vx3, prs, tr1, tr2]).astype(np.float32).tofile(fname)

                    print(f'time = {time}, rot1 = {int(rot1)}, rot2 = {int(rot2)}, rot3 = {int(rot3)}')


if __name__ == "__main__":
    main(sys.argv[1:])

