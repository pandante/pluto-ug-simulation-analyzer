import numpy as np
import tools.grid_dot_out_reader as gdor
from pyevtk.hl import gridToVTK

# This script exports a flt file to a vtk file for VisIt to read

# Set these
infile = 'rho-full.0236.flt'
outfile = 'rho-full.0236'
grid_file = 'grid.out'
nvar = 7

# Get cell center arrays from grid file
x1d, x2d, x3d = gdor.read(grid_file, 'nodal')
nx1, nx2, nx3 = len(x1d) - 1, len(x2d) - 1, len(x3d) - 1

# Read and dump data
rho = np.log10(np.fromfile('rho-full.0236.flt', dtype=np.float32).reshape(nx3, nx2, nx1))
gridToVTK(f'{outfile}', x3d, x2d, x1d, cellData = {'rho': rho.astype(np.float64)})
