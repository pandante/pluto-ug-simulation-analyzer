import matplotlib.cm as cm
import matplotlib.pyplot as pl
import numpy as np

import tools.mpl_aesth as mpla
import tools.physconst as pc

mpla.adjust_rcParams(use_kpfonts=True)

sky = 'xy'
do_pv = True

nx1, nx2, nx3 = 672, 672, 544
x1_min, x1_max = -2, 2
x2_min, x2_max = -2, 2
x3_min, x3_max = -1.632, 1.632
off_min, off_max = -2, 2
v_min, v_max = -700, 700
v_norm = pc.c / 1.e5
slit_height = 0.45 / 2.
rho_cut = 10.
log_rho_vmin, log_rho_vmax = -8, 6
pv_log_dyn_range = 3
pv_nbins = 256
pv_ncont = 0

rho = np.fromfile('data_for_paper/rho-disc.0236.flt', dtype=np.float32).reshape(nx3, nx2, nx1)

x1 = np.linspace(x1_min, x1_max, nx1)
x2 = np.linspace(x2_min, x2_max, nx2)
x3 = np.linspace(x3_min, x3_max, nx3)
x3g, x2g, x1g = np.meshgrid(x3, x2, x1, indexing='ij')

if sky == 'xz':

    pl.imshow(np.log(rho[:, int(nx2 / 2), :]), extent=[x1_min, x1_max, x3_min, x3_max], origin='lower', aspect='equal',
              cmap=cm.gray_r, vmin=log_rho_vmin, vmax=log_rho_vmax)

    rho = np.where(np.fabs(x1g - x3g) < slit_height * np.sqrt(2), rho, 0)

    pl.imshow(np.log(rho[:, int(nx2 / 2), :]), extent=[x1_min, x1_max, x3_min, x3_max], origin='lower', aspect='equal',
              cmap=cm.Spectral_r, vmin=log_rho_vmin, vmax=log_rho_vmax)
    pl.xlabel(rf'${sky[0]}$ [ kpc ]')
    pl.ylabel(rf'${sky[1]}$ [ kpc ]')
    cb = pl.colorbar(aspect=40, pad=0.01)
    cb.set_label('$n$ [ cm$^{-3}$ ]')
    pl.savefig(f'density_slit_{sky}.pdf', bbox_inches='tight')
    pl.close(pl.gcf())

    if do_pv:
        vx2 = np.fromfile('data_for_paper/vx2-disc.0236.flt', dtype=np.float32).reshape(nx3, nx2, nx1) * v_norm

        rho = np.where(rho > rho_cut, rho, 0)
        cyl_rad = np.sqrt(x1g * x1g + x2g * x2g)
        rho = np.where(np.logical_or(np.fabs(x3g) > 0.5, np.logical_and(cyl_rad > 1.5, cyl_rad < 1.8)), rho, 0)

        off = np.sqrt(2.) / 2. * (x1g + x3g)

        del x1g
        del x2g
        del x3g

        h = np.histogram2d(np.ravel(off), -np.ravel(vx2), pv_nbins, range=[[x1_min, x1_max], [v_min, v_max]],
                           weights=np.ravel(rho))

        del vx2
        del off
        del rho

        log_h0min = np.log10(h[0].max()) - pv_log_dyn_range

        pl.imshow(np.log10(h[0]).T - log_h0min, aspect='auto', origin='lower', cmap=cm.pink,
                  extent=[x1_min, x1_max, v_min, v_max], vmin=0.0, vmax=pv_log_dyn_range)
        pl.gca().set_facecolor('k')
        cb = pl.colorbar(aspect=40, pad=0.01)
        pl.contour(h[1][:-1], h[2][:-1], np.log10(h[0].T) - log_h0min, np.linspace(0, pv_log_dyn_range, pv_ncont), cmap=cm.gray_r,
                   linewidths=0.5, vmin=0.0, vmax=pv_log_dyn_range)

        pl.xlabel(r'offset [ kpc ]')
        pl.ylabel(r'$v_\mathrm{LOS}$ [ km s$^{-1}$ ]')

        cb.set_label('log mass [ arbitrary units ]')
        pl.savefig(f'pvplot_{sky}.pdf', bbox_inches='tight')

elif sky == 'xy':

    pl.imshow(np.log(rho[int(nx3 / 2), :, :]), extent=[x1_min, x1_max, x2_min, x2_max], origin='lower', aspect='equal',
              cmap=cm.gray_r, vmin=log_rho_vmin, vmax=log_rho_vmax)

    rho = np.where(np.fabs(x2g) < slit_height, rho, 0)

    pl.imshow(np.log(rho[int(nx3 / 2), :, :]), extent=[x1_min, x1_max, x2_min, x2_max], origin='lower', aspect='equal',
              cmap=cm.Spectral_r, vmin=log_rho_vmin, vmax=log_rho_vmax)
    pl.xlabel(rf'${sky[0]}$ [ kpc ]')
    pl.ylabel(rf'${sky[1]}$ [ kpc ]')
    cb = pl.colorbar(aspect=40, pad=0.01)
    cb.set_label('$n$ [ cm$^{-3}$ ]')
    pl.savefig(f'density_slit_{sky}.pdf', bbox_inches='tight')
    pl.close(pl.gcf())

    if do_pv:
        vx3 = np.fromfile('data_for_paper/vx3-disc.0236.flt', dtype=np.float32).reshape(nx3, nx2, nx1) * v_norm

        rho = np.where(rho > rho_cut, rho, 0)
        # rho = np.where(x3g > 0.5, rho, 0)
        # rho = np.where(x3g < -0.5, rho, 0)

        off = x1g

        del x1g
        del x2g
        del x3g

        h = np.histogram2d(np.ravel(off), np.ravel(vx3), pv_nbins, range=[[x1_min, x1_max], [v_min, v_max]],
                           weights=np.ravel(rho))

        del vx3
        del off
        del rho

        log_h0min = np.log10(h[0].max()) - pv_log_dyn_range

        pl.imshow(np.log10(h[0]).T - log_h0min, aspect='auto', origin='lower', cmap=cm.pink,
                  extent=[x1_min, x1_max, v_min, v_max], vmin=0.0, vmax=pv_log_dyn_range)
        pl.gca().set_facecolor('k')
        cb = pl.colorbar(aspect=40, pad=0.01)
        pl.contour(h[1][:-1], h[2][:-1], np.log10(h[0].T) - log_h0min, np.linspace(0, pv_log_dyn_range, pv_ncont), cmap=cm.gray_r,
                   linewidths=0.5, vmin=0.0, vmax=pv_log_dyn_range)

        pl.xlabel(r'offset [ kpc ]')
        pl.ylabel(r'$v_\mathrm{LOS}$ [ km s$^{-1}$ ]')

        cb.set_label('log mass [ arbitrary units ]')
        pl.savefig(f'pvplot_{sky}.pdf', bbox_inches='tight')
