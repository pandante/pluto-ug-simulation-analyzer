import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import tools.mpl_aesth as mpla
import tools.physconst as pc
import tools.grid_dot_out_reader as gdor
from scipy.interpolate import RegularGridInterpolator

mpla.adjust_rcParams(use_kpfonts=False, grid=False)

grid_file = 'grid.out'
x1n, x2n, x3n = gdor.read(grid_file, 'nodal')
x1, x2, x3 = gdor.read(grid_file, 'zonal')
nx1 = x1.size
nx2 = x2.size
nx3 = x3.size
x1min = np.min(x1n)
x1max = np.max(x1n)
x2min = np.min(x2n)
x2max = np.max(x2n)
x3min = np.min(x3n)
x3max = np.max(x3n)
dx1 = x1n[1:] - x1n[:-1]
dx2 = x2n[1:] - x2n[:-1]
dx3 = x3n[1:] - x3n[:-1]
dx = np.min(np.r_[dx1, dx2, dx3])
nxi1 = int((x1max - x1min) / dx) + 1
nxi2 = int((x2max - x2min) / dx) + 1
nxi3 = int((x3max - x3min) / dx) + 1
xi1 = np.linspace(x1min, x1max, nxi1)
xi2 = np.linspace(x2min, x2max, nxi2)
xi3 = np.linspace(x3min, x3max, nxi3)
x3ig, x1ig = np.meshgrid(xi3, xi1, indexing='ij')
xig = np.moveaxis(np.array([np.ravel(x3ig), np.ravel(x1ig)]), 0, -1).reshape((nxi3, nxi1, 2))

run = 'slices'
run = './'
pfx = 'sl-rho-ax2-0336-'
time_j = 17
# times = [[17, 37, 100, 150, 236]]
times = [[17, 37, 80, 100]]
# times = [[100]]
# times = [list(range(50, 231, 10))]
sfx = '.sli'
scale_bar_label = '1.2 kpc'
vmin, vmax = -4, 4
vnorm = 1.
cmin, cmax, cstp = -4, 5, 2
cmap = cm.Spectral_r
cb_label = r'$\log{} n$ [ cm$^{-3}$ ]'
tnorm = 3 * pc.kpc / pc.c / pc.Myr
time_units = 'Myr'
scale_units = 'kpc'

def set_axis_spine_color(ax, color):
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)


nrow = len(times)
ncol = len(times[0])
fig_scale = 4.0
fig_ratio = 2
fig_adj = 2.02

fig = pl.figure(figsize=(ncol*fig_scale/fig_ratio, nrow*fig_scale+fig_adj))

gs = fig.add_gridspec(nrow, ncol, hspace=0, wspace=0)
axes = gs.subplots(sharex=True, sharey=True)
if nrow == 1:
    axes = np.atleast_2d(axes)

for irow, row in enumerate(times):
    for icol, time in enumerate(row):

        data = np.loadtxt(f'{run}/{pfx}{time:0>4d}{sfx}').reshape((nx3, nx1))

        rho_int = RegularGridInterpolator((x3, x1), data, bounds_error=False, fill_value=0)

        data = rho_int(xig)
        del rho_int

        ax = axes[irow, icol]
        im = ax.imshow(np.log10(data*vnorm), aspect='equal', vmin=vmin, vmax=vmax, cmap=cmap, origin='lower')
        ax.tick_params(bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labeltop=False, labelleft=False, labelright=False)
        set_axis_spine_color(ax, 'black')
        pl.text(0.05, 0.95, f'{(time - time_j)*tnorm:2.1f} {time_units}', color='black', transform=ax.transAxes)


ax = axes[0, 0]
pl.sca(ax)
cbaxes = ax.inset_axes([0.97, 0.2, 0.03, 0.6], transform=ax.transAxes)
cb = pl.colorbar(im, ax=axes[0, 0], cax=cbaxes, ticks=range(cmin, cmax+1, cstp))
cbaxes.set_yticklabels([f'{i}' for i in range(cmin, cmax+1, cstp)])
cbaxes.yaxis.set_label_position('left')
cbaxes.yaxis.set_ticks_position('left')
pl.setp(pl.getp(cbaxes, 'yticklabels'), color='black')
cb.outline.set_edgecolor('black')
pl.text(0.45, 0.95, cb_label, color='black', transform=ax.transAxes)

pl.axhline(0.15*nx3, 0.5, 0.8, color='black')
pl.text(0.515, 0.03, f'0.3 {scale_units}', color='black', transform=ax.transAxes)

pl.savefig(f'{run}/{pfx}tiled.pdf', dpi=300, bbox_inches='tight')
# pl.savefig(f'{run}/{pfx}tiled.png', dpi=1000, bbox_inches='tight')
