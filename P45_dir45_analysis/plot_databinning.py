# This scripts plots the output to bov or vtk from VisIt of a DataBinning operation.
# In case of output to the VTK file, the VTK header bit must be removed from the file.

import numpy as np
import tools.physconst as pc
import tools.norm as norm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import tools.physconst as pc
from tools.vrenorm import renorm_vminmax
import re
import seaborn
import tools.mpl_aesth as mpla
# seaborn.set(font_scale=1.0)
mpla.adjust_rcParams(use_kpfonts=True)

# ______________________________________________________________________________________________________________________
# Set these

# Chose mode. Currently supported:'PV' or 'Nh'

# set type of file to plot 'vtk' or 'bov' ('bov' not implemented yet)
ftype = 'vtk'

# Set models
# models = ['pv-ap6hter-']
# models = ['pv-a6hte-']
models = ['pv-ap6hter-']
# models = ['pv-ap6hte-', 'pv-a6hte-']
# models = ['Nh-x-ap6hter-']
# models = ['Nh-z-ap6hter-']
# models = ['Nh-x-ap6hte-', 'Nh-x-a6hte-']
# models = ['Nh-z-ap6hte-', 'Nh-z-a6hte-']
snapshots = [1694,]
# snapshots = [400, 700, 1000]

if re.search('Nh-', models[0]):
    x1_min = -5.
    x1_max = 5.
    nx1 = 256
    x1_units = 'pc'
    x1_label = r'$y$'

    x2_min = -5
    x2_max = 5
    nx2 = 256
    x2_units = 'pc'
    x2_label = r'$z$' if re.search('-x-', models[0]) else r'$x$'

    v_log = True
    v_normalize = False
    v_minmax_frame = -1
    v_minmax_model = -1
    vmin = 23
    vmax = 24
    vnorm = 0.6034 * pc.pc
    v_units = r'cm$^{-2}$'
    v_label = r'$\log\,N$'
    cmap = cm.copper
    cmin = 23
    cmax = 24
    cstp = 1
    clabelpad = -12

    overshoot = 2

elif re.search('pv-', models[0]):
    x1_min = -5.
    x1_max = 5.
    nx1 = 256
    x1_units = 'pc'
    x1_label = r'$y$'

    x2_min = -150
    x2_max = 150
    nx2 = 200
    x2_units = r'km s$^{-1}$'
    x2_label = r'$v_x$'

    v_log = True
    v_normalize = True
    v_minmax_frame = -1
    v_minmax_model = -1
    vmin = 2.0
    vmax = 2.0
    vnorm = 1.
    v_units = r'arb. units'
    v_label = r'$\log\,$mass'
    cmap = cm.magma
    cmin = 0
    cmax = 2
    cstp = 2
    clabelpad = -7

    overshoot = 1

else:
    print('Unknown mode. Choose either "Nh" or "PV".')
    exit(1)


t_norm = 10./1000
t_units = 'kyr'

# How many data points missing at end of vtk files

# ______________________________________________________________________________________________________________________
# Main bit

if re.search('Nh-', models[0]):
    aspect = 'equal'
    figsize = (5, 3)
elif re.search('pv-', models[0]):
    aspect = 'auto'
    figsize = (5, 3)

# Normalize
vmin_old, vmax_old = vmin, vmax
if v_minmax_frame >= 0:
    sn = snapshots[v_minmax_frame]
    fprefix = models[v_minmax_model]
    data = np.loadtxt(f'{fprefix}{sn:0>4d}.{ftype}').flatten()[:-overshoot].reshape(nx2, nx1) * vnorm
    if v_log:
        data = np.log10(data * vnorm)
    else:
        data *= vnorm
    vmin, vmax, dummy = renorm_vminmax(vmin_old, vmax_old, data, v_normalize)


for fprefix in models:

    for sn in snapshots:

        data = np.loadtxt(f'{fprefix}{sn:0>4d}.{ftype}').flatten()[:-overshoot].reshape(nx2, nx1)

        if v_log:
            data = np.log10(data * vnorm)
        else:
            data *= vnorm

        if v_minmax_frame < 0:
            vmin, vmax, data = renorm_vminmax(vmin_old, vmax_old, data, v_normalize)
        else:
            if v_normalize:
                data = data - vmin_old


        fig = pl.figure(figsize=figsize)
        pl.imshow(data, vmin=vmin, vmax=vmax, cmap=cmap, extent=(x1_min, x1_max, x2_min, x2_max), origin='lower', aspect=aspect)
        pl.gca().set_facecolor('k')

        cb = pl.colorbar(pad=0.01, aspect=40, ticks=range(cmin, cmax+1, cstp))
        cb.set_label(f'{v_label} [ {v_units} ]', labelpad=clabelpad)

        pl.xlabel(rf'{x1_label} [ {x1_units} ]')
        pl.xlim(x1_min, x1_max)
        pl.ylabel(rf'{x2_label} [ {x2_units} ]')
        pl.ylim(x2_min, x2_max)

        pl.text(0.66, 1.02, rf'$t=$ {sn*t_norm:2.1f} {t_units}', transform=pl.gca().transAxes)

        pl.savefig(f'{fprefix}{sn:0>4d}.pdf', bbox_inches='tight')

        pl.close(pl.gcf())
