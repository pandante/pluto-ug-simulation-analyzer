import numpy as np
d = np.fromfile('data.full.0236.flt', dtype=np.float32)

nx3 = 2 * 120 + 544
nx2 = 672
nx1 = 672
nv = 7
d = d.reshape((nv, nx3, nx2, nx1))
variables = ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr1', 'tr2']

for ivar, var in enumerate(variables):
    d[ivar, :, :, :].tofile(f'{var}-full.0236.flt')
    d[ivar, 120:-120, :, :].tofile(f'{var}-disc.0236.flt')
