import numpy as np
import scipy.ndimage as ndi
import copy
from scipy.interpolate import RegularGridInterpolator
from scipy.spatial.transform import Rotation as R
import matplotlib.pyplot as pl
import matplotlib as mpl
import matplotlib.patches as patches
import tools.physconst as pc
import tools.grid_dot_out_reader as gdor
import tools.rotate as rotate
import tools.mpl_aesth as mpla
import multiprocessing as mp
from tools.expressions import need_vars

import sys
import os
import tools.norm as norm
import configparser
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager

mpla.adjust_rcParams(use_kpfonts=False, grid=True)


# ______________________________________________________________________________________________________________________
# Description

# This script creates a position-velocity plot and also projection plots for the data used
# in the analysis of B2 0258*35.
#
# TODO: Separate pv plot routine and projections
# TODO: include options for slits in the projection plotting

# To launch use something like:
#
# python pv_proj_b2.py -r runs.ini -R <run> -i pv_proj_b2.ini -s <setting> [data_dir] [out_dir]
#
# where   <run> is configuration section name in runs.ini
#         <setting> is configuration section name in data_binning.ini

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here at the moment

# ______________________________________________________________________________________________________________________
# Global settings

# Number of processors
nproc = 1

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Time
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        # Rotations
        self.rot1_i = confi.getfloat(setting, 'rot1_i')
        self.rot1_f = confi.getfloat(setting, 'rot1_f')
        self.rot1_delta = confi.getfloat(setting, 'rot1_delta')
        self.rot2_i = confi.getfloat(setting, 'rot2_i')
        self.rot2_f = confi.getfloat(setting, 'rot2_f')
        self.rot2_delta = confi.getfloat(setting, 'rot2_delta')
        self.rot3_i = confi.getfloat(setting, 'rot3_i')
        self.rot3_f = confi.getfloat(setting, 'rot3_f')
        self.rot3_delta = confi.getfloat(setting, 'rot3_delta')
        self.rot_data = confi.getboolean(setting, 'rot_data')
        self.rot_data_reduction = confi.getfloat(setting, 'rot_data_reduction')
        self.rot_limits = confi.get(setting, 'rot_limits')
        self.rot_zoom = confi.getfloat(setting, 'rot_zoom')

        # General settings
        self.vars = confi.get(setting, 'vars').split()
        self.rho_cut = confi.getfloat(setting, 'rho_cut')
        self.vnorm = confi.getfloat(setting, 'vnorm')
        self.ax = confi.getint(setting, 'ax')
        self.reverse = confi.getboolean(setting, 'reverse')
        self.use_kpfonts = confi.getboolean(setting, 'use_kpfonts')
        self.output = confi.get(setting, 'output').split()

        # Slit
        self.slit_orientation = confi.get(setting, 'slit_orientation')
        self.slit = confi.get(setting, 'slit')
        self.slit_height = confi.getfloat(setting, 'slit_height')
        self.slit_width = confi.getfloat(setting, 'slit_width')

        # Projections plotting settings
        self.radio_mode = confi.get(setting, 'radio_mode')
        self.sigma1_vmin = confi.getfloat(setting, 'sigma1_vmin')
        self.sigma1_vmax = confi.getfloat(setting, 'sigma1_vmax')
        self.rotv1_vmin = confi.getfloat(setting, 'rotv1_vmin')
        self.rotv1_vmax = confi.getfloat(setting, 'rotv1_vmax')
        self.pr_x1min = confi.getfloat(setting, 'pr_x1min')
        self.pr_x1max = confi.getfloat(setting, 'pr_x1max')
        self.pr_x2min = confi.getfloat(setting, 'pr_x2min')
        self.pr_x2max = confi.getfloat(setting, 'pr_x2max')
        self.smooth = confi.getboolean(setting, 'smooth')
        self.smooth_kw = confi.getfloat(setting, 'smooth_kw')

        # PV diagram settings
        self.pv_nbins1 = confi.getint(setting, 'pv_nbins1')
        self.pv_nbins2 = confi.getint(setting, 'pv_nbins2')
        self.pv_nbins = [self.pv_nbins1, self.pv_nbins2]
        self.pv_vmin = confi.getfloat(setting, 'pv_vmin')
        self.pv_vmax = confi.getfloat(setting, 'pv_vmax')
        self.pv_log_dyn_range = confi.getfloat(setting, 'pv_log_dyn_range')

        # General options for plotting/dumping
        self.do_plot = confi.getboolean(setting, 'do_plot')
        self.do_dump = confi.getboolean(setting, 'do_dump')
        self.read_from_dump = confi.getboolean(setting, 'read_from_dump')


def slit_bottom(x1, theta, slit_height):
    return x1 * np.tan(theta) - slit_height / (2. * np.cos(theta))

def slit_top(x1, theta, slit_height):
    return x1 * np.tan(theta) + slit_height / (2. * np.cos(theta))

def slit_left(x2, theta, slit_width):
    return -x2 * np.tan(theta) - slit_width / (2. * np.cos(theta))

def slit_right(x2, theta, slit_width):
    return -x2 * np.tan(theta) + slit_width / (2. * np.cos(theta))

def apply_slit_mask(data, x1, x2, theta, slit_width, slit_height, meshgrid=False):
    """
    Return masked array of input array with all values masked except for those in the slit. Works with 2D or 3D data.
    Slit is assumed to be centered at (x1, x2) = (0, 0).
    """

    if not meshgrid:
        x2g, x1g = np.meshgrid(x2, x1, indexing='ij')
    else:
        x2g, x1g = x2, x1

    data_slit = np.ma.masked_array(data)

    # Treat special angles first
    if np.abs(theta) % np.pi < 1.e-3:
        data_slit = np.ma.masked_where(np.less(x1g, -slit_width / 2.), data_slit)
        data_slit = np.ma.masked_where(np.greater(x1g, slit_width / 2.), data_slit)
        data_slit = np.ma.masked_where(np.less(x2g, -slit_height / 2.), data_slit)
        data_slit = np.ma.masked_where(np.greater(x2g, slit_height / 2.), data_slit)

    elif np.abs(theta + np.pi / 2.) % np.pi < 1.e-3:
        data_slit = np.ma.masked_where(np.less(x1g, -slit_height / 2.), data_slit)
        data_slit = np.ma.masked_where(np.greater(x1g, slit_height / 2.), data_slit)
        data_slit = np.ma.masked_where(np.less(x2g, -slit_width / 2.), data_slit)
        data_slit = np.ma.masked_where(np.greater(x2g, slit_width / 2.), data_slit)

    else:
        data_slit = np.ma.masked_where(np.less(x2g, slit_bottom(x1g, theta, slit_height)), data_slit)
        data_slit = np.ma.masked_where(np.greater(x2g, slit_top(x1g, theta, slit_height)), data_slit)
        data_slit = np.ma.masked_where(np.less(x1g, slit_left(x2g, theta, slit_width)), data_slit)
        data_slit = np.ma.masked_where(np.greater(x1g, slit_right(x2g, theta, slit_width)), data_slit)

    return data_slit


def add_slit_box(slit_width, slit_height, theta, reverse):
    rect = patches.Rectangle((-slit_width / 2., -slit_height / 2.), slit_width, slit_height, fc=(0, 0, 0, 0), ec='w',
                             lw=1., zorder=10)
    ax = pl.gca()
    t2 = mpl.transforms.Affine2D().rotate((np.pi - theta) if reverse else theta) + ax.transData
    rect.set_transform(t2)
    ax.add_patch(rect)


def main(argv):
    """
    Driver routine
    :param argv:  see set_params function
    """


    # Get runfile and runname, dat_dir, inifile, setting
    global iom
    iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

    # Read settings parameter
    global par
    par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

    # Normalizations
    global nm
    mu = 0.60364
    mmpp = mu * pc.amu
    kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
    nm = norm.PhysNorm(x=par.xdnorm, v=par.vdnorm, dens=mmpp, curr=1., temp=kelvin)

    # The processes will be in sync so that we always have
    # the same time, theta, and phi at any given time
    # Automatically gets inherited (copied) to sub-processes
    global time

    par_orig = copy.deepcopy(par)

    # Loop over all time steps
    for time in range(par.time_i, par.time_f + 1, par.time_delta):

        # TODO: if files are read in the loops over angle should come before the data reading,
        #       else they should come after data reading.

        if not par.rot_data:
            rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars), time)

        for rot1 in np.arange(par.rot1_i, par.rot1_f + 1., par.rot1_delta):
            for rot2 in np.arange(par.rot2_i, par.rot2_f + 1., par.rot2_delta):
                for rot3 in np.arange(par.rot3_i, par.rot3_f + 1., par.rot3_delta):

                    rotvec_1 = [np.deg2rad(rot1), 0, 0]
                    rotvec_2 = [0, np.deg2rad(rot2), 0]
                    rotvec_3 = [0, 0, np.deg2rad(rot3)]
                    rot_1 = R.from_rotvec(rotvec_1)
                    rot_2 = R.from_rotvec(rotvec_2)
                    rot_3 = R.from_rotvec(rotvec_3)

                    # TODO: really should be rotating the nodal coords.
                    x1, x2, x3 = rotate.rotated_axes(par.x1, par.x2, par.x3, [rot_1, rot_2, rot_3],
                                                     par.rot_data_reduction, par.rot_limits, par.rot_zoom)
                    par.update_grid_params(x1, x2, x3, centering='zonal')

                    if par.read_from_dump:
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'radio1-{par.slit_orientation}-{par.ax}', ext='prj')
                        radio1 = np.loadtxt(fname)
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'sigma1-{par.slit_orientation}-{par.ax}', ext='prj')
                        sigma1 = np.loadtxt(fname)
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'rotv1-{par.slit_orientation}-{par.ax}', ext='prj')
                        rotv1 = np.loadtxt(fname)

                    else:

                        # Read data file for this timestep
                        if par.rot_data:
                            rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars), time, [rot1, rot2, rot3])

                        elif np.abs(rot1) + np.abs(rot2) + np.abs(rot3) > 0.:
                            # TODO: Do rotation here - not programmed yet.

                            pass

                        if par.radio_mode == 'tr':
                            radio = np.heaviside(tr1 - 1.e-1, 1) * tr1
                            radio1 = np.average(radio, axis=par.ax)

                        elif par.radio_mode == 'pr':
                            radio = np.heaviside(tr1 - 1.e-3, 1) * (tr1 * prs) ** 1.8
                            radio1 = np.sum(radio, axis=par.ax)

                        else:
                            print(f'Unknown radio_mode "{par.radio_mode}". Has to be either "tr" or "pr".')
                            exit(1)

                        del radio

                        vx_pr = vx1 if par.ax == 2 else vx2
                        weights = np.ma.masked_less(rho, par.rho_cut)
                        rotv1 = np.average(vx_pr, axis=par.ax, weights=weights)
                        rotv1_e = rotv1[:, :, None] if par.ax == 2 else rotv1[:, None, :]
                        sigma1 = np.sqrt(np.average((vx_pr - rotv1_e) ** 2, axis=par.ax, weights=weights))
                        del rotv1_e

                        # Since we don't keep masks in dumps, we need to do this here already
                        rotv1[rotv1.mask] = 0
                        sigma1[sigma1.mask] = 0

                    if par.do_dump and not par.read_from_dump:
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'radio1-{par.slit_orientation}-{par.ax}', ext='prj')
                        np.savetxt(fname, radio1)
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'sigma1-{par.slit_orientation}-{par.ax}', ext='prj')
                        np.savetxt(fname, sigma1)
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'rotv1-{par.slit_orientation}-{par.ax}', ext='prj')
                        np.savetxt(fname, rotv1)

                    # The following is to be done in both the do_plot and do_dump cases, because
                    # we need theta to create the PV diagrams.

                    # del weights

                    # Normalize to km / s
                    sigma1 *= par.vdnorm / par.vnorm
                    rotv1 *= par.vdnorm / par.vnorm

                    if par.smooth:
                        sigma1 = ndi.gaussian_filter(sigma1, par.smooth_kw)
                        rotv1 = ndi.gaussian_filter(rotv1, par.smooth_kw)

                    xp = x2 if par.ax == 2 else x1

                    if par.slit_orientation == 'disc':
                        rv_pr = rotvec_2[1] if par.ax == 2 else rotvec_1[0]
                        theta = -rv_pr * np.sin(rotvec_3[2])

                    elif par.slit_orientation == 'rotation':
                        x3m, xpm = np.meshgrid(x3, xp, indexing='ij')
                        rpf = np.polyfit(xpm.ravel(), x3m.ravel(), deg=1, w=rotv1.ravel() ** 3)
                        theta = np.arctan(rpf[0])

                    elif par.slit_orientation == 'jet':
                        x3m, xpm = np.meshgrid(x3, xp, indexing='ij')
                        rpf = np.polyfit(xpm.ravel(), x3m.ravel(), deg=1, w=radio1.ravel() ** 3)
                        theta = np.arctan(rpf[0])

                    else:
                        print(f'Unknown slit_orientation {par.slit_orientation}.')
                        exit()

                    if par.do_plot:

                        if par.slit == 'color' or par.slit == 'color_inv':
                            sigma1_slit = apply_slit_mask(sigma1, xp, x3, theta, par.slit_width, par.slit_height)
                            rotv1_slit = apply_slit_mask(rotv1, xp, x3, theta, par.slit_width, par.slit_height)
                            if par.slit == 'color_inv':
                                sigma1_slit.mask = np.logical_not(sigma1_slit.mask)
                                rotv1_slit.mask = np.logical_not(rotv1_slit.mask)

                        figsize = (4, 3) if par.use_kpfonts else (5, 4)

                        if par.reverse:
                            sigma1 = sigma1[:, ::-1]
                            rotv1 = rotv1[:, ::-1]
                            radio1 = radio1[:, ::-1]
                            if par.slit == 'color' or par.slit == 'color_inv':
                                sigma1_slit = sigma1_slit[:, ::-1]
                                rotv1_slit = rotv1_slit[:, ::-1]

                        if par.radio_mode == 'tr':
                            radio1_mid = (np.min(radio1) + np.max(radio1)) / 2.
                            radio1_max = np.max(radio1)
                        elif par.radio_mode == 'pr':
                            radio1_mid = (np.min(np.ma.log10(radio1)) + np.max(np.ma.log10(radio1))) / 2.
                            radio1_max = np.max(np.ma.log10(radio1))

                        fig = pl.figure(figsize=figsize)
                        if not par.slit or par.slit == 'box':
                            pl.imshow(sigma1, cmap='magma', vmin=par.sigma1_vmin, vmax=par.sigma1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower')
                            if par.slit == 'box':
                                add_slit_box(par.slit_width, par.slit_height, theta, par.reverse)

                        elif par.slit == 'color' or 'color_inv':
                            pl.imshow(sigma1, cmap='gray', vmin=par.sigma1_vmin, vmax=par.sigma1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower')
                            pl.imshow(sigma1_slit, cmap='magma', vmin=par.sigma1_vmin, vmax=par.sigma1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower')

                        pl.gca().set_aspect('equal')
                        cb = pl.colorbar(pad=0.01, aspect=40)
                        # if par.radio_mode == 'tr':
                        #     pl.contour(xp, x3, radio1, colors='k', levels=4, linewidths=1.)
                        # elif par.radio_mode == 'pr':
                        #     pl.contour(xp, x3, np.log10(radio1), colors='k', levels=4, linewidths=1.)
                        pl.xlabel(r'$y$ [ kpc ]')
                        pl.ylabel(r'$z$ [ kpc ]')
                        cb.set_label(r'$\sigma_\mathrm{LOS}$ [ km s$^{-1}$ ]')
                        pl.xticks(np.arange(par.pr_x1min, par.pr_x1max * 1.01, 2))
                        pl.yticks(np.arange(par.pr_x2min, par.pr_x2max * 1.01, 2))
                        for ext in par.output:
                            fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'radio-sigma1-{par.slit_orientation}-{par.ax}', ext=ext)
                            pl.savefig(fname, bbox_inches='tight', dpi=1000)
                        pl.close(fig)


                        fig = pl.figure(figsize=figsize)
                        if not par.slit or par.slit == 'box':
                            pl.imshow(rotv1, cmap='twilight_r', vmin=par.rotv1_vmin, vmax=par.rotv1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower', zorder=0)
                            if par.slit == 'box':
                                add_slit_box(par.slit_width, par.slit_height, theta, par.reverse)
                        elif par.slit == 'color' or 'color_inv':
                            pl.imshow(np.abs(rotv1), cmap='gray', vmin=0, vmax=par.rotv1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower', zorder=0)
                            pl.imshow(rotv1_slit, cmap='twilight_r', vmin=par.rotv1_vmin, vmax=par.rotv1_vmax, extent=(xp[0], xp[-1], x3[0], x3[-1]), origin='lower', zorder=1)

                        pl.gca().set_aspect('equal')
                        cb = pl.colorbar(pad=0.01, aspect=40)
                        if par.radio_mode == 'tr':
                            pl.contourf(xp, x3, radio1, cmap='bone', levels=4, linewidths=1., zorder=2, alpha=0.75)
                        elif par.radio_mode == 'pr':
                            pl.contourf(xp, x3, np.log10(radio1), cmap='bone', levels=4, linewidths=1., zorder=2, alpha=0.75)
                        pl.xlabel(r'$y$ [ kpc ]')
                        pl.ylabel(r'$z$ [ kpc ]')
                        cb.set_label(r'$\bar{v}_\mathrm{LOS}$ [ km s$^{-1}$ ]')
                        pl.xticks(np.arange(par.pr_x1min, par.pr_x1max * 1.01, 2))
                        pl.yticks(np.arange(par.pr_x2min, par.pr_x2max * 1.01, 2))
                        for ext in par.output:
                            fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'radio-rotv1-{par.slit_orientation}-{par.ax}', ext=ext)
                            pl.savefig(fname, bbox_inches='tight', dpi=1000)
                        pl.close(fig)


                    # PV plot

                    if par.read_from_dump:
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'pvplot-{par.slit_orientation}-{par.ax}', ext='dbn')
                        h = np.array([np.loadtxt(fname),])

                    else:

                        vx_pr *= par.vdnorm / par.vnorm

                        x3g, x2g, x1g = np.meshgrid(x3, x2, x1, indexing='ij')

                        xpg = x2g if par.ax == 2 else x1g

                        off = np.sqrt(x3g * x3g + xpg * xpg) * np.cos(theta - np.arctan2(x3g, xpg))

                        weights = apply_slit_mask(weights, xpg, x3g, theta, par.slit_width, par.slit_height, meshgrid=True)

                        h = np.histogram2d(np.ravel(off[~weights.mask]), np.ravel(vx_pr[~weights.mask]), par.pv_nbins,
                                           range=[[-par.slit_width / 2., par.slit_width / 2.], [par.pv_vmin, par.pv_vmax]], weights=np.ravel(weights[~weights.mask]))

                    if par.do_dump and not par.read_from_dump:
                        fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'pvplot-{par.slit_orientation}-{par.ax}', ext='dbn')
                        np.savetxt(fname, h[0])


                    if par.do_plot:

                        fig = pl.figure(figsize=figsize)

                        log_h0min = np.log10(np.max(h[0])) - par.pv_log_dyn_range
                        loghist = np.log10(h[0]).T - log_h0min
                        if par.reverse:
                            loghist = loghist[:, ::-1]
                        pl.imshow(loghist, aspect='auto', origin='lower', cmap='pink',
                                  extent=[-par.slit_width / 2., par.slit_width / 2., par.pv_vmin, par.pv_vmax], vmin=0.0, vmax=par.pv_log_dyn_range)
                        pl.gca().set_facecolor('k')
                        cb = pl.colorbar(aspect=40, pad=0.01)

                        pl.xlabel(r'offset [ kpc ]')
                        pl.ylabel(r'$v_\mathrm{LOS}$ [ km s$^{-1}$ ]')

                        cb.set_label('log mass [ arbitrary units ]')
                        for ext in par.output:
                            fname = par.data_name(time, [rot1, rot2, rot3], dir=iom.out_dir, pfx=f'pvplot-{par.slit_orientation}-{par.ax}', ext=ext)
                            pl.savefig(fname, bbox_inches='tight', dpi=300)

                        pl.close(fig)

                    # Reset par to the original
                    par = copy.deepcopy(par_orig)


if __name__ == "__main__":
    main(sys.argv[1:])

