import numpy as np
import multiprocessing as mp
import ctypes
import sys
import os
import tools.norm as norm
import configparser
import tools.physconst as pc
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from mp_assistant import slices, tonumpyarray
from tools.expressions import need_vars, expression, normalization

# ______________________________________________________________________________________________________________________
# Description

# This script creates databins of a 3D cartesian PLUTO.
# It can, for example, be used to create simple position velocity diagrams.
# simulation, on a shared memory node, using the multiprocessing module.
# A (non-locking) shared array is used, although it could also have been
# achieved by just returning and adding the databins.

# To launch use something like:
#
# python data_binning.py -r runs.ini -R <run> -i data_binning.ini -s <setting> [data_dir] [out_dir]
#
# where   <run> is configuration section name in runs.ini
#         <setting> is configuration section name in data_binning.ini

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here at the moment

# ______________________________________________________________________________________________________________________
# Global settings

# Number of processors
nproc = 1

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Data_binning settings
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.vars = confi.get(setting, 'vars').split()
        self.nvars = len(self.vars)
        self.weight = confi.get(setting, 'weight')
        self.nbins1 = confi.getint(setting, 'nbins1')
        self.nbins2 = confi.getint(setting, 'nbins2')
        self.nbins = [self.nbins1, self.nbins2]
        self.vmin1 = confi.getint(setting, 'vmin1')
        self.vmax1 = confi.getint(setting, 'vmax1')
        self.vmin2 = confi.getint(setting, 'vmin2')
        self.vmax2 = confi.getint(setting, 'vmax2')
        self.log1 = confi.getboolean(setting, 'log1')
        self.log2 = confi.getboolean(setting, 'log2')
        self.vnorm = confi.getfloat(setting, 'vnorm')
        self.normed = confi.getboolean(setting, 'normed')

        # TODO: make sure all the parameters are read in. E.g. te_max is not read in yet.
        #       however, we need to think of a better way to apply these thresholds.
        self.tr1_min = confi.getfloat(setting, 'tr1_min')
        self.tr2_min = confi.getfloat(setting, 'tr2_min')
        self.te_min = confi.getfloat(setting, 'te_min')
        self.te_max = confi.getfloat(setting, 'te_max')
        self.rho_min = confi.getfloat(setting, 'rho_min')
        self.rho_crit = confi.getfloat(setting, 'rho_crit')
        self.radius_max = confi.getfloat(setting, 'radius_max')

        self.fbasenm = confi.get(setting, 'fbasenm')


def databin(x1, x2, x3, vx1, vx2, vx3, rho, prs, tr1, tr2, out):
    """
    Do data binning
    """

    # TODO: histogram normalizations

    weights = expression(par.weight, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, par.rho_min, te_min=par.te_min,
                         te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                         rho_crit=par.rho_crit)

    var1 = expression(par.vars[0], x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, par.rho_min, te_min=par.te_min,
                      te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                      rho_crit=par.rho_crit)
    var1 *= normalization(par.vars[0], nm, par.vnorm)
    if par.log1:
        var1 = np.log10(var1)

    if par.nvars > 1:
        var2 = expression(par.vars[1], x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, par.rho_min, te_min=par.te_min,
                          te_max=par.te_max, tr1_min=par.tr1_min, tr2_min=par.tr2_min, radius_max=par.radius_max,
                          rho_crit=par.rho_crit)
        var2 *= normalization(par.vars[1], nm, par.vnorm)
        if par.log2:
            var2 = np.log10(var2)

        lims = [[par.vmin1, par.vmax1], [par.vmin2, par.vmax2]]
        out[:], h1, h2 = np.histogram2d(var1.ravel(), var2.ravel(), par.nbins, lims,
                                        weights=weights.ravel(), normed=par.normed)

    else:
        lims = [par.vmin1, par.vmax1]
        out[:], h1 = np.histogram(var1.ravel(), par.nbins1, lims,
                                  weights=weights.ravel(), density=par.normed)

    return


def mp_databin_slice(ii):
    """
    Effectively currying in order to
    - preserve the signature of the xv_space_slice function,
    """

    i = ii[0]
    s = ii[1]
    isl = np.s_[i, :, :]
    if par.nvars > 1:
        osl = np.s_[s, :, :]
    else:
        osl = np.s_[s, :]

    return databin(gx1 if gx1 is not None else None,
                   gx2 if gx2 is not None else None,
                   gx3[isl] if gx3 is not None else None,
                   gv1[isl] if gv1 is not None else None,
                   gv2[isl] if gv2 is not None else None,
                   gv3[isl] if gv3 is not None else None,
                   grho[isl] if grho is not None else None,
                   gprs[isl] if gprs is not None else None,
                   gtr1[isl] if gtr1 is not None else None,
                   gtr2[isl] if gtr2 is not None else None,
                   gout[osl])


def mp_databin_init(x1, x2, x3, v1, v2, v3, rho, prs, tr1, tr2, out):
    """
    Called on each child process initialization.
    Create global variables for all variables databin() depends on
    """

    global gx1, gx2, gx3, gv1, gv2, gv3, grho, gprs, gtr1, gtr2, gout
    gx1, gx2, gx3, gv1, gv2, gv3 = x1, x2, x3, v1, v2, v3
    grho, gprs, gtr1, gtr2, gout = rho, prs, tr1, tr2, out


def databin_mp(x1, x2, x3, v1, v2, v3, rho, prs, tr1, tr2, nprocesses=nproc):
    """
    Perform databinning using multiple processes.
    """

    if par.nvars > 1:
        # Shared array where result will be stored. And make it a numpy array.
        out = mp.RawArray(ctypes.c_float, nprocesses * par.nbins1 * par.nbins2)
        out = tonumpyarray(out, [nprocesses, par.nbins1, par.nbins2], par.dfile_dtype)
    else:
        # Shared array where result will be stored. And make it a numpy array.
        out = mp.RawArray(ctypes.c_float, nprocesses * par.nbins1)
        out = tonumpyarray(out, [nprocesses, par.nbins1], par.dfile_dtype)

    # Start processes
    pool = mp.Pool(nprocesses,
                   initializer=mp_databin_init,
                   initargs=(x1, x2, x3, v1, v2, v3, rho, prs, tr1, tr2, out))

    # Slices and process numbers as arguments to worker function
    sli = zip(list(slices(par.nx3, nprocesses)), range(nprocesses))

    # Perform databinning (h1 and h2 are always the same)
    pool.map(mp_databin_slice, sli)

    pool.close()
    pool.join()

    return out


# Velocities are now never saved as four vectors
# def mp_four_to_three_vector_slice(i):
#     """
#     Effectively currying in order to
#     - preserve the signature of the v_space_slice function,
#     - requiring the pool.map function to only take one parameters
#     """
#     four_to_three_vector(gv1[i, :, :], gv2[i, :, :], gv3[i, :, :])
#

def mp_four_to_three_vector_init(v1, v2, v3):
    """
    Initialization for mp_xv_space_slice:
    Create global variables for all the variables the mp_v_space_slice function depends on.
    """

    global gv1, gv2, gv3
    gv1, gv2, gv3 = v1, v2, v3


def four_to_three_vector_mp(v1, v2, v3):
    """
    Create raw shared memory arrays, converted to numpy arrays.
    Create a pool and initialize and launch processes on allocated cpus.
    """

    # Processes
    pool = mp.Pool(nproc, initializer=mp_four_to_three_vector_init, initargs=(v1, v2, v3))
    pool.map(mp_four_to_three_vector_slice, slices(par.nx3, nproc))
    pool.close()
    pool.join()

    return v1, v2, v3


def create_fname_2d():

    # Add binning resolution
    fname = par.fbasenm + '-'.join(par.vars) + '-' + format(par.nbins1, '>03d') + 'x' + format(par.nbins2, '>03d')
    fname += '-' + format(time, '>04d')

    return fname


def create_fname_1d():

    # Add binning resolution
    fname = par.fbasenm + '-'.join(par.vars) + '-' + format(par.nbins1, '>03d')

    return fname


def main(argv):
    """
    Driver routine
    :param argv:  see set_params function
    """

    # Set parameters for the binning
    # Automatically gets inherited (copied) to sub-processes

    # Get runfile and runname, dat_dir, inifile, setting
    global iom
    iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

    # Read settings parameter
    global par
    par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

    # Normalizations
    global kelvin, mu, nm
    mu = 0.60364
    kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
    nm = norm.PhysNorm(x=par.xdnorm, v=par.vdnorm, dens=mu*pc.amu, temp=kelvin, curr=1.)

    # The processes will be in sync so that we always have
    # the same time, theta, and phi at any given time
    # Automatically gets inherited (copied) to sub-processes
    global time

    times = range(par.time_i, par.time_f + 1, par.time_delta)

    # Create position space (the same for a given simulation)
    x1, x2, x3 = par.x_space()

    # Do main databins. Loop over all timesteps
    for time in times:

        # Read data file for this timestep
        rho, vx1, vx2, vx3, prs, tr1, tr2 = par.read_data_mp(need_vars(par.vars + [par.weight]), time, [0, 0, 0])

        # Create velocity space
        # if par.four_vector and {'vx1', 'vx2', 'vx3'} & set(need_vars(par.vars+[par.weight])):
        #     if nproc > 1:
        #         four_to_three_vector_mp(vx1, vx2, vx3)
        #     else:
        #         vx1, vx2, vx3 = four_to_three_vector(vx1, vx2, vx3)

        # Create databin
        h = databin_mp(x1, x2, x3, vx1, vx2, vx3, rho, prs, tr1, tr2)
        h = h.sum(0)

        # Dump databin file
        # TODO: make extension modifiable as input parameter (see project.py)
        if par.nvars > 1:
            hfname = iom.out_dir + '/' + create_fname_2d() + '.dbn'
            np.savetxt(hfname, h)

        else:
            hfname = iom.out_dir + '/' + create_fname_1d() + '.dbn'
            with open(hfname, 'ab') as fh:
                np.savetxt(fh, h[None, :])

if __name__ == "__main__":
    main(sys.argv[1:])

