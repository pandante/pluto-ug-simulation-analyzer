import numpy as np
import tools.physconst as pc
import tools.expressions as expr

# ______________________________________________________________________________________________________________________
# Set these

# Either fill in i_* or snapshots. If snapshots is not empty the list of numbers in there will be used.
i_begin = 64
i_end = 64
i_step = 1
snapshots = [0236]

# Do data too (or just header)?
do_data = True

# In physical units, to convert i into time
time0 = 0
delta_time = 1

fprefix = 'data.full.'
fsuffix = '.flt'

nx1 = 256
nx2 = 256
nx3 = 256

ncpu = 8

vars = ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr1', 'tr2']

brick_origin = [-5.0, -5.0, -5.0]
# brick_origin = [-0.5, -0.5, -0.5]
brick_size = [10., 10., 10.]
# brick_size = [1.0, 1.0, 1.0]


# ______________________________________________________________________________________________________________________
# Helper methods

def closest_divisors(n):
    a = round(np.sqrt(n))
    while n % a > 0:
        a -= 1
    return a, n//a


# ______________________________________________________________________________________________________________________
# Main bit

if not snapshots:
    data_files = range(i_begin, i_end + 1, i_step)

nvar = len(vars)

nb2, nb1 = closest_divisors(ncpu)
nb3 = 1
if nx1 % nb1 != 0 or nx2 % nb2 != 0:
    print('Choose different processor number')
    exit()
else:
    bricklet_size = [nx1 / nb1, nx2 / nb2, nx3 / nb3]

for i in snapshots:

    # Filenames
    fname_flt = f'{fprefix}{i:0>4d}{fsuffix}'
    fname_bvh = f'{fprefix}{i:0>4d}.bov'
    fname_bov = f'{fprefix}{i:0>4d}.bov{fsuffix}'

    # Write header
    fh = open(fname_bvh, 'w')
    fh.write(f'TIME: {time0 + i * delta_time}\n')
    fh.write(f'DATA_FILE: {fname_bov}\n')
    fh.write(f'DATA_SIZE: {nx1} {nx2} {nx3}\n')
    fh.write(f'DATA_FORMAT: FLOAT\n')
    fh.write(f'VARIABLE: hydro\n')
    fh.write(f'DATA_ENDIAN: LITTLE\n')
    fh.write(f'CENTERING: zonal\n')
    fh.write(f'BRICK_ORIGIN: {brick_origin[0]} {brick_origin[1]} {brick_origin[2]}\n')
    fh.write(f'BRICK_SIZE: {brick_size[0]} {brick_size[1]} {brick_size[2]}\n')
    fh.write(f'DIVIDE_BRICK: {"true" if ncpu > 1 else "false"}\n')
    if ncpu > 1:
        fh.write(f'DATA_BRICKLETS: {bricklet_size[0]} {bricklet_size[1]} {bricklet_size[2]}\n')
    fh.write(f'DATA_COMPONENTS: {nvar}\n')
    fh.close()
    print(f'Success writing to {fname_bvh}')

    # Reconstruct data file
    if do_data:

        # Move variables axis to the fastest moving one
        d = np.moveaxis(np.fromfile(fname_flt, dtype=np.float32).reshape((nvar, nx3, nx2, nx1)), 0, 3)
        fh = open(fname_bov, "w")
        d.astype(np.float32).tofile(fh)
        fh.close()
        print(f'Success writing to {fname_bov}')

