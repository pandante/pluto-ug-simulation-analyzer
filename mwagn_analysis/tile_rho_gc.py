import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import matplotlib.patches as patches
import re
import tools.physconst as pc
from tools.vrenorm import renorm_vminmax
import tools.mpl_aesth as mpla
mpla.adjust_rcParams(use_kpfonts=True, grid=False)

# run = 'ash'
# run = 'a6hte'
# run = 'ap6hte'
run = 'relax'
pfx = 'rho-ax2-0128-'
# times = [[24, 120, 360, 651]]
# times = [[133, 430, 660, 1273]]
# times = [[300, 500, 900, 1204]]
times = [[1308, 1600, 1694]]
sfx = '.sli'
nx1 = nx2 = 256
scale_bar_label = '0.3 kpc'
vmin, vmax = -4, 3
vnorm = 1.
v_log = True
v_normalize = False
v_minmax_frame = 1
cmin, cmax, cstp = -4, 3, 1
cmap = cm.Spectral_r
cb_label = r'$\log{} n$ [ cm$^{-3}$ ]'
im_origin = 'upper'
cb_label_w_adj = 0.0
cb_label_h_adj = 0.0
tnorm = 10./1000
time_units = 'kyr'
time_bb = False
cb_label_bb = False
scale_label_bb = False
bg_color = 0.
edge_color = 'k'
font_color = 'k'


def set_axis_spine_color(ax, color):
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)


if font_color == 'w':
    bb_color = 'k'
elif font_color == 'k':
    bb_color = 'w'

# Normalize
vmin_old, vmax_old = vmin, vmax
if v_minmax_frame >= 0:
    norm_time = np.ravel(times)[v_minmax_frame]
    data = np.loadtxt(f'{run}/{pfx}{norm_time:0>4d}{sfx}')
    if v_log:
        data = np.log10(data * vnorm)
    else:
        data *= vnorm
    vmin_old = vmin
    vmin, vmax, dummy = renorm_vminmax(vmin_old, vmax_old, data, v_normalize)

nrow = len(times)
ncol = len(times[0])
fig_scale = 3.0
fig_adj = 0.02
fig = pl.figure(figsize=(ncol*fig_scale, nrow*fig_scale+fig_adj))

gs = fig.add_gridspec(nrow, ncol, hspace=0, wspace=0)
axes = gs.subplots(sharex=True, sharey=True)
if nrow == 1:
    axes = np.atleast_2d(axes)

for irow, row in enumerate(times):
    for icol, time in enumerate(row):

        data = np.loadtxt(f'{run}/{pfx}{time:0>4d}{sfx}').reshape((nx2, nx1))
        if v_log:
            data = np.log10(data * vnorm)
        else:
            data *= vnorm

        if v_minmax_frame < 0:
            vmin, vmax, data = renorm_vminmax(vmin_old, vmax_old, data, v_normalize)
        else:
            if v_normalize:
                data = data - vmin_old

        ax = axes[irow, icol]
        im = ax.imshow(data, aspect='equal', origin=im_origin, vmin=vmin, vmax=vmax, cmap=cmap)
        if bg_color:
            ax.set_facecolor(cmap(bg_color))
        ax.tick_params(bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labeltop=False, labelleft=False, labelright=False)
        set_axis_spine_color(ax, edge_color)
        text = pl.text(0.05, 0.9, f'{time*tnorm:2.1f} {time_units}', color=font_color, transform=ax.transAxes,
                       bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if time_bb else None))


if cb_label_bb:
    rect = patches.FancyBboxPatch((0.87, 0.15), 0.08, 0.7, lw=0, boxstyle='round, pad=0, rounding_size=0.02', fc=bb_color, alpha=0.3, transform=ax.transAxes)
    ax.add_patch(rect)

cbaxes = ax.inset_axes([0.97, 0.2, 0.03, 0.6], transform=ax.transAxes)
cb = pl.colorbar(im, ax=axes[0, 0], cax=cbaxes, ticks=range(cmin, cmax+1, cstp))
cbaxes.set_yticklabels([f'{i}' for i in range(cmin, cmax+1, cstp)])
cbaxes.yaxis.set_label_position('left')
cbaxes.yaxis.set_ticks_position('left')
pl.setp(pl.getp(cbaxes, 'yticklabels'), color=font_color)
cb.outline.set_edgecolor(edge_color)
pl.text(0.6+cb_label_w_adj, 0.9+cb_label_h_adj, cb_label, color=font_color, ma='right', transform=ax.transAxes,
        bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if cb_label_bb else None))

axhline_pos = 0.9 * nx2 if im_origin == 'upper' else 0.1 * nx2
pl.axhline(axhline_pos, 0.5, 0.8, color=font_color)
pl.text(0.535, 0.03, f'{scale_bar_label}', color=font_color, transform=ax.transAxes,
        bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if scale_label_bb else None))

pl.savefig(f'{run}/{pfx}tiled.pdf', dpi=300, bbox_inches='tight')