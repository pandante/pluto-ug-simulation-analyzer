import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import tools.mpl_aesth as mpla
import tools.physconst as pc
mpla.adjust_rcParams(use_kpfonts=True, grid=False)

# run = 'cp6'
run = 'cp6hf'
pfx = 'rho-ax2-0128-'
# times = [[0, 51, 101, 201], [300, 429, 700, 981]]
times = [[0, 31, 150, 794]]
sfx = '.sli'
nx1 = nx2 = 256
scale_bar_label = '0.3 kpc'
vmin, vmax = -5, 3
vnorm = 1.
cmin, cmax, cstp = -5, 3, 2
cmap = cm.Spectral_r
cb_label = r'$\log{} n$ [ cm$^{-3}$ ]'
tnorm = 10./1000
time_units = 'Myr'
scale_units = 'kpc'

def set_axis_spine_color(ax, color):
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)


nrow = len(times)
ncol = len(times[0])
fig_scale = 3.0
fig_adj = 0.02

fig = pl.figure(figsize=(ncol*fig_scale, nrow*fig_scale+fig_adj))

gs = fig.add_gridspec(nrow, ncol, hspace=0, wspace=0)
axes = gs.subplots(sharex=True, sharey=True)
if nrow == 1:
    axes = np.atleast_2d(axes)

for irow, row in enumerate(times):
    for icol, time in enumerate(row):

        data = np.loadtxt(f'{run}/{pfx}{time:0>4d}{sfx}').reshape((nx2, nx1))
        ax = axes[irow, icol]
        im = ax.imshow(np.log10(data*vnorm), aspect='equal', vmin=vmin, vmax=vmax, cmap=cmap)
        ax.tick_params(bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labeltop=False, labelleft=False, labelright=False)
        set_axis_spine_color(ax, 'white')
        pl.text(0.05, 0.9, f'{time*tnorm:2.1f} {time_units}', color='white', transform=ax.transAxes,
                bbox={'facecolor':'k', 'alpha':0.3, 'boxstyle':'round', 'edgecolor':'none'})


cbaxes = ax.inset_axes([0.97, 0.2, 0.03, 0.6], transform=ax.transAxes)
cb = pl.colorbar(im, ax=axes[0, 0], cax=cbaxes, ticks=range(cmin, cmax+1, cstp))
cbaxes.set_yticklabels([f'{i}' for i in range(cmin, cmax+1, cstp)])
cbaxes.yaxis.set_label_position('left')
cbaxes.yaxis.set_ticks_position('left')
pl.setp(pl.getp(cbaxes, 'yticklabels'), color='white')
cb.outline.set_edgecolor('white')
pl.text(0.6, 0.9, cb_label, color='white', transform=ax.transAxes)

pl.axhline(0.9*nx2, 0.5, 0.8, color='white')
pl.text(0.535, 0.03, f'0.3 {scale_units}', color='white', transform=ax.transAxes)

pl.savefig(f'{run}/{pfx}tiled.pdf', dpi=300, bbox_inches='tight')