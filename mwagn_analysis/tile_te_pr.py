import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import matplotlib.patches as patches
import re
import tools.physconst as pc
import tools.mpl_aesth as mpla
mpla.adjust_rcParams(use_kpfonts=True, grid=False)

run = 'cp6'
pfx = 'pr-te_pr-ax1-'
# times = [[0, 101, 300, 700]]
times = [[51, 201, 429, 981]]
# times = [[0, 51, 101, 201], [300, 429, 700, 981]]
sfx = '.prj'
nx1 = nx2 = 256
scale_bar_label = '0.3 kpc'
vmin, vmax = 2, 9
vnorm = pc.c * pc.c * pc.amu / pc.kboltz
v_log = True
v_normalize = False
v_minmax_frame = 1
cmin, cmax, cstp = 2, 9, 1
cmap = cm.RdYlBu
cb_label = r'$\log{} <T>$ [ K ]'
im_origin = 'lower'
cb_label_w_adj = 0.0
cb_label_h_adj = 0.0
tnorm = 10./1000
time_units = 'Myr'
scale_units = 'kpc'
time_bb = True
cb_label_bb = True
scale_label_bb = True
bg_color = None
edge_color = 'k'
font_color = 'k'


def renorm_vminmax(vmin, vmax, d, v_normalize):
    if not vmin:
        vmin = np.min(d)
    if not vmax:
        vmax = np.max(d)

    if vmin == vmax:
        if vmin > 0:
            vmax = np.max(d)
            vmin = vmax - vmin
        elif vmin < 0:
            vmin = np.min(d)
            vmax = vmin + vmax
        else:
            vmin = np.min(d)
            vmax = np.max(d)

        if v_normalize:
            d = d - vmin
            vmax = vmax - vmin
            vmin = 0

    return vmin, vmax, d


def set_axis_spine_color(ax, color):
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)


if font_color == 'w':
    bb_color = 'k'
elif font_color == 'k':
    bb_color = 'w'

# Normalize
if v_minmax_frame >= 0:
    norm_time = np.ravel(times)[v_minmax_frame]
    data = np.loadtxt(f'{run}/{pfx}{norm_time:0>4d}{sfx}')
    if v_log:
        data = np.log10(data * vnorm)
    else:
        data *= vnorm
    vmin_old = vmin
    vmin, vmax, dummy = renorm_vminmax(vmin, vmax, data, v_normalize)

nrow = len(times)
ncol = len(times[0])
fig_scale = 3.0
fig_adj = 0.02
fig = pl.figure(figsize=(ncol*fig_scale, nrow*fig_scale+fig_adj))

gs = fig.add_gridspec(nrow, ncol, hspace=0, wspace=0)
axes = gs.subplots(sharex=True, sharey=True)
if nrow == 1:
    axes = np.atleast_2d(axes)

for irow, row in enumerate(times):
    for icol, time in enumerate(row):

        data = np.loadtxt(f'{run}/{pfx}{time:0>4d}{sfx}').reshape((nx2, nx1))
        if v_log:
            data = np.log10(data * vnorm)
        else:
            data *= vnorm
        if v_minmax_frame < 0:
            vmin, vmax, data = renorm_vminmax(vmin, vmax, data, v_normalize)
        else:
            if v_normalize:
                data = data - vmin_old
        ax = axes[irow, icol]
        im = ax.imshow(data, aspect='equal', origin=im_origin, vmin=vmin, vmax=vmax, cmap=cmap)
        if bg_color:
            ax.set_facecolor(cmap(bg_color))
        ax.tick_params(bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labeltop=False, labelleft=False, labelright=False)
        set_axis_spine_color(ax, edge_color)
        text = pl.text(0.05, 0.9, f'{time*tnorm:2.1f} {time_units}', color=font_color, transform=ax.transAxes,
                       bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if time_bb else None))


if cb_label_bb:
    rect = patches.FancyBboxPatch((0.87, 0.15), 0.08, 0.7, lw=0, boxstyle='round, pad=0, rounding_size=0.02', fc=bb_color, alpha=0.3, transform=ax.transAxes)
    ax.add_patch(rect)

cbaxes = ax.inset_axes([0.97, 0.2, 0.03, 0.6], transform=ax.transAxes)
cb = pl.colorbar(im, ax=axes[0, 0], cax=cbaxes, ticks=range(cmin, cmax+1, cstp))
cbaxes.set_yticklabels([f'{i}' for i in range(cmin, cmax+1, cstp)])
cbaxes.yaxis.set_label_position('left')
cbaxes.yaxis.set_ticks_position('left')
pl.setp(pl.getp(cbaxes, 'yticklabels'), color=font_color)
cb.outline.set_edgecolor(edge_color)
pl.text(0.6+cb_label_w_adj, 0.9+cb_label_h_adj, cb_label, color=font_color, ma='right', transform=ax.transAxes,
        bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if cb_label_bb else None))

axhline_pos = 0.9 * nx2 if im_origin == 'upper' else 0.1 * nx2
pl.axhline(axhline_pos, 0.5, 0.8, color=font_color)
pl.text(0.535, 0.03, f'0.3 {scale_units}', color=font_color, transform=ax.transAxes,
        bbox=({'fc': bb_color, 'alpha': 0.3, 'boxstyle': 'round', 'lw': 0} if scale_label_bb else None))

pl.savefig(f'{run}/{pfx}tiled.pdf', dpi=300, bbox_inches='tight')