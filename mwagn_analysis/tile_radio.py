import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import re
import tools.physconst as pc
import tools.mpl_aesth as mpla
mpla.adjust_rcParams(use_kpfonts=True, grid=False)

# run = 'cp6'
run = 'cp6hf'
pfx = 'pr-radio-ax1-'
# times = [[64, 101, 302, 868]]
times = [[31, 101, 300, 794]]
sfx = '.prj'
nx1 = nx2 = 256
scale_bar_label = '0.3 kpc'
vmin, vmax = 4, 4
vnorm = pc.kpc
v_log = True
v_normalize = True
v_minmax_frame = 1
cmin, cmax, cstp = 0, 4, 1
cmap = cm.cividis
cb_label = r'$\log{}$' + 'SB [ arb. units ]'
im_origin = 'lower'
cb_label_w_adj = -0.14
cb_label_h_adj = 0.0
tnorm = 10./1000
time_units = 'Myr'
scale_units = 'kpc'
timebb = False


def renorm_vminmax(vmin, vmax, d, v_normalize):
    if not vmin:
        vmin = np.min(d)
    if not vmax:
        vmax = np.max(d)

    if vmin == vmax:
        if vmin > 0:
            vmax = np.max(d)
            vmin = vmax - vmin
        elif vmin < 0:
            vmin = np.min(d)
            vmax = vmin + vmax
        else:
            vmin = np.min(d)
            vmax = np.max(d)

        if v_normalize:
            d = d - vmin
            vmax = vmax - vmin
            vmin = 0

    return vmin, vmax, d


def set_axis_spine_color(ax, color):
    ax.spines['bottom'].set_color(color)
    ax.spines['top'].set_color(color)
    ax.spines['right'].set_color(color)
    ax.spines['left'].set_color(color)


# Normalize
if v_minmax_frame >= 0:
    norm_time = np.ravel(times)[v_minmax_frame]
    data = np.loadtxt(f'{run}/{pfx}{norm_time:0>4d}{sfx}')
    if v_log:
        data = np.log10(data * vnorm)
    else:
        data *= vnorm
    vmin_old = vmin
    vmin, vmax, dummy = renorm_vminmax(vmin, vmax, data, v_normalize)

nrow = len(times)
ncol = len(times[0])
fig_scale = 3.0
fig_adj = 0.02
fig = pl.figure(figsize=(ncol*fig_scale, nrow*fig_scale+fig_adj))

gs = fig.add_gridspec(nrow, ncol, hspace=0, wspace=0)
axes = gs.subplots(sharex=True, sharey=True)
if nrow == 1:
    axes = np.atleast_2d(axes)

for irow, row in enumerate(times):
    for icol, time in enumerate(row):

        data = np.loadtxt(f'{run}/{pfx}{time:0>4d}{sfx}').reshape((nx2, nx1))
        if v_log:
            data = np.log10(data * vnorm)
        else:
            data *= vnorm
        if v_minmax_frame < 0:
            vmin, vmax, data = renorm_vminmax(vmin, vmax, data, v_normalize)
        else:
            data = data - vmin_old
        ax = axes[irow, icol]
        im = ax.imshow(data, aspect='equal', origin=im_origin, vmin=vmin, vmax=vmax, cmap=cmap)
        ax.set_facecolor(cmap(0.))
        ax.tick_params(bottom=False, top=False, left=False, right=False,
                       labelbottom=False, labeltop=False, labelleft=False, labelright=False)
        set_axis_spine_color(ax, 'white')
        text = pl.text(0.05, 0.9, f'{time*tnorm:2.1f} {time_units}', color='white', transform=ax.transAxes,
                bbox=({'facecolor':'k', 'alpha':0.3, 'boxstyle':'round', 'edgecolor':'none'} if timebb else None))


cbaxes = ax.inset_axes([0.97, 0.2, 0.03, 0.6], transform=ax.transAxes)
cb = pl.colorbar(im, ax=axes[0, 0], cax=cbaxes, ticks=range(cmin, cmax+1, cstp))
cbaxes.set_yticklabels([f'{i}' for i in range(cmin, cmax+1, cstp)])
cbaxes.yaxis.set_label_position('left')
cbaxes.yaxis.set_ticks_position('left')
pl.setp(pl.getp(cbaxes, 'yticklabels'), color='white')
cb.outline.set_edgecolor('white')
pl.text(0.6+cb_label_w_adj, 0.9+cb_label_h_adj, cb_label, color='white', ma='right', transform=ax.transAxes)

axhline_pos = 0.9 * nx2 if im_origin == 'upper' else 0.1 * nx2
pl.axhline(axhline_pos, 0.5, 0.8, color='white')
pl.text(0.535, 0.03, f'0.3 {scale_units}', color='white', transform=ax.transAxes)

pl.savefig(f'{run}/{pfx}tiled.pdf', dpi=300, bbox_inches='tight')