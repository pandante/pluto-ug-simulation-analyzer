import numpy as np
import multiprocessing as mp
import sys
import os
import subprocess
import configparser
from tools.expressions import need_vars, expression, get_var_label, get_var_type, normalization
from pluto_ug_sim import PlutoUGSim
from tools.io_manager import IOManager
from mp_assistant import slices
import tools.physconst as pc
import tools.norm as norm
import tools.cmtools as cmt
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
import matplotlib.cm as cm
import seaborn
import tools.mpl_aesth as mpla
# seaborn.set(font_scale=1.0)
mpla.adjust_rcParams(use_kpfonts=True)

# TODO: add option for black background (or any color), as done in pvplot with an additional image layer and zorder.

# TODO: Turn plot into a paint routine to be able to create multi-panel output, with time_i, time_f, and time_delta

# ______________________________________________________________________________________________________________________
# Description

# This script plots slices of any variable available in expressions.py using the outputs of slicer.py.
# To launch with test example use parameters: ...

nproc = 1

# ______________________________________________________________________________________________________________________
# Just for debug

# Nothing here for the moment....

# ______________________________________________________________________________________________________________________
# Functions


class Parameters(PlutoUGSim):

    def __init__(self, runfile, runname, data_dir, inifile, setting):

        # The parameters for the run
        super().__init__(runfile, runname, data_dir)

        # The parameters for this script
        self._set_params(inifile, setting)

    def _set_params(self, inifile, setting):
        """
        :param argv:  should contain ini file name
        :return: Nothing

        Set global parameters for this script
        """

        confi = configparser.ConfigParser()
        confi.read(inifile)

        # Slice settings
        self.axis1 = confi.getboolean(setting, 'axis1')
        self.axis2 = confi.getboolean(setting, 'axis2')
        self.axis3 = confi.getboolean(setting, 'axis3')
        self.axes = [i + 1 for i, a in enumerate([self.axis1, self.axis2, self.axis3]) if a]
        self.ax1_intcpt = confi.getint(setting, 'ax1_intcpt')
        self.ax2_intcpt = confi.getint(setting, 'ax2_intcpt')
        self.ax3_intcpt = confi.getint(setting, 'ax3_intcpt')
        self.intercepts = [self.ax1_intcpt, self.ax2_intcpt, self.ax3_intcpt]

        self.vars = confi.get(setting, 'vars').split()
        self.npvars = len(self.vars)
        self.hide = self.multi_getboolean(confi, setting, 'hide', self.npvars)
        self.vnorm = confi.getfloat(setting, 'vnorm')
        self.add_time = self.multi_getboolean(confi, setting, 'add_time', self.npvars)
        self.superimpose = confi.getboolean(setting, 'superimpose')

        # TODO: If times are negative, use time from ic5063.ini (for all plots)
        self.time_i = confi.getint(setting, 'time_i')
        self.time_f = confi.getint(setting, 'time_f')
        self.time_delta = confi.getint(setting, 'time_delta')

        self.vmin = self.multi_getfloat(confi, setting, 'vmin', self.npvars)
        self.vmax = self.multi_getfloat(confi, setting, 'vmax', self.npvars)
        self.tr1_min = self.multi_getfloat(confi, setting, 'tr1_min', self.npvars)
        self.tr2_min = self.multi_getfloat(confi, setting, 'tr2_min', self.npvars)
        self.rho_min = self.multi_getfloat(confi, setting, 'rho_min', self.npvars)
        self.rho_max = self.multi_getfloat(confi, setting, 'rho_max', self.npvars)
        self.te_min = self.multi_getfloat(confi, setting, 'te_min', self.npvars)
        self.te_max = self.multi_getfloat(confi, setting, 'te_max', self.npvars)
        self.ncont = confi.getint(setting, 'ncont')
        self.cont_lw = confi.getfloat(setting, 'cont_lw')

        self.log = self.multi_getboolean(confi, setting, 'log', self.npvars)
        self.plottype = self.multi_get(confi, setting, 'plottype', self.npvars)
        self.colormap = self.multi_get(confi, setting, 'colormap', self.npvars)
        self.colorbar = self.multi_getboolean(confi, setting, 'colorbar', self.npvars)
        self.cb_delta_ticks = self.multi_getfloat(confi, setting, 'cb_delta_ticks', self.npvars)
        self.xlabel = self.multi_getboolean(confi, setting, 'xlabel', self.npvars)
        self.ylabel = self.multi_getboolean(confi, setting, 'ylabel', self.npvars)
        self.model_label = self.multi_getboolean(confi, setting, 'model_label', self.npvars)
        self.insert_wbg = self.multi_getboolean(confi, setting, 'insert_wbg', self.npvars)
        self.wbg_point = self.multi_getfloat(confi, setting, 'wbg_point', self.npvars)
        self.threshold = self.multi_getboolean(confi, setting, 'threshold', self.npvars)
        self.floor = self.multi_getboolean(confi, setting, 'floor', self.npvars)
        self.rescale = self.multi_getboolean(confi, setting, 'rescale', self.npvars)

        # TODO: do sep_in_var_dir
        self.sep_out_var_dir = confi.getboolean(setting, 'sep_out_var_dir')
        self.output = confi.get(setting, 'output').split()
        self.make_movie = confi.getboolean(setting, 'make_movie')

        self.slice_fbasenm = confi.get(setting, 'slice_fbasenm')
        self.slice_ext = confi.get(setting, 'slice_ext')
        self.plot_fbasenm = confi.get(setting, 'plot_fbasenm')


def create_fname(basenm, var, axis, intcpt, time=None):

    var = str.replace(var, '/', '_by_')
    fname = basenm + var + '-ax' + format(axis, '>01d')
    fname += '-' + format(intcpt, '>04d') if intcpt is not None else ''

    if time is None:
        pass

    elif time == 'movie':
        fname += '-%4d'

    else:
        fname += '-' + format(time, '>04d')

    return fname


def paint_slice(var, ivar, field, pl_axis, time):

    # Shortcut variables for things that depend on ivar
    # TODO: in principle, this function should not depend on ivar or lists, but its messy implementing it otherwise.
    vmin = None if par.vmin[ivar] == par.vmax[ivar] else par.vmin[ivar]
    vmax = None if par.vmin[ivar] == par.vmax[ivar] else par.vmax[ivar]
    vrange = np.abs(par.vmin[ivar]) if vmin is None else vmax - vmin
    plottype = par.plottype[ivar]
    colormap = par.colormap[ivar]
    colorbar = par.colorbar[ivar]
    cbdt = par.cb_delta_ticks[ivar]
    xlabel = par.xlabel[ivar]
    ylabel = par.ylabel[ivar]
    model_label = par.model_label[ivar]
    add_time = par.add_time[ivar]
    ins_wbg = par.insert_wbg[ivar]
    pnt_wbg = par.wbg_point[ivar]
    log = par.log[ivar]
    threshold = par.threshold[ivar]
    floor = par.floor[ivar]
    rescale = par.rescale[ivar]

    # Create plot
    # TODO: Make spatial units settable
    if pl_axis == 3:
        extent = (par.x1min, par.x1max, par.x2min, par.x2max)
        domain = (x1, x2)
        xlabeltext = r'$x$ [ pc ]'
        ylabeltext = r'$y$ [ pc ]'
        xmin, xmax = par.x1min, par.x1max
        ymin, ymax = par.x2min, par.x2max
    if pl_axis == 2:
        extent = (par.x1min, par.x1max, par.x3min, par.x3max)
        domain = (x1, x3)
        xlabeltext = r'$x$ [ pc ]'
        ylabeltext = r'$z$ [ pc ]'
        xmin, xmax = par.x1min, par.x1max
        ymin, ymax = par.x3min, par.x3max
    if pl_axis == 1:
        extent = (par.x2min, par.x2max, par.x3min, par.x3max)
        domain = (x2, x3)
        xlabeltext = r'$y$ [ pc ]'
        ylabeltext = r'$z$ [ pc ]'
        xmin, xmax = par.x2min, par.x2max
        ymin, ymax = par.x3min, par.x3max

    # 0 Thresholding (Useful for linear image plots - log plots will automatically do this)
    # Note, this must come before rescale
    # TODO: This will not be needed when revised thresholding is implemented
    if threshold:
        field = np.ma.masked_where(np.isclose(field, 0., 1.e-40), field)

    # If par.vmin = par.vmax, either rescale such that colormap covers
    # a) vmax - vrange -> vmax, if par.vmin > 0, or
    # b) vmin -> vmin + vrange
    # The value of abs(par.vmin) = abs(par.vmin) is used for vrange
    # TODO: Do we really need the float casting here?
    field = np.ma.masked_invalid(field)
    if rescale:
        if vmin is None:
            if par.vmin[ivar] > 0:
                field -= np.nanmax(field) - vrange
            elif par.vmin[ivar] < 0:
                field -= np.nanmin(field)
            else:
                field -= np.nanmin(field)
                vrange = np.nanmax(field)
        else:
            field -= np.float(vmin)
        vmin, vmax = 0., vrange

    else:
        if vmin is None:
            if par.vmin[ivar] > 0:
                vmax = np.nanmax(field)
                vmin = vmax - vrange
            elif par.vmin[ivar] < 0:
                vmin = np.nanmin(field)
                vmax = vmin + vrange
            else:
                vmin = np.nanmin(field)
                vmax = np.nanmax(field)
                vrange = vmax - vmin

    # Flooring (useful for (log) plots with masked cells, to give masked values a color from colormap)
    # Note, this must come after rescale
    if floor:
        field = np.ma.filled(np.ma.masked_invalid(field), np.nanmin(field))

    # Plot the thing
    if plottype == 'imshow':
        pl.imshow(field, vmin=vmin, vmax=vmax, extent=extent)
    elif plottype == 'contour':
        levels = np.linspace(vmin, vmax, par.ncont)
        pl.contour(*domain, field, levels, vmin=vmin, vmax=vmax, linewidths=par.cont_lw)
    else:
        print('Unknown plottype: ' + plottype +'. Must be "imshow" or "contour".')
        raise ValueError

    # Colorbar and label
    try:
        pl.set_cmap(colormap)
    except ValueError:
        pl.set_cmap('Spectral_r')

    if ins_wbg:
        # TODO: cmtools even() not programmed yet. Would be useful here.
        cme = cmt.ColormapEditor(cmap=cm.get_cmap(colormap))
        cme.insert(point=pnt_wbg)
        pl.set_cmap(cme.cmap)

    # TODO: My cmtools doesn't quite work anymore after matplotlib updates. Fix.
    # cccm = cmt.CustomColormaps(cm_in=cm)
    # cmi, cmap = cccm.hot_desaturated_wbg(bgpt=0.48)
    # pl.set_cmap(cmap)

    if colorbar:
        # if par.superimpose and ivar > 0:
        #     cax = pl.gcf().axes[1].axes
        #     cb = pl.colorbar(cax=cax)
        #     cb.ax.tick_params(right=False, left=True)
        #     cb.ax.yaxis.set_label_position('left')
        # else:
        #     cb = pl.colorbar()
        if cbdt > 0:
            ticks = np.arange(vmin, vmax+1.e-12, cbdt)
            if np.all(np.equal(np.mod(ticks, 1), 0)):
                ticks = ticks.astype(np.int)
            cb = pl.colorbar(pad=0.02, aspect=30, ticks=ticks)
        else:
            cb = pl.colorbar(pad=0.02, aspect=30)

        cb_label = get_var_label(var)
        if log:
            cb_label = r'$\log$ ' + cb_label
        cb.set_label(cb_label)

        if plottype == 'contour':
            cb.ax.get_children()[0].set_linewidths(16)

    # Annotate plot
    pl.xticks(np.linspace(xmin, xmax, 5))
    pl.yticks(np.linspace(ymin, ymax, 5))

    if xlabel:
        pl.xlabel(xlabeltext)
    else:
        pl.tick_params(labelbottom=False)
        pl.ylim(ymin * 0.99, ymax)
        if cbdt > 0 and colorbar:
            ticklabels = [str(i) for i in ticks]
            ticklabels[0] = ''
            cb.set_ticklabels(ticklabels)

    if ylabel:
        pl.ylabel(ylabeltext)
    else:
        pl.tick_params(labelleft=False)
        pl.xlim(xmin + 0.01 * abs(xmin), xmax)

    # Time in plot
    if add_time:
        # TODO: Make time units settable and include normalization in tdnorm (this is the time between outputs)
        time_text = r'$t = $' + format(par.tdnorm / pc.kyr * (time - par.time_j), '>4.2f') + ' kyr'
        pl.text(0.65, 1.02, time_text, color='k', transform=pl.gca().transAxes)

    if model_label:
        pl.text(0.45, 1.02, par.run_id, color='k', transform=pl.gca().transAxes)

def dump_and_close_image(pl_axis, pvar, time):

    # Save plot
    pfname = iom.out_dir + '/' + (pvar + '/' if par.sep_out_var_dir else '')
    pfname += '/' + create_fname(par.plot_fbasenm, pvar, pl_axis, par.intercepts[pl_axis-1], time)

    for ext in par.output:
        pl.savefig(pfname + '.' + ext, bbox_inches='tight', dpi=300)

    pl.close(pl.gcf())


def read_slices(var_list, pl_axis, time):
    """
    Reads all necessary files to create plot for a given slice
    :param var_list:   list of necessary primitive slice files
    :param pl_axis:    slice axis number
    :param time:       timestep
    :return:           data
    """

    data = par.nvar * [None]
    dvars = ['rho', 'vx1', 'vx2', 'vx3', 'prs', 'tr1', 'tr2']

    for ivar, dvar in enumerate(dvars):
        if dvar in var_list:
            sfname = create_fname(par.slice_fbasenm, dvar, pl_axis, par.intercepts[pl_axis-1], time)
            sfname = iom.data_dir + '/' + sfname + '.sli'

            data[ivar] = np.loadtxt(sfname)

    # Convert four-vector velocities to standard 3-vector velocities
    # if par.four_vector and {'v1', 'v2', 'v3'} & set(var_list):
    #
    #     vx1, vx2, vx3 = data[par.ivx1], data[par.ivx2], data[par.ivx3]
    #     data[par.ivx1], data[par.ivx2], data[par.ivx3] = four_to_three_vector(vx1, vx2, vx3)

    return data


def prepare_slice(pvar, ivar, rho, vx1, vx2, vx3, prs, tr1, tr2):
    """
    Do the hard work. Create expressions, normalize, and paint result.
    """
    result = expression(pvar, x1, x2, x3, rho, vx1, vx2, vx3, prs, tr1, tr2, rho_min=par.rho_min[ivar],
                        te_min=par.te_min[ivar], te_max=par.te_max[ivar], tr1_min=par.tr1_min[ivar],
                        tr2_min=par.tr2_min[ivar], nm=nm)

    result *= normalization(pvar, nm, par.vnorm)

    if par.log[ivar]:
        result = np.log10(result)

    return result


def movie(ax):

    for pvar in par.vars:

        ifname = iom.out_dir + '/' + create_fname(par.plot_fbasenm, pvar, ax, par.intercepts[ax-1], 'movie') + '.png'
        mfname = iom.out_dir + '/' + create_fname(par.plot_fbasenm, pvar, ax, par.intercepts[ax-1]) + '.mp4'
        cmd = 'ffmpeg -start_number ' + str(par.time_i) + ' -i ' + ifname
        cmd += ' -y -vcodec mpeg4 -framerate 25 -filter:v "setpts=3.0*PTS" -b:v 18000000 '
        cmd += mfname
        print('\n')
        print('Generating movie with following command... ')
        print(cmd)
        print('\n')

        # subprocess.call(cmd, shell=True)

        fm = open(mfname + '.make', 'w')
        fm.write(cmd)
        fm.close()
        subprocess.call(['chmod 775 ' + mfname + '.make'], shell=True)

        subprocess.call(['./' + mfname + '.make'], shell=True)


def mp_slplot(times):

    # Loop over all timesteps
    for time in times:

        for pl_axis in par.axes:

            # Only process non-hidden variables
            pt_vars = [v for i, v in enumerate(par.vars) if par.hide[i] == 0]
            ipt_vars = [i for i, v in enumerate(par.vars) if par.hide[i] == 0]
            rho, vx1, vx2, vx3, prs, tr1, tr2 = read_slices(need_vars(pt_vars), pl_axis, time)

            global x1, x2, x3
            x1, x2, x3 = par.x_space_slices(pl_axis, par.intercepts[pl_axis-1])

            if par.superimpose:
                for ivar, pvar in zip(ipt_vars, pt_vars):
                    result = prepare_slice(pvar, ivar, rho, vx1, vx2, vx3, prs, tr1, tr2)
                    paint_slice(pvar, ivar, result, pl_axis, time)
                    print('pl_axis = ' + str(pl_axis) + '  pvar = ' + pvar)
                dump_and_close_image(pl_axis, '-'.join(par.vars), time)

            else:
                for ivar, pvar in zip(ipt_vars, pt_vars):
                    result = prepare_slice(pvar, ivar, rho, vx1, vx2, vx3, prs, tr1, tr2)
                    paint_slice(pvar, ivar, result, pl_axis, time)
                    dump_and_close_image(pl_axis, pvar, time)
                    print('pl_axis = ' + str(pl_axis) + '  pvar = ' + pvar)


# ______________________________________________________________________________________________________________________
# Main bit

# Get runfile and runname, dat_dir, inifile, setting
iom = IOManager(sys.argv[1:], os.path.splitext(__file__)[0])

# Read settings parameter
par = Parameters(iom.runfile, iom.runname, iom.data_dir, iom.inifile, iom.setting)

# Normalizations
mu = 0.60364
mmpp = mu * pc.amu
kelvin = par.vdnorm * par.vdnorm * pc.amu / pc.kboltz
nm = norm.PhysNorm(x=par.xdnorm, v=par.vdnorm, dens=mmpp, curr=1., temp=kelvin)

# All time steps and number of time steps
times = range(par.time_i, par.time_f + 1, par.time_delta)

# All time steps and number of time steps
all_times = range(par.time_i, par.time_f + 1, par.time_delta)
nt = len(all_times)

times_slices = list(slices(nt, nproc))

# Processes
if nproc > 1:
    pool = mp.Pool(nproc)
    pool.map(mp_slplot, [all_times[i] for i in times_slices])
    pool.close()
    pool.join()

else:
    mp_slplot(all_times)

if par.make_movie:

    if par.axis1:
        movie(1)

    if par.axis2:
        movie(2)

    if par.axis3:
        movie(3)

